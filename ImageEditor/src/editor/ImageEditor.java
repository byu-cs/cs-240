package editor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.Math;
import java.util.Scanner;

public class ImageEditor{
  private int maxRGB = 255;
  private int minRGB = 0;

  public static void main(String[] args) throws FileNotFoundException {
    ImageEditor editor = new ImageEditor();

    String inputFileName = args[0];
    Image inputImage = editor.readImage(inputFileName);

    PrintWriter outputWriter = new PrintWriter(args[1]);
    editor.applyEffect(args, outputWriter, inputImage, editor);
    outputWriter.close();
  }

  /**
   * Applies different effects to an image.
   * @param args list of arguments like the effect type, and the blur amount if exists
   * @param outputWriter An object that writes the output file
   * @param inputImage An object that represents the original image
   * @param editor An object that manipulates the image
   */
  private void applyEffect(String[] args, PrintWriter outputWriter, Image inputImage, ImageEditor editor){
    int width = inputImage.getWidth();
    int height = inputImage.getHeight();
    switch (args[2]) {
      case "invert": {
        Image invertedImage = editor.invert(inputImage);
        outputWriter.println(invertedImage.toString());
        break;
      }
      case "grayscale": {
        Image grayscaleImage = editor.grayscale(inputImage);
        outputWriter.println(grayscaleImage.toString());
        break;
      }
      case "emboss": {
        Image embossedImage = editor.emboss(inputImage);
        outputWriter.println(embossedImage);
        break;
      }
      case "motionblur": {
        //Read the blur amount.
        int blurAmount = Integer.parseInt(args[3]);
        if (blurAmount <= 0) {
          System.out.println("USAGE: java ImageEditor inputFile outputFile motionblur (blur-amount)");
          outputWriter.close();
          return;
        }
        Image blurredImage = editor.motionBlur(inputImage, blurAmount);
        outputWriter.println(blurredImage.toString());
        break;
      }
    }
  }

  /**
   *
   * @param inputFileName the name of the file to read and parse
   * @return image object
   */
  private Image readImage(String inputFileName){
    Image inputImage = new Image(0,0);
    try {
      File imageFile = new File(inputFileName);
      FileReader imageFileReader = new FileReader(imageFile);
      BufferedReader bufferedReader = new BufferedReader(imageFileReader);
      Scanner imageFileScanner = new Scanner(imageFile);
      String commentRegex = "\\s*#.*\n\\s*";
      String whitespaceRegex = "\\s+";
      imageFileScanner.useDelimiter(commentRegex + "|" +whitespaceRegex);
      imageFileScanner.next();
      int width = imageFileScanner.nextInt();
      int height = imageFileScanner.nextInt();
      imageFileScanner.nextInt();
      inputImage = new Image(width, height);
      for (int j = 0; j < height; j++) {
        for (int i = 0; i < width; i++) {
          inputImage.pixels[i][j].setRGB(imageFileScanner.nextInt(),imageFileScanner.nextInt(),imageFileScanner.nextInt());
        }
      }
      bufferedReader.close();
      imageFileScanner.close();
    } catch(FileNotFoundException exception){
      System.out.println("Cannot open file: '" + inputFileName + "'");
    } catch(IOException exception) {
      System.out.println("Error reading file '" + inputFileName + "'");
    } catch(ArrayIndexOutOfBoundsException exception){
      System.out.println("Array index out of bounds. Please, try again");
      System.out.println("Exception: " + exception);
    }
    return inputImage;
  }

  /**
   * Inverts each pixel by subtracting the maximum RGB value and turning the result
   * into an absolute value.
   * @param image An image to be inverted
   * @return The inverted image object
   */
  private Image invert(Image image){
    //Initialize outputImage with the inputImage's width and height
    int width = image.getWidth();
    int height = image.getHeight();
    Image outputImage = new Image(width,height);

    //Manipulate pixels to turn image into grayscale
    for (int x = 0; x < width; x++) {
      for (int y = 0; y < height; y++) {
        //Get the absolute values after subtracting the maxRGB value
        int invertedRed = Math.abs(image.pixels[x][y].getRed() - maxRGB);
        int invertedGreen = Math.abs(image.pixels[x][y].getGreen() - maxRGB);
        int invertedBlue = Math.abs(image.pixels[x][y].getBlue() - maxRGB);

        //Set the outputImage's pixel values to the inverted values
        outputImage.pixels[x][y].setRGB(invertedRed, invertedGreen, invertedBlue);
      }
    }
    return outputImage;
  }

  /**
   * Averages each individual pixel values to turn the image into a grayscale image.
   * @param image An image to turn grayscale
   * @return The grayscale image object
   */
  private Image grayscale(Image image){
    //Initialize outputImage with the inputImage's width and height
    int width = image.getWidth();
    int height = image.getHeight();
    Image outputImage = new Image(width, height);

    //Manipulate pixels to turn image into grayscale
    for (int x = 0; x < width; x++) {
      for (int y = 0; y < height; y++) {
        int redValue = image.pixels[x][y].getRed();
        int greenValue = image.pixels[x][y].getGreen();
        int blueValue = image.pixels[x][y].getBlue();

        //Compute the average of the RGB values
        int colorValuesSum = redValue + greenValue + blueValue;
        final int numberOfColors = 3;
        int average = colorValuesSum / numberOfColors;

        //Set the outputImage's pixel values to the average values
        outputImage.pixels[x][y].setRGB(average, average, average);
      }
    }
    return outputImage;
  }

  private Image emboss(Image image){
    //Initialize outputImage with the inputImage's width and height
    int width = image.getWidth();
    int height = image.getHeight();
    Image outputImage = new Image(width, height);

    //Initialize helper variables for calculating pixel values
    int maxDifference, redDiff, greenDiff, blueDiff;

    for (int x = 0; x < width; x++) {
      for (int y = 0; y < height; y++) {
        //Reset maxDifference variable
        if (x != 0 && y != 0) {
          redDiff = image.pixels[x][y].getRed() - image.pixels[(x - 1)][(y - 1)].getRed();
          greenDiff = image.pixels[x][y].getGreen() - image.pixels[(x - 1)][(y - 1)].getGreen();
          blueDiff = image.pixels[x][y].getBlue() - image.pixels[(x - 1)][(y - 1)].getBlue();

          //Determine the maxDifference
          //Check if blueDiff is greater than greenDiff. Use absolute values to compare.
          maxDifference = Math.abs(blueDiff) > Math.abs(greenDiff) ? blueDiff : greenDiff;
          //Check if redDiff is greater than the other two. Use absolute values to compare.
          maxDifference = Math.abs(maxDifference) > Math.abs(redDiff) ? maxDifference : redDiff;
          //Finally, add 128 to the greater value
          maxDifference += 128;

          //If the result is below 0, set to 0 because negative values are invalid. RGB min = 0
          //If the result is above 255, set to 255. RGB max = 255
          if (maxDifference < minRGB) maxDifference = minRGB;
          else if (maxDifference > maxRGB) maxDifference = maxRGB;
          //Set value to 128 if the pixel of the first row or first column is selected
        } else maxDifference = 128;

        //Set the outputImage's pixel values to the difference values
        outputImage.pixels[x][y].setRGB(maxDifference, maxDifference, maxDifference);
      }
    }
    return outputImage;
  }

  private Image motionBlur(Image image, int blurAmount){
    //Initialize outputImage with the inputImage's width and height
    int width = image.getWidth();
    int height = image.getHeight();
    Image outputImage = new Image(width,height);

    for (int y = 0; y < height; y++) {
      for (int x = 0; x < width; x++) {
        int sumOfRed, sumOfGreen, sumOfBlue, pixelsUsed;
        sumOfRed = sumOfGreen = sumOfBlue = pixelsUsed = 0;
        for (int pixelsMoved = x; pixelsMoved < (blurAmount + x); pixelsMoved++) {
          if (pixelsMoved != width) {
            pixelsUsed++;
            sumOfRed += image.pixels[pixelsMoved][y].getRed();
            sumOfGreen += image.pixels[pixelsMoved][y].getGreen();
            sumOfBlue += image.pixels[pixelsMoved][y].getBlue();
          } else {
            break;
          }
        }

        //Compute the average for each RGB value
        int averageRed = sumOfRed / pixelsUsed;
        int averageGreen = sumOfGreen / pixelsUsed;
        int averageBlue = sumOfBlue / pixelsUsed;

        //Set the outputImage's pixel values to the average values
        outputImage.pixels[x][y].setRGB(averageRed, averageGreen, averageBlue);
      }
    }
    return outputImage;
  }
}