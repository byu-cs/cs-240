package editor;

/**
 * Represents a Pixel that can be manipulated. 
 * @author Josue Lopez
 * @author www.josuelopez.dev
 * @version 1.0
 * @since 1.0
*/

class Pixel {
  private int red;
  private int green;
  private int blue;

  /** Creates a pixel with the specified RGB values.
   * @param  red  the red value of the pixel, max value is 255
   * @param  green the red value of the pixel, max value is 255
   * @param  blue the red value of the pixel, max value is 255
  */
  Pixel(int red, int green, int blue){
    this.red=red;
    this.green=green;
    this.blue=blue;
  }

  //Getters for the pixel color values
  int getRed(){return this.red;}
  int getGreen(){return this.green;}
  int getBlue(){return this.blue;}

  //Setters for the pixel color values
  /**
   *Sets the pixel RGB values in one function.
   * @param red A red value. Max is 255
   * @param green A green value. Max is 255
   * @param blue A blue value. Max is 255
   */
  void setRGB(int red, int green, int blue){
    setRed(red);
    setGreen(green);
    setBlue(blue);
  }

  private void setRed(int red){this.red=red;}
  private void setGreen(int green){this.green=green;}
  private void setBlue(int blue){this.blue=blue;}
}
