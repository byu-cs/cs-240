package editor;

public class Image {
  private int width;
  private int height;
  Pixel[][] pixels;

  /**
   *Creates an image with the specified width and height values.
   * @param width A value for the width of the image.
   * @param height A value for the height of the image.
   */
  Image(int width,int height){
    this.width = width;
    this.height = height;
    this.pixels = new Pixel[width][height];
    for(int x=0;x<width;x++){
      for(int y=0;y<height;y++){
        this.pixels[x][y] = new Pixel(0,0,0);
      }
    }
  }

  /**
   * Get the width of the image.
   * @return An integer that represents the image's width.
   */
  int getWidth(){return width;}

  /**
   * Get the height of the image.
   * @return An integer that represents the image's height.
   */
  int getHeight(){return height;}

  /**
   * Appends image format, width, height, max value, and each pixel.
   * @return a long string with the image data to be parsed.
   */
  public String toString(){
	StringBuilder sb = new StringBuilder();
	sb.append("P3").append("\n");
	sb.append(width).append("\t");
	sb.append(height).append("\n");
	sb.append("255").append("\n");
  for(int j=0;j<height;j++){
    for(int i=0;i<width;i++){
			sb.append(pixels[i][j].getRed()).append("\t");
			sb.append(pixels[i][j].getGreen()).append("\t");
			sb.append(pixels[i][j].getBlue()).append("\t");
		}
    sb.append("\n");
	}
	return sb.toString();
  }
}
