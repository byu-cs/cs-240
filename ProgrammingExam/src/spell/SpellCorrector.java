package spell;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;
import spell.Trie.*;

public class SpellCorrector implements ISpellCorrector {
  private Trie dictionary;
  private char a = 'a';
  private char z = 'z';

  //helper vars
  private String bestWord;
  private int bestFreq;

  public SpellCorrector(){
    dictionary = new Trie();
  }

  @Override
  public void useDictionary(String dictionaryFileName) throws IOException {
    File dicFile = new File(dictionaryFileName);
    Scanner dicScanner = new Scanner(new BufferedReader(new FileReader(dicFile)));

    while(dicScanner.hasNext()){
      String word = dicScanner.next();
      dictionary.add(word);
    }
  }

  @Override
  public String suggestSimilarWord(String inputWord) {
    inputWord = inputWord.toLowerCase();

    if(getWordFrequency(inputWord) > 0) return inputWord;

    bestWord = null;
    bestFreq = 0;

    List<String> firstEdit = generateWordList(inputWord);
    evaluateWords(firstEdit);

    if(bestFreq > 0) return bestWord;

    for(String word : firstEdit){
      List<String> secondEdit = generateWordList(word);
      evaluateWords(secondEdit);
    }

    if(bestFreq > 0) return bestWord;

    return null;
  }

  private void evaluateWords(List<String> edits){
    for(String word : edits){
      int freq = getWordFrequency(word);
      if(freq > 0){
        if(freq == bestFreq && word.compareTo(bestWord) < 0) {
          bestWord = word;
        }
        if(freq > bestFreq) {
          bestFreq = freq;
          bestWord = word;
        }
      }
    }
  }

  private int getWordFrequency(String word){
    TrieNode node = (TrieNode) dictionary.find(word);
    return Objects.isNull(node) ? 0 : node.getValue();
  }

  private List<String> generateWordList(String word){
    List<String> edits = new ArrayList<>();
    int length = word.length();

    //Pass through edits
    deletion(edits, word, length);
    alteration(edits, word, length);
    insertion(edits, word, length);

    return edits;
  }

  private void deletion(List<String> edits, String word, int length){
    for(int i = 0; i < length; i++){
      if(i > 0){
        String one = word.substring(0,i) + word.substring(i+1);
        String two = word.substring(0, i - 1) + word.charAt(i) + word.charAt(i-1) + word.substring(i + 1);

        edits.add(one);
        edits.add(two);
      } else {
        edits.add(word.substring(i+1));
      }
    }
  }
  private void alteration(List<String> edits, String word, int length) {
    for(int i = 0; i < length; i++){
      for(char letter = a; letter <= z; letter++){
        edits.add(word.substring(0,i) + letter + word.substring(i+1));
      }
    }
  }

  private void insertion(List<String> edits, String word, int length) {
    for(int i = 0; i <= length; i++){
      for(char letter = a; letter <= z; letter++){
        edits.add(word.substring(0,i) + letter + word.substring(i));
      }
    }
  }
}
