package spell;

import javax.management.loading.MLet;
import java.awt.desktop.PreferencesEvent;
import java.util.Arrays;
import java.util.Objects;

public class Trie implements ITrie {
  private TrieNode root;
  private int wordCount;
  private int nodeCount;

  //helper vars
  private char a = 'a';
  private char z = 'z';

  public Trie(){
    root = new TrieNode();
    wordCount = 0;
    nodeCount = 1;
  }

  @Override
  public void add(String word) {
    word = word.toLowerCase();

    if(checkDuplicate(word)) return;

    TrieNode currNode = root;

    for(int i = 0; i < word.length(); i++){
      char letter = word.charAt(i);
      TrieNode letterNode = currNode.getCharAt(letter);

      if(Objects.isNull(letterNode)) {
        currNode.setCharAt(letter, new TrieNode());
        nodeCount++;
      }

      currNode = currNode.getCharAt(letter);
    }

    currNode.value++;
    wordCount++;
  }

  private boolean checkDuplicate(String word){
    TrieNode duplicate = (TrieNode) find(word);

    if(Objects.isNull(duplicate)) return false;

    duplicate.value++;
    return true;
  }

  @Override
  public INode find(String word) {
    TrieNode currNode = root;

    for(int i = 0; i < word.length(); i++){
      char letter = word.charAt(i);
      TrieNode letterNode = currNode.getCharAt(letter);

      if(Objects.isNull(letterNode)) return null;
      currNode = currNode.getCharAt(letter);
    }

    return currNode.value == 0 ? null : currNode;
  }

  @Override
  public int getWordCount() {
    return wordCount;
  }

  @Override
  public int getNodeCount() {
    return nodeCount;
  }

  //toSTRING
  @Override
  public String toString(){
    StringBuilder word = new StringBuilder();
    StringBuilder result = new StringBuilder();

    return toStringRec(word, result, root).toString();
  }

  private StringBuilder toStringRec(StringBuilder word, StringBuilder result, TrieNode currNode){
    if(Objects.isNull(currNode)) return result;

    if(currNode.getValue() > 0){
      result.append(word);
      result.append('\n');
    }

    for(char letter = a; letter < z; letter++){
      word.append(letter);

      toStringRec(word, result, currNode.getCharAt(letter));

      word.setLength(word.length() - 1);
    }

    return result;
  }

  //HASH
  @Override
  public int hashCode(){
    return hashRecursion(root);
  }

  private int hashRecursion(TrieNode start){
    if(Objects.isNull(start)) return 1;

    int code = 1;
    for (int letter = a; letter <= z; letter++){
      code += letter * hashRecursion(start.getCharAt((char) letter));
      code *= getWordCount();
    }

    return code;
  }

  //EQUALS
  @Override
  public boolean equals(Object wordTrie){
    //Check object class
    if(!(wordTrie instanceof Trie)) return false;

    Trie word = (Trie) wordTrie;
    boolean sameWordsCount = word.getWordCount() == getWordCount();
    boolean sameNodesCount = word.getWordCount() == getWordCount();
    boolean sameNodes = equalsRecursion(word.root, root);

    return sameWordsCount && sameNodesCount && sameNodes;
  }

  private boolean equalsRecursion(TrieNode wordRoot, TrieNode start){
    if (Objects.isNull(wordRoot) || Objects.isNull(start)) return true;

    if(wordRoot.value != start.value) return false;

    for(char letter = a; letter < z; letter++){
      if(!equalsRecursion(wordRoot.getCharAt(letter), start.getCharAt(letter))) return false;
    }

    return true;
  }

  public static class TrieNode implements INode{
    int value;
    TrieNode[] nodes = new TrieNode[26];

    //helper variables
    private char a = 'a';

    TrieNode(){
      value = 0;
      Arrays.fill(nodes, null);
    }

    @Override
    public int getValue() {
      return value;
    }

    TrieNode getCharAt(char letter) {
      return nodes[letter - a];
    }

    void setCharAt(char letter, TrieNode letterNode) {
      nodes[letter - a] = letterNode;
    }
  }
}
