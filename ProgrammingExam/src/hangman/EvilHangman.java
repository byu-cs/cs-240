package hangman;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import java.util.SortedSet;

public class EvilHangman {


  public static void main(String[] args) {
    File dicFile = null;
    int wordLength = 0;
    int guesses = 0;

    try {
      dicFile = new File(args[0]);
      wordLength = Integer.parseInt(args[1]);
      guesses = Integer.parseInt(args[2]);
    } catch (IllegalArgumentException ex) {
      ex.printStackTrace();
    }

    if (wordLength <1 || guesses < 1) return;

    EvilHangmanGame game = new EvilHangmanGame();

    try{
      game.startGame(dicFile, wordLength);
    } catch (IOException | EmptyDictionaryException ex){
      ex.printStackTrace();
    }

    String finalWord = null;

    boolean wordsMatch = false;
    while(guesses > 0 && !wordsMatch){
      String userWord = printMessage(guesses, game);
      try {
        char guess = getInput();
        game.makeGuess(guess);
        printLetterCount(guess, game.getWords().first());
        guesses--;
      } catch (IllegalArgumentException | GuessAlreadyMadeException ex){
        print(ex.getMessage() + '\n');
      }
      finalWord = getWord(game);
      wordsMatch = !finalWord.contains("-");
    }

    String secret = game.getWords().first();
    print(wordsMatch ? (finalWord + " is correct. You win!\n") : ("You lose! the word was: " + secret));
  }

  private static void printLetterCount(char guess, String word){
    int count = 0;
    for(int i = 0; i < word.length(); i++){
      if (guess == word.charAt(i)) count++;
    }

    if (count == 0) {
      print("There are no " + guess + "\'s\n\n");
    } else {
      print("There " + (count == 1 ? "is " : "are ") + count + " " + guess + "\n\n");
    }
  }

  private static char getInput(){
    Scanner inputReader = new Scanner(System.in);
    String input = inputReader.next();

    if(input.length() != 1) throw new IllegalArgumentException("Please enter only one character\n");
    char guess = input.charAt(0);

    if(!Character.isLetter(guess)) throw new IllegalArgumentException("Please enter a letter\n");

    return guess;
  }

  private static String printMessage(int guesses, EvilHangmanGame game){
    StringBuilder message = new StringBuilder();
    message.append("You have ");
    message.append(guesses);
    message.append(guesses == 1 ? " guess " : " guesses ");
    message.append("left");
    message.append('\n');

    SortedSet<Character> guessedLetters = game.getGuessedLetters();
    message.append("Used letters:");
    for(char letter : guessedLetters) message.append(" ").append(letter);
    message.append('\n');

    message.append("Word: ");
    String word = getWord(game);
    message.append(word);
    message.append('\n');

    message.append("Enter guess: ");

    print(message.toString());

    return word;
  }

  private static boolean checkMatch(){
    return false;
  }

  private static String getWord(EvilHangmanGame game){
    SortedSet<String> words = game.getWords();
    String secret = words.first();
    StringBuilder word = new StringBuilder("-".repeat(secret.length()));


    for(char letter : game.getGuessedLetters()){
      for(int i = 0; i < secret.length(); i++){
        if(letter == secret.charAt(i)) word.setCharAt(i, letter);
      }
    }

    return word.toString();
  }

  private static void print(String out){
    System.out.print(out);
  }
}
