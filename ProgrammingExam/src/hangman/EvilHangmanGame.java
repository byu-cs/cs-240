package hangman;

import java.io.*;
import java.util.*;

public class EvilHangmanGame implements IEvilHangmanGame {
  private SortedSet<String> words = null;
  private SortedSet<Character> guessedLetters = new TreeSet<Character>();

  @Override
  public void startGame(File dictionary, int wordLength) throws IOException, EmptyDictionaryException {
    if(dictionary.length() == 0 || wordLength < 1 ) throw new EmptyDictionaryException();

    Scanner dicScanner = new Scanner(new BufferedReader(new FileReader(dictionary)));

    words = new TreeSet<String>();
    while(dicScanner.hasNext()){
      String word = dicScanner.next();
      if(word.length() == wordLength) words.add(word);
    }

    if (words.size() == 0) throw new EmptyDictionaryException();

  }

  @Override
  public Set<String> makeGuess(char guess) throws GuessAlreadyMadeException {
    guess = Character.toLowerCase(guess);
    if(!guessedLetters.add(guess)) throw new GuessAlreadyMadeException("Guess already made for letter: " + guess + '\n');

    //initialize partition
    HashMap<Integer, SortedSet<String>> partition = groupWords(guess);

    words = getMostDifficultGroup(partition);

    return words;
  }

  private HashMap<Integer, SortedSet<String>> groupWords(char guess){
    HashMap<Integer, SortedSet<String>> partition = new HashMap<>();

    for(String word : words){
      int count = 0;
      int hash = 1;

      for(int i = 0; i < word.length(); i++){
        if(guess == word.charAt(i)) {
          hash *= (word.length() - i);
          count ++;
        }
      }

      hash *= count;

      if(!partition.containsKey(hash)) partition.put(hash, new TreeSet<String>());

      partition.get(hash).add(word);
    }

    return partition;
  }

  private SortedSet<String> getMostDifficultGroup(HashMap<Integer, SortedSet<String>> partition){
    int maxSize = 0;
    int finalKey = 0;
    for(Map.Entry<Integer, SortedSet<String>> entry : partition.entrySet()){
      SortedSet<String> group = entry.getValue();
      int currKey = entry.getKey();

      int currSize = group.size();
      if(currSize == maxSize) if (currKey < finalKey) finalKey = currKey;
      if(currSize > maxSize){
        maxSize = currSize;
        finalKey = currKey;
      }
    }

    return partition.get(finalKey);
  }

  @Override
  public SortedSet<Character> getGuessedLetters() {
    return guessedLetters;
  }

  public SortedSet<String> getWords(){
    return words;
  }
}
