package exception;

public class LoadException extends Exception{
    public LoadException(String message){
        super(message);
    }
}
