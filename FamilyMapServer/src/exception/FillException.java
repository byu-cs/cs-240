package exception;

public class FillException extends Exception{
    public FillException(String message){
        super(message);
    }
}
