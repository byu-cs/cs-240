package service;

import dataAccess.EventDAO;
import exception.DataAccessException;
import exception.EventException;
import model.Event;
import request.EventRequest;
import response.EventResponse;

import java.sql.Connection;
import java.util.ArrayList;

public class EventService {
  private Connection conn = null;

  public EventService(Connection conn) { this.conn = conn; }

  public EventResponse find(EventRequest req) throws EventException {
    try{
      EventDAO eDAO = new EventDAO(conn);
      ArrayList<Event> eventList = new ArrayList<Event>();
      String username = req.getUsername();
      if (req.getEventID() == null || req.getEventID().equals("")) {
        eventList = eDAO.findForUserName(username);
        return new EventResponse(eventList);
      } else {
        Event event = eDAO.findByID(username, req.getEventID());
        if (event == null) throw new EventException("Event not found error");
        eventList.add(event);
        return new EventResponse(eventList);
      }
    } catch (DataAccessException e) {
      e.printStackTrace();
      throw new EventException(e.getMessage());
    }
  }
}
