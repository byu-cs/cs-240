package service;

import dataAccess.EventDAO;
import dataAccess.PersonDAO;
import dataAccess.UserDAO;
import exception.DataAccessException;
import exception.LoadException;
import model.Event;
import model.Person;
import model.User;
import request.LoadRequest;
import response.LoadResponse;

import java.sql.Connection;

public class LoadService {
  private Connection conn = null;

  public LoadService(Connection conn) {
    this.conn = conn;
  }

  public LoadResponse load(LoadRequest req) throws LoadException {
    try{
      LoadResponse res = new LoadResponse(req.getUsers().length, req.getPersons().length, req.getEvents().length);

      insertData(req);

      return res;
    } catch (DataAccessException e) {
      throw new LoadException(e.getMessage());
    }
  }

  private void insertData(LoadRequest data) throws DataAccessException {
    UserDAO uDAO = new UserDAO(conn);
    PersonDAO pDAO = new PersonDAO(conn);
    EventDAO eDAO = new EventDAO(conn);

    for(User user : data.getUsers()){
      User newUser = new User(user.getUserName(),
          user.getPassword(),
          user.getEmail(),
          user.getfirstName(),
          user.getlastName(),
          user.getGender(),
          user.getPersonID());
      uDAO.insert(newUser);
    }

    for(Event event : data.getEvents()){
      eDAO.insert(event);
    }

    for(Person person : data.getPersons()){
      pDAO.insert(person);
    }
  }
}
