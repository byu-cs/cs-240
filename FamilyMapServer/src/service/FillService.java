package service;

import com.google.gson.Gson;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.sql.Connection;
import java.util.Map;

import dataAccess.*;
import exception.DataAccessException;
import exception.FillException;
import model.*;
import request.FillRequest;
import response.FillResponse;

public class FillService {
  private Connection conn = null;

  public FillService(Connection conn) {
    this.conn = conn;
  }

  public FillResponse fill(FillRequest req) throws FillException {
    System.out.println("Filling " + req.getUserName() + "'s data with ancestors and events");
    Reader reader = null;
    FillResponse result = new FillResponse();

    try {
      UserDAO uDAO = new UserDAO(conn);
      PersonDAO pDAO = new PersonDAO(conn);
      EventDAO eDAO = new EventDAO(conn);
      String userName = req.getUserName();
      User usr = uDAO.findUsername(userName);
      if (usr == null) {
        throw new FillException("Error while finding user");
      }

      Person prsn = pDAO.find(usr.getPersonID());

      //Clear Ancestors and Events
      System.out.print("Clearing ancestors and events");
      pDAO.clearUserAncestors(userName);
      eDAO.clearUserEvents(userName);
      System.out.print("... Done!\n");

      //Parse locations to generate ancestors
      Locations locations = readLocations();
      prsn.setLocations(locations.getData());

      //Parse names to generate ancestors
      Names names = readNames();
      prsn.setNames(names);

      int birthYear = 2019 - (int) (Math.random() * 100);

      //Generate events and insert into DB
      prsn.generateEvents(true, 0, null);
      prsn.updateEventTotals(prsn.getNewEvents().size());
      insertEvents(prsn, eDAO);

      //Generate ancestors and insert into DB
      prsn.generateAncestors(req.getGenCount());
      pDAO.insert(prsn);
      insertAncestors(prsn, pDAO, eDAO);

      result = new FillResponse(prsn.getAncestors().size(), prsn.getEventTotals());
    }
    catch (FileNotFoundException | DataAccessException e) {
      e.printStackTrace();
    } catch (FillException e) {
      e.printStackTrace();
      throw new FillException(e.getMessage());
    }
    return result;
  }

  private void insertEvents(Person person, EventDAO eDAO) throws DataAccessException {
    for(Map.Entry<String, Event> entry: person.getNewEvents().entrySet()){
      Event event = entry.getValue();
      eDAO.insert(event);
    }
  }

  private void insertAncestors(Person prsn, PersonDAO pDAO, EventDAO eDAO) throws DataAccessException {
    for (Person ancestor : prsn.getAncestors()) {
      pDAO.insert(ancestor);
      insertEvents(ancestor, eDAO);
    }
  }

  private Locations readLocations() throws FileNotFoundException {
    Reader reader = null;
    Gson gson = new Gson();

    reader = new FileReader("json/locations.json");
    return gson.fromJson(reader, Locations.class);
  }

  private Names readNames() throws FileNotFoundException {
    Reader reader = null;
    Names names = new Names();
    Gson gson = new Gson();

    //Female names
    reader = new FileReader("json/fnames.json");
    Names.Female f = gson.fromJson(reader, Names.Female.class);
    names.setFemale(f.getData());

    //Male names
    reader = new FileReader("json/mnames.json");
    Names.Male m = gson.fromJson(reader, Names.Male.class);
    names.setMale(m.getData());

    //Surnames
    reader = new FileReader("json/snames.json");
    Names.Surname s = gson.fromJson(reader, Names.Surname.class);
    names.setSurname(m.getData());

    return names;
  }
}
