package service;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.sql.Connection;
import java.util.UUID;

import exception.DataAccessException;
import exception.FillException;
import exception.LoginException;
import exception.RegisterException;
import model.Locations;
import model.User;
import model.Person;
import dataAccess.UserDAO;
import dataAccess.PersonDAO;
import request.FillRequest;
import request.LoginRequest;
import request.RegisterRequest;
import response.LoginResponse;
import response.RegisterResponse;
import com.google.gson.Gson;

public class RegisterService {
  private Connection conn = null;

  public RegisterService(Connection conn) {
    this.conn = conn;
  }

  public RegisterResponse register(RegisterRequest req) throws RegisterException {

    try {
      RegisterResponse result = new RegisterResponse();

      //Check if user already exists
      User usr = createUser(req);

      FillRequest request = new FillRequest(usr.getUserName(), 4);
      FillService service = new FillService(conn);
      service.fill(request);

      LoginRequest loginReq = new LoginRequest(usr.getUserName(), usr.getPassword());
      LoginService logService = new LoginService(conn);
      LoginResponse logResult = logService.login(loginReq);

      result = new RegisterResponse(logResult.getAuthToken(), usr.getUserName(), usr.getPersonID());
      return result;
//          }
    } catch(DataAccessException | FillException | LoginException e) {
      e.printStackTrace();
      throw new RegisterException(e.getMessage());
    }
  }

  private User createUser(RegisterRequest req) throws DataAccessException, RegisterException {
    System.out.print("Creating user for username " + req.getUserName());
    UserDAO uDAO = new UserDAO(conn);
    User usr = new User(req.getUserName(), req.getPassword(), req.getEmail(), req.getfirstName(), req.getlastName(), req.getGender().toLowerCase());

    if (uDAO.findUsername(usr.getUserName()) != null) {
      throw new RegisterException("Username (" + usr.getUserName() + ") already exists error");
    }


    System.out.print("... Done!\n");

    Person prsn = createPerson(usr);

    usr.setPersonID(prsn.getPersonID());
    uDAO.insert(usr);
    return usr;
  }

  private Person createPerson(User usr) throws RegisterException {
    System.out.print("Creating person for " + usr.getfirstName() + " " + usr.getlastName());

    try{
      Person prsn = new Person(usr.getUserName(), usr.getfirstName(), usr.getlastName(), usr.getGender());
      prsn.setAssociatedUsername(usr.getUserName());

      //Generate locations
      Locations locationData = loadLocations();
      prsn.setLocations(locationData.getData());
      prsn.generateEvents(true, 0, null);

      prsn.setMotherID(UUID.randomUUID().toString());
      prsn.setFatherID(UUID.randomUUID().toString());

      PersonDAO pDAO = new PersonDAO(conn);
      pDAO.insert(prsn);

      System.out.print("... Done!\n");
      return prsn;
    } catch (FileNotFoundException | DataAccessException e) {
      throw new RegisterException(e.getMessage());
    }

  }

  private Locations loadLocations() throws FileNotFoundException {
    Reader reader = new FileReader("json/locations.json");
    Gson gson = new Gson();
    return gson.fromJson(reader, Locations.class);
  }
}

