package handler;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import dataAccess.Database;
import exception.DataAccessException;
import exception.ClearException;
import exception.RequestException;
import request.Request;
import response.ClearResponse;
import response.Response;

import java.io.IOException;
import java.net.HttpURLConnection;

public class ClearHandler implements HttpHandler{
  public ClearHandler() { }

  @Override
  public void handle(HttpExchange exchange) throws IOException {
    System.out.print("/clear");
    Request request = new Request(exchange);
    Response response = new Response(exchange);

    try{
      //Validate method. Only allow POST
      request.validateMethod("POST");

      //Execute request and serve response
      ClearResponse resBody = executeRequest();

      //If no errors are thrown, set the response status to 200
      exchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);

      //Write the data to the response body
      response.writeResBody(resBody);

      System.out.print("... Done!\n");
      //Close the output stream
      exchange.getResponseBody().close();
    } catch (DataAccessException | ClearException | RequestException e) {
      System.out.print("... Failed!\n");
      int status = HttpURLConnection.HTTP_OK;
      response.sendError(status, e);
    }
  }

  private ClearResponse executeRequest() throws DataAccessException, ClearException {
    Database db = new Database();
    try{
      db.openConnection();
      db.clearTables();
      db.closeConnection(true);
      return new ClearResponse("Clear succeeded.");
    } catch (DataAccessException e) {
      db.closeConnection(false);
      throw new ClearException(e.getMessage());
    }
  }
}
