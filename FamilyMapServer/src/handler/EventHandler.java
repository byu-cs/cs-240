package handler;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import dataAccess.Database;
import exception.DataAccessException;
import exception.EventException;
import exception.RequestException;
import json.JSON;
import model.AuthToken;
import request.EventRequest;
import request.Request;
import response.EventResponse;
import response.Response;
import service.EventService;

import java.io.IOException;
import java.net.HttpURLConnection;

public class EventHandler implements HttpHandler {
  Database db = new Database();

  public EventHandler() { }

  @Override
  public void handle(HttpExchange exchange) throws IOException {
    String uri = exchange.getRequestURI().toString();
    System.out.print("GET: " + uri);

    JSON json = new JSON();
    Request request = new Request(exchange);
    Response response = new Response(exchange);

    try{
      request.validateMethod("GET");
      AuthToken authToken = request.validateAuthorization();

      String username = authToken.getAssociatedUserName();
      String eventID = parseEventID(uri);

      EventRequest req = new EventRequest(username, eventID);

      EventResponse resBody = executeRequest(req);

      //If no errors are thrown, set the response status to 200
      exchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);

      //Write the data to the response body
      response.writeResBody(eventID != null ? resBody.getData().get(0) : resBody);

      //Close the output stream
      System.out.print("... Done!\n");
      exchange.getResponseBody().close();
    } catch (RequestException | EventException | DataAccessException e) {
      System.out.print("... Failed!\n");
      int status = HttpURLConnection.HTTP_OK;
      response.sendError(status, e);
    }

  }

  private String parseEventID(String uri){
    String[] params = uri.split("/");
    if(params.length > 2) return params[2];
    return null;
  }

  private EventResponse executeRequest(EventRequest req) throws EventException, DataAccessException {
    try{
      EventService service = new EventService(db.openConnection());
      EventResponse res = service.find(req);
      db.closeConnection(true);
      return res;
    } catch (DataAccessException | EventException e) {
      db.closeConnection(false);
      throw new EventException(e.getMessage());
    }
  }


}
