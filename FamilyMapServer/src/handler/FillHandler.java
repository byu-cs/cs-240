package handler;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import dataAccess.Database;
import exception.DataAccessException;
import exception.FillException;
import exception.RequestException;
import json.JSON;
import request.FillRequest;
import request.Request;
import response.FillResponse;
import response.Response;
import service.FillService;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.sql.Connection;

public class FillHandler implements HttpHandler {
  private Database db = new Database();

  public FillHandler() { }

  @Override
  public void handle(HttpExchange exchange) throws IOException {
    System.out.println("POST: /fill");
    JSON json = new JSON();
    Request request = new Request(exchange);
    Response response = new Response(exchange);

    try{
      request.validateMethod("POST");

      String[] params =  exchange.getRequestURI().toString().split("/");

      String userName = params[2];
      int generations = getGenerations(params);

      FillRequest reqBody = new FillRequest(userName, generations);

      FillResponse resBody = executeRequest(reqBody);

      exchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);

      response.writeResBody(resBody);

      System.out.print("Done Filling!\n");
      exchange.getResponseBody().close();
    } catch (FillException | DataAccessException | RequestException e) {
      response.sendError(HttpURLConnection.HTTP_OK, e);
    }
  }

  private FillResponse executeRequest(FillRequest reqBody) throws DataAccessException, FillException {
    Database db = new Database();
    try{
      Connection conn = db.openConnection();
      FillService service = new FillService(conn);
      FillResponse response = service.fill(reqBody);
      db.closeConnection(true);
      return response;
    } catch (DataAccessException | FillException e) {
      db.closeConnection(false);
      throw new FillException(e.getMessage());
    }
  }

  private int getGenerations(String[] params) throws FillException {
    int generations = 4;
    try{
      if (params.length > 3) generations = Integer.parseInt(params[3]);
      if (generations < 0) throw new NumberFormatException();
    } catch (NumberFormatException e){
      throw new FillException("Please enter a number");
    }
    return generations;
  }
}
