package handler;

import com.google.gson.JsonSyntaxException;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import dataAccess.Database;
import exception.DataAccessException;
import exception.LoginException;
import exception.RequestException;
import request.LoginRequest;
import request.Request;
import response.LoginResponse;
import response.Response;
import service.LoginService;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.sql.Connection;

public class LoginHandler implements HttpHandler{
  private Database db = new Database();

  @Override
  public void handle(HttpExchange exchange) throws IOException {
    System.out.print("/login");
    Request request = new Request(exchange);
    Response response = new Response(exchange);

    try{
      //Validate method
      request.validateMethod("POST");

      //Load request data
      LoginRequest reqBody = (LoginRequest) request.getRequestBody(LoginRequest.class);

      //Send request to the service and return the response
      LoginResponse resBody = executeRequest(reqBody);

      //If no errors are thrown, set the response status to 200
      exchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);

      //Write the data to the response body
      response.writeResBody(resBody);

      //Close the output stream
      System.out.print("... Done!\n");
      exchange.getResponseBody().close();
    } catch (DataAccessException | LoginException | JsonSyntaxException | RequestException e) {
      System.out.print("... Failed!\n");
      int status = HttpURLConnection.HTTP_OK;
      response.sendError(status, e);
    }
  }

  private LoginResponse executeRequest(LoginRequest req) throws DataAccessException, LoginException {
    Database db = new Database();
    try{
      Connection conn = db.openConnection();
      LoginService service = new LoginService(conn);
      LoginResponse res = service.login(req);
      db.closeConnection(true);
      return res;
    } catch (DataAccessException | LoginException e) {
      db.closeConnection(false);
      throw new LoginException(e.getMessage());
    }
  }
}
