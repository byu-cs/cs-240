package handler;

import com.google.gson.JsonSyntaxException;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import dataAccess.Database;
import exception.DataAccessException;
import exception.LoadException;
import exception.RequestException;
import json.JSON;
import request.LoadRequest;
import request.Request;
import response.LoadResponse;
import response.Response;
import service.LoadService;

import java.io.*;
import java.net.HttpURLConnection;
import java.sql.Connection;

public class LoadHandler implements HttpHandler {

  @Override
  public void handle(HttpExchange exchange) throws IOException {
    System.out.print("/load");
    JSON json = new JSON();
    Request request = new Request(exchange);
    Response response = new Response(exchange);

    try{
      //Validate method
      request.validateMethod("POST");

      //Load request data
      LoadRequest reqBody = (LoadRequest) request.getRequestBody(LoadRequest.class);

      //Send request to the service and return the response
      LoadResponse resBody = executeRequest(reqBody);

      //If no errors are thrown, set the response status to 200
      exchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);

      //Write the data to the response body
      response.writeResBody(resBody);

      //Close the output stream
      System.out.print("... Done!\n");
      exchange.getResponseBody().close();
    } catch (DataAccessException | LoadException | JsonSyntaxException | RequestException e) {
      System.out.print("... Failed!\n");
      int status = HttpURLConnection.HTTP_OK;
      response.sendError(status, e);
    }
  }

  private LoadResponse executeRequest(LoadRequest req) throws DataAccessException, LoadException {
    Database db = new Database();
    try{
      Connection conn = db.openConnection();
      db.clearTables();
      LoadService service = new LoadService(conn);
      LoadResponse res = service.load(req);
      db.closeConnection(true);
      return res;
    } catch (DataAccessException | LoadException e) {
      db.closeConnection(false);
      throw new LoadException(e.getMessage());
    }
  }
}
