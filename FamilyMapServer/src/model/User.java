package model;

import java.util.Objects;
import java.util.UUID;

public class User {
    private String userID;
    private String userName;
    private String password;
    private String email;
    private String firstName;
    private String lastName;
    private String gender;
    private String personID;

    /**
     * Constructs a User object with the following parameters
     * @param userName Unique user name (non-empty string)
     * @param password User's password (non-empty string)
     * @param email User's email address (non-empty string)
     * @param firstName User's first name (non-empty string)
     * @param lastName User's last name (non-empty string)
     * @param gender User's gender (string: "f" or "m")
     * @param personID Unique Person ID assigned to this user's generated Person object
     */
    public User(String userName, String password, String email, String firstName, String lastName, String gender, String personID) {
        this.userID = UUID.randomUUID().toString();
        this.userName = userName;
        this.password = password;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.personID = personID;
    }

    public User(String userID, String userName, String password, String email, String firstName, String lastName, String gender, String personID) {
        this.userID = userID;
        this.userName = userName;
        this.password = password;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.personID = personID;
    }

    public User(String userName, String password, String email, String firstName, String lastName, String gender) {
        this.userID = UUID.randomUUID().toString();
        this.userName = userName;
        this.password = password;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
    }

  public User(String userName) {
    this.personID = UUID.randomUUID().toString();
    this.userName = userName;
  }

    public String getUserID() { return userID; }

    public void setUserID(String userID) { this.userID = userID; }

    public String getUserName() { return userName; }

    public void setUserName(String userName) { this.userName = userName; }

    public String getPassword() { return password; }

    public void setPassword(String password) { this.password = password; }

    public String getEmail() { return email; }

    public void setEmail(String email) { this.email = email; }

    public String getfirstName() { return firstName; }

    public void setfirstName(String firstName) { this.firstName = firstName; }

    public String getlastName() { return lastName; }

    public void setlastName(String lastName) { this.lastName = lastName; }

    public String getGender() { return gender; }

    public void setGender(String gender) { this.gender = gender; }

    public String getPersonID() { return personID; }

    public void setPersonID(String personID) { this.personID = personID; }

    @Override
    public String toString() {
        return "User{" +
            "userID='" + userID + '\'' +
            ", userName='" + userName + '\'' +
            ", password='" + password + '\'' +
            ", email='" + email + '\'' +
            ", firstName='" + firstName + '\'' +
            ", lastName='" + lastName + '\'' +
            ", gender='" + gender + '\'' +
            ", personID='" + personID + '\'' +
            '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return getUserID().equals(user.getUserID()) &&
            getUserName().equals(user.getUserName()) &&
            getPassword().equals(user.getPassword()) &&
            getEmail().equals(user.getEmail()) &&
            getfirstName().equals(user.getfirstName()) &&
            getlastName().equals(user.getlastName()) &&
            Objects.equals(getGender(), user.getGender()) &&
            getPersonID().equals(user.getPersonID());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUserID(), getUserName(), getPassword(), getEmail(), getfirstName(), getlastName(), getGender(), getPersonID());
    }
}