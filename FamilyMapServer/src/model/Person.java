package model;

import java.util.*;

public class Person {
  private String personID;
  private String associatedUsername;
  private String firstName;
  private String lastName;
  private String gender;
  private String fatherID = null;
  private String motherID = null;
  private String spouseID = null;
  private transient Names names = null;

  //External Data
  private transient Locations.Location[] locations = null;
  private transient HashMap<String, Event> newEvents = new HashMap<>();

  private transient int eventTotals;
  private transient ArrayList<Person> ancestors = new ArrayList<Person>();

  /**
   * Constructs a Person object with the following parameters
   * @param associatedUsername User to which this person belongs
   * @param firstName Person's first name (non-empty string)
   * @param lastName Person's last name (non-empty string)
   * @param gender Person's gender ("m" or "f")
   * @param fatherID Person's father (possibly null)
   * @param motherID Person's mother (possibly null)
   * @param spouseID Person's spouse (possibly null)
   */
  public Person(String associatedUsername, String firstName, String lastName, String gender, String fatherID, String motherID, String spouseID) {
    this.personID = UUID.randomUUID().toString();
    this.associatedUsername = associatedUsername;
    this.firstName = firstName;
    this.lastName = lastName;
    this.gender = gender;
    this.fatherID = fatherID;
    this.motherID = motherID;
    this.spouseID = spouseID;
  }

  public Person(String personID, String associatedUsername, String firstName, String lastName, String gender, String fatherID, String motherID, String spouseID) {
    this.personID = personID;
    this.associatedUsername = associatedUsername;
    this.firstName = firstName;
    this.lastName = lastName;
    this.gender = gender;
    this.fatherID = fatherID;
    this.motherID = motherID;
    this.spouseID = spouseID;
  }

  public Person(String associatedUsername, String firstName, String lastName, String gender) {
    this.personID = UUID.randomUUID().toString();
    this.associatedUsername = associatedUsername;
    this.firstName = firstName;
    this.lastName = lastName;
    this.gender = gender;
  }

  public String getPersonID() {
    return personID;
  }

  public void setPersonID(String personID) {
    this.personID = personID;
  }

  public String getAssociatedUsername() {
    return associatedUsername;
  }

  public void setAssociatedUsername(String associatedUsername) {
    this.associatedUsername = associatedUsername;
  }

  public String getfirstName() {
    return firstName;
  }

  public void setfirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getlastName() {
    return lastName;
  }

  public void setlastName(String lastName) {
    this.lastName = lastName;
  }

  public String getGender() {
    return gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public String getFatherID() {
    return fatherID;
  }

  public void setFatherID(String fatherID) {
    this.fatherID = fatherID;
  }

  public String getMotherID() {
    return motherID;
  }

  public void setMotherID(String motherID) {
    this.motherID = motherID;
  }

  public String getSpouseID() {
    return spouseID;
  }

  public void setSpouseID(String spouseID) {
    this.spouseID = spouseID;
  }

  public Locations.Location[] getLocations() { return locations; }

  public void setLocations(Locations.Location[] locations) { this.locations = locations; }

  public Names getNames() { return names; }

  public void setNames(Names names) { this.names = names; }

  public ArrayList<Person> getAncestors() { return ancestors; }

  public void setAncestors(ArrayList<Person> ancestors) { this.ancestors = ancestors; }

  public int getEventTotals() { return eventTotals; }

  public HashMap<String, Event> getNewEvents() { return newEvents; }

  public void setNewEvents(HashMap<String, Event> newEvents) { this.newEvents = newEvents; }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Person)) return false;
    Person person = (Person) o;
    return Objects.equals(getPersonID(), person.getPersonID()) &&
        Objects.equals(getAssociatedUsername(), person.getAssociatedUsername()) &&
        Objects.equals(getfirstName(), person.getfirstName()) &&
        Objects.equals(getlastName(), person.getlastName()) &&
        Objects.equals(getGender(), person.getGender()) &&
        Objects.equals(getFatherID(), person.getFatherID()) &&
        Objects.equals(getMotherID(), person.getMotherID()) &&
        Objects.equals(getSpouseID(), person.getSpouseID());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getPersonID(), getAssociatedUsername(), getfirstName(), getlastName(), getGender(), getFatherID(), getMotherID(), getSpouseID());
  }

  public void generateEvents(boolean user, int childYear, Event marriageEvent) {
    int MAX_YEAR;
    int MAX_LIFE = 100;

    int ageAtChildBirth = 0;
    int birthYear;
    String type;

    //Create birth event;
    type = "birth";
    if(user){
      MAX_YEAR = 2019;
      int yearsBefore = getRandomInt(90, 8);
      birthYear = MAX_YEAR - yearsBefore;

      Event birth = createEvent(type, birthYear);
      this.newEvents.put(type, birth);
    } else {
      //For parent birth
      int oldest = childYear - 50;
      int youngest = childYear - 13;

      if(marriageEvent != null) {
        int marriageYear = Integer.parseInt(marriageEvent.getYear());
        youngest = marriageYear - 13;
      }

      birthYear = getYearInBetween(oldest, youngest);

      Event birth = createEvent(type, birthYear);
      this.newEvents.put(type, birth);
    }

    //Create marriage event
    type = "marriage";
    if(!user){
      if(marriageEvent != null){
        Event marriage = new Event(
            this.getPersonID(), this.getAssociatedUsername(),
            marriageEvent.getLatitude(), marriageEvent.getLongitude(),
            marriageEvent.getCountry(), marriageEvent.getCity(),
            type, marriageEvent.getYear()
        );

        this.newEvents.put(type, marriage);
      } else {
        int youngest = birthYear + 13;
        int marriageYear = getYearInBetween(childYear, youngest);

        Event marriage = createEvent(type, marriageYear);
        this.newEvents.put(type, marriage);
      }
    }

    //Create death event
    type = "death";
    if(!user){
      int marriageYear = Integer.parseInt(this.getNewEvents().get("marriage").getYear());
      int oldest = birthYear + MAX_LIFE;
      int youngest = Math.max(childYear, marriageYear);
      int deathYear = getYearInBetween(oldest, youngest);

      Event death = createEvent(type, deathYear);
      this.newEvents.put(type, death);
    }
  }

  private int getRandomInt(int max, int min){
    return (int) (Math.random() * (max - min)) + min;
  }

  private transient Random rand = new Random();
  private Event createEvent(String type, int year){
    int index = rand.nextInt(locations.length);

    return new Event(
        this.personID,
        this.associatedUsername,
        locations[index].getLatitude(),
        locations[index].getLongitude(),
        locations[index].getCountry(),
        locations[index].getCity(),
        type,
        Integer.toString(year)
    );
  }

  private int getYearInBetween(int max, int min){
    return min + (int) (Math.random() * (max - min));
  }

  private void addAncestors(Person person) {
    ancestors.add(person);
  }

  public void generateAncestors(int generations) {
    generateAncestorsRecurse(this, generations);
  }

  private void generateAncestorsRecurse(Person originalPerson, int generations) {
    if(generations < 1) { return; }

    //Generate parents for current person
    Person mother = createParent(originalPerson, true);
    Person father = createParent(originalPerson, false);

    //Create relation between parents
    mother.setSpouseID(father.getPersonID());
    father.setSpouseID(mother.getPersonID());

    //Get the current person's birth year
    int childYear = Integer.parseInt(this.getNewEvents().get("birth").getYear());

    //Generate events for parents
    mother.generateEvents(false, childYear, null);
    father.generateEvents(false, childYear, mother.getNewEvents().get("marriage"));

    //Add the ancestor to the list
    originalPerson.addAncestors(mother);
    originalPerson.addAncestors(father);

    //Update totals
    originalPerson.updateEventTotals(mother.getNewEvents().size());
    originalPerson.updateEventTotals(father.getNewEvents().size());

    //Recurse
    mother.generateAncestorsRecurse(originalPerson, generations-1);
    father.generateAncestorsRecurse(originalPerson, generations-1);
  }

  public void updateEventTotals(int eventNum) {
    this.eventTotals += eventNum;
  }

  private Person createParent(Person descendant, boolean isMother){
    String name;
    String surname;
    
    if(isMother){
      name = names.getFemale()[rand.nextInt(names.getFemale().length)];
      surname = names.getSurname()[rand.nextInt(names.getSurname().length)];
    } else {
      name = names.getMale()[rand.nextInt(names.getMale().length)];
      surname = this.getlastName();
    }

    Person person = new Person(this.associatedUsername, name, surname, isMother ? "f" : "m");

    if(isMother){
      this.setMotherID(person.getPersonID());
    } else {
      this.setFatherID(person.getPersonID());
    }
    
    person.setNames(this.names);
    person.setLocations(this.locations);
    person.setAssociatedUsername(descendant.getAssociatedUsername());

    return person;
  }

  @Override
  public String toString() {
    return "Person{" +
        "personID='" + personID + '\'' +
        ", name='" + firstName + " " + lastName + "\'" +
        ", father='" + fatherID + '\'' +
        ", mother='" + motherID + '\'' +
        ", spouse='" + spouseID + '\'' +
        '}';
  }
}
