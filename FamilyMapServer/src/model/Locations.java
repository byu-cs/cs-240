package model;

public class Locations {
  private Location[] data;

  public Locations(){}

  public Location[] getData(){ return data;}

  public static class Location {
    private double latitude;
    private double longitude;
    private String city;
    private String country;

    public Location(){}

    double getLatitude() {
      return latitude;
    }

    double getLongitude() {
      return longitude;
    }

    String getCity() {
      return city;
    }

    String getCountry() {
      return country;
    }

    @Override
    public String toString() {
      return "Location{" +
              "latitude=" + latitude +
              ", longitude=" + longitude +
              ", city='" + city + '\'' +
              ", country='" + country + '\'' +
              '}';
    }
  }
}
