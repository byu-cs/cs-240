package request;

/**
 * Event request class used to create request objects
 */
public class EventRequest {
  String username;
  String eventID;

  /**
   * Constructor for the event request object
   * @param username authentication username passed in header
   * @param eventID id for the current event
   */
  public EventRequest(String username, String eventID) {
    this.username = username;
    this.eventID = eventID;
  }

  public String getUsername() {
    return username;
  }

  public String getEventID() {
    return eventID;
  }
}
