package request;

/**
 * Register request class used to create request objects
 */
public class RegisterRequest {
  String userName;
  String password;
  String email;
  String firstName;
  String lastName;
  String gender = "";

  /**
   * Constructor for the object
   * @param userName   "susan", // Non-empty string
   * @param password   "secret", // Non-empty string
   * @param email   "susan@gmail.com", // Non-empty string
   * @param firstName   "Susan", // Non-empty string
   * @param lastName   "Ellis", // Non-empty string
   * @param gender   "f" // "f" or "m"
   */
  public RegisterRequest(String userName, String password, String email, String firstName, String lastName, String gender) {
    this.userName = userName;
    this.password = password;
    this.email = email;
    this.firstName = firstName;
    this.lastName = lastName;
    this.gender = gender;
  }

  public String getUserName() {
    return userName;
  }

  public String getPassword() {
    return password;
  }

  public String getEmail() {
    return email;
  }

  public String getfirstName() {
    return firstName;
  }

  public String getlastName() {
    return lastName;
  }

  public String getGender() {
    return gender;
  }

  @Override
  public String toString() {
    return "RegisterRequest{" +
        "userName='" + userName + '\'' +
        ", password='" + password + '\'' +
        ", email='" + email + '\'' +
        ", firstName='" + firstName + '\'' +
        ", lastName='" + lastName + '\'' +
        ", gender='" + gender + '\'' +
        '}';
  }
}
