package request;

/**
 * Clear request class used to create request objects
 */
public class ClearRequest {
  String token = null;

  public ClearRequest(String token) { this.token = token; }

  public String getToken() { return token; }

  public void setToken(String token) { this.token = token; }
}
