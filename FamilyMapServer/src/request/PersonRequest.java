package request;

/**
 * Person request class used to create request objects
 */
public class PersonRequest {
  String username;
  String personID;

  /**
   * Constructor for the person request object
   * @param username authentication username passed in header
   * @param personID id for the current person
   */
  public PersonRequest(String username, String personID) {
    this.username = username;
    this.personID = personID;
  }

  public String getUsername() {
    return username;
  }

  public String getPersonID() {
    return personID;
  }
}
