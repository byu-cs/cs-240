package request;

/**
 * Login request class used to create request objects
 */
public class LoginRequest {
  /**
   * "susan", // Non-empty string
   */
  String userName;
  /**
   * "mysecret" // Non-empty string
   */
  String password;

  public LoginRequest(String userName, String password) {
    this.userName = userName;
    this.password = password;
  }

  public String getUserName() { return userName; }

  public String getPassword() { return password; }
}
