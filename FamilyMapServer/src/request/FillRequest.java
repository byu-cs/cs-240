package request;

public class FillRequest {
  private String userName;
  private int genCount;

  public FillRequest(String userName, int genCount) {
    this.userName = userName;
    this.genCount = genCount;
  }

  public String getUserName() { return userName; }

  public int getGenCount() { return genCount; }
}
