package request;

import com.google.gson.JsonSyntaxException;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import dataAccess.AuthTokenDAO;
import dataAccess.Database;
import exception.DataAccessException;
import exception.RequestException;
import json.JSON;
import model.AuthToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

public class Request {
  HttpExchange exchange = null;

  public Request(HttpExchange exchange) {
    this.exchange = exchange;
  }

  public void validateMethod(String method) throws IOException, RequestException {
    if (!method.equals(exchange.getRequestMethod().toUpperCase())){
      System.out.print("... Method Failed!\n");
      exchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, 0);
      throw new RequestException("Invalid method error");
    }
  }

  public AuthToken validateAuthorization() throws RequestException, DataAccessException {
    Headers reqHeaders = exchange.getRequestHeaders();
    // Check to see if an "Authorization" header is present
    if (reqHeaders.containsKey("Authorization")) {

      // Extract the auth token from the "Authorization" header
      String token = reqHeaders.getFirst("Authorization");

      if(token == null || token.equals("")) {
        throw new RequestException("No token provided error");
      }

      AuthToken authToken = null;
      Database db = new Database();

      try{
        AuthTokenDAO tDAO = new AuthTokenDAO(db.openConnection());
        authToken = tDAO.find(token);

        if(authToken == null) {
          throw new RequestException("Invalid token error");
        }

        int maxExpiration = 24 * 60 * 60 * 1000; //24 hours
        if(System.currentTimeMillis() - authToken.getDateCreated() > maxExpiration ){
          throw new RequestException("Token expired error");
        }
        db.closeConnection(true);
        return authToken;
      } catch (DataAccessException | RequestException e) {
        db.closeConnection(false);
        throw new RequestException(e.getMessage());
      }
    } else throw new RequestException("No authorization header error");
  }

  public Object getRequestBody(Class objClass) throws IOException {
    try{
      JSON json = new JSON();
      String reqData = readRequestBody();
      return json.decode(reqData, objClass);
    } catch (JsonSyntaxException e) {
      throw new IOException("Invalid JSON");
    }
  }

  private String readRequestBody() throws IOException {
    InputStream is = exchange.getRequestBody();
    StringBuilder sb = new StringBuilder();
    InputStreamReader sr = new InputStreamReader(is);
    char[] buf = new char[1024];
    int len;
    while ((len = sr.read(buf)) > 0) {
      sb.append(buf, 0, len);
    }
    return sb.toString();
  }
}
