package server;

import com.sun.net.httpserver.HttpServer;
import dataAccess.Database;
import exception.DataAccessException;
import handler.*;

import java.io.IOException;
import java.net.InetSocketAddress;

public class Server {

  public static void main(String[] args){
    try {
    //Determine the port number from the arguments
    int port = args.length > 0 ? Integer.parseInt(args[0]) : 8080;
      new Server().startServer(port);
    } catch (IOException | NumberFormatException e) {
      e.printStackTrace();
    }
  }

  private void startServer(int port) throws IOException {
    InetSocketAddress serverAddress = new InetSocketAddress(port);
    HttpServer server = HttpServer.create(serverAddress, 10);
    startDatabase();
    registerHandlers(server);
    server.start();
    System.out.println("FamilyMapServer listening on port " + port);
    System.out.println("-------- LOGS -------");
  }

  private void registerHandlers(HttpServer server) {
    System.out.print("Registering handlers");
    //File Handlers
    server.createContext("/", new FileRequestHandler());
    server.createContext("/css/main.css", new FileRequestHandler());
    server.createContext("/favicon.ico", new FileRequestHandler());
    //API Handlers
    server.createContext("/clear",  new ClearHandler());
    server.createContext("/load", new LoadHandler());
    server.createContext("/fill", new FillHandler());
    server.createContext("/user/register", new RegisterHandler());
    server.createContext("/user/login", new LoginHandler());
    server.createContext("/event", new EventHandler());
    server.createContext("/person", new PersonHandler());
    System.out.print("... Done!\n");
  }

  private void startDatabase(){
    Database db = new Database();

    try {
      db.openConnection();
      System.out.print("Creating tables");
      db.createTables();
      db.closeConnection(true);
    } catch (DataAccessException e) {
      e.printStackTrace();
      try {
        db.closeConnection(false);
      } catch (DataAccessException e1) {
        e1.printStackTrace();
      }
    }

    System.out.print("... Tables created!\n");
  }
}
