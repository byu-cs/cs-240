package dataAccess;

import exception.DataAccessException;
import model.AuthToken;
import model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Handles database communication with the AuthToken table
 */
public class AuthTokenDAO {

  private Connection conn;

  public AuthTokenDAO(Connection conn) {
    this.conn = conn;
  }

  /**
   * Creates a token in the db with the given parameters
   * @param user the user associated with the token
   * @param token the token string
   * @return the authToken
   * @throws DataAccessException if something goes wrong with the query execution
   */
  public AuthToken insert(User user, AuthToken token) throws DataAccessException {
    String sql = "INSERT INTO AuthToken (TokenID, AssociatedUserName, Token, PersonID, DateCreated) " +
        "VALUES (?,?,?,?,?);";
    try (PreparedStatement stmt = conn.prepareStatement(sql)){
      stmt.setString(1, token.getTokenID());
      stmt.setString(2, user.getUserName());
      stmt.setString(3, token.getToken());
      stmt.setString(4, user.getPersonID());
      stmt.setDouble(5, token.getDateCreated());

      if (stmt.executeUpdate() == 1)
      {
        return token;
      }
    } catch (SQLException e) {
      e.printStackTrace();
      throw new DataAccessException("Error while inserting token into database");
    }

    return null;
  }

  /**
   * Queries the database to find a specific token
   * @param token the token string
   * @return the AuthToken object
   * @throws DataAccessException when there is an error in the SQL execution
   */
  public AuthToken find(String token) throws DataAccessException {
    AuthToken tokenObj;
    ResultSet res = null;
    String sql = "SELECT * FROM AuthToken where Token = ?;";
    try (PreparedStatement stmt = conn.prepareStatement(sql)){
      stmt.setString(1, token);
      res = stmt.executeQuery();

      if(res.next()){
        tokenObj = new AuthToken(
            res.getString("token"),
            res.getString("tokenID"),
            res.getDouble("dateCreated"),
            res.getString("associatedUserName"),
            res.getString("personID")
        );
        return tokenObj;
      }
    } catch (SQLException e){
      e.printStackTrace();
      throw new DataAccessException("Error encountered while finding User");
    } finally {
      if(res != null)
        try{
          res.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
    }

    return null;
  }
}