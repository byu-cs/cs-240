package dataAccess;

import exception.DataAccessException;
import model.Person;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Handles database communication with the Person table
 */
public class PersonDAO {
  private Connection conn;

  public PersonDAO(Connection conn){
    this.conn = conn;
  }

  /**
   * Inserts a person into the database
   * @param person the person to be inserted
   * @return a boolean stating the status of the query
   * @throws DataAccessException if the query fails
   */
  public boolean insert(Person person) throws DataAccessException {
    boolean commit = true;
    String sql = "INSERT INTO person (" +
        "PersonID, AssociatedUserName, " +
        "firstName, lastName, " +
        "Gender, FatherID, " +
        "MotherID, SpouseID)" +
        "VALUES (?,?,?,?,?,?,?,?)";
    try (PreparedStatement stmt = conn.prepareStatement(sql)){
      stmt.setString(1, person.getPersonID());
      stmt.setString(2, person.getAssociatedUsername());
      stmt.setString(3, person.getfirstName());
      stmt.setString(4, person.getlastName());
      stmt.setString(5, person.getGender());
      stmt.setString(6, person.getFatherID());
      stmt.setString(7, person.getMotherID());
      stmt.setString(8, person.getSpouseID());

      stmt.executeUpdate();
    } catch (SQLException e) {
      commit = false;
      e.printStackTrace();
      throw new DataAccessException("Error encountered while inserting into Person Table");
    }

    return commit;
  }

  /**
   * Finds a person by ID
   * @param personID the id of the person
   * @return the person object if found
   * @throws DataAccessException if the query fails
   */
  public Person find(String personID) throws DataAccessException {
    Person person;
    ResultSet res = null;
    String sql = "SELECT * FROM person where personid = ?;";
    try (PreparedStatement stmt = conn.prepareStatement(sql)){
      stmt.setString(1, personID);
      res = stmt.executeQuery();

      if(res.next()){
        person = new Person(
            res.getString("personid"),
            res.getString("associateduserName"),
            res.getString("firstName"),
            res.getString("lastName"),
            res.getString("gender"),
            res.getString("fatherid"),
            res.getString("motherid"),
            res.getString("spouseid")
        );
        return person;
      }
    } catch (SQLException e){
      e.printStackTrace();
      throw new DataAccessException("Error encountered while finding Person");
    } finally {
      if(res != null)
        try {
          res.close();
        } catch (SQLException e){
          e.printStackTrace();
        }
    }

    return null;
  }

  /**
   * Finds a specific person by associated username and person id
   * @param userName the associated username
   * @param personID the personID to look up
   * @return the person object if found
   * @throws DataAccessException if the query fails
   */
  public Person findByID(String userName, String personID) throws DataAccessException {
    Person person;
    ResultSet res = null;
    String sql = "SELECT * FROM person where AssociatedUserName = ? and PersonID = ?;";
    try (PreparedStatement stmt = conn.prepareStatement(sql)){
      stmt.setString(1, userName);
      stmt.setString(2, personID);
      res = stmt.executeQuery();

      if(res.next()){
        person = new Person(
            res.getString("personid"),
            res.getString("associateduserName"),
            res.getString("firstName"),
            res.getString("lastName"),
            res.getString("gender"),
            res.getString("fatherid"),
            res.getString("motherid"),
            res.getString("spouseid")
        );
        return person;
      }
    } catch (SQLException e){
      e.printStackTrace();
      throw new DataAccessException("Error encountered while finding Person");
    } finally {
      if(res != null)
        try {
          res.close();
        } catch (SQLException e){
          e.printStackTrace();
        }
    }

    return null;
  }

  /**
   * Finds all the people associated with the given username
   * @param userName the associated username
   * @return an array of persons associated with the username
   * @throws DataAccessException if the query fails
   */
  public ArrayList<Person> findForUserName(String userName) throws DataAccessException {
    ArrayList<Person> personList = new ArrayList<Person>();
    ResultSet res = null;
    String sql = "SELECT * FROM person where AssociatedUserName = ?";
    try (PreparedStatement stmt = conn.prepareStatement(sql)){
      stmt.setString(1, userName);
      res = stmt.executeQuery();

      while(res.next()){
        Person person = new Person(
            res.getString("personid"),
            res.getString("associateduserName"),
            res.getString("firstName"),
            res.getString("lastName"),
            res.getString("gender"),
            res.getString("fatherid"),
            res.getString("motherid"),
            res.getString("spouseid")
        );
        personList.add(person);
      }

      return personList;
    } catch (SQLException e){
      e.printStackTrace();
      throw new DataAccessException("Error encountered while finding Person");
    } finally {
      if(res != null)
        try {
          res.close();
        } catch (SQLException e){
          e.printStackTrace();
        }
    }
  }

  /**
   * Queries all persons in the database
   * @return an array of persons
   * @throws DataAccessException if the query fails
   */
  public ArrayList<Person> queryAll() throws DataAccessException {
    ArrayList<Person> personList = new ArrayList<>();
    ResultSet res = null;
    Person person = null;
    String sql = "SELECT * FROM person;";
    try (PreparedStatement stmt = conn.prepareStatement(sql)){
      res = stmt.executeQuery();

      while(res.next()){
        person = new Person(
          res.getString("personid"),
          res.getString("associateduserName"),
          res.getString("firstName"),
          res.getString("lastName"),
          res.getString("gender"),
          res.getString("fatherid"),
          res.getString("motherid"),
          res.getString("spouseid")
        );

        personList.add(person);
      }

      if(personList.size() > 0) return personList;
    } catch (SQLException e) {
      personList = null;
      e.printStackTrace();
      throw new DataAccessException("Error encountered while finding all Persons");
    } finally {
      if(res != null)
        try {
          res.close();
        } catch (SQLException e){
          e.printStackTrace();
        }
    }

    return null;
  }

  /**
   * clears the table
   * @return a boolean stating the status
   * @throws DataAccessException if the query fails
   */
    public boolean deleteAll() throws DataAccessException {
    boolean commit = true;
    String sql = "DELETE FROM person";
    try (PreparedStatement stmt = conn.prepareStatement(sql)){
      stmt.executeUpdate();
    } catch (SQLException e) {
      commit = false;
      throw new DataAccessException("Error encountered while deleting all Persons");
    }

    return commit;
  }

  /**
   * Clears the persons associated with a username
   * @param userName the associated username
   * @return a boolean stating the status of the query
   * @throws DataAccessException if the query fails
   */
  public boolean clearUserAncestors(String userName) throws DataAccessException {
    boolean commit = true;
    String sql = "DELETE FROM person where associatedUserName = ?";
    try (PreparedStatement stmt = conn.prepareStatement(sql)){
      stmt.setString(1, userName);
      stmt.executeUpdate();
    } catch (SQLException e) {
      commit = false;
      throw new DataAccessException("Error encountered while deleting all Persons");
    }

    return commit;
  }
}
