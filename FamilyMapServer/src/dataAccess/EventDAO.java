package dataAccess;

import exception.DataAccessException;
import model.Event;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Handles database communication with the Event table
 */
public class EventDAO {
  Connection conn = null;

  public EventDAO(Connection conn) {
    this.conn = conn;
  }

  /**
   * Inserts an event into the database
   * @param event the event to be inserted
   * @return a boolean stating the status of the query
   * @throws DataAccessException when query encounters an error
   */
  public boolean insert(Event event) throws DataAccessException {
    boolean commit = true;
    String sql = "INSERT INTO event (eventID, personID, associatedUserName, " +
        "latitude, longitude, country, " +
        "city, eventType, year) " +
        "VALUES (?,?,?,?,?,?,?,?,?);";

    try(PreparedStatement stmt = conn.prepareStatement(sql)) {
      stmt.setString(1, event.getEventID());
      stmt.setString(2, event.getPersonID());
      stmt.setString(3, event.getAssociatedUsername());
      stmt.setDouble(4, event.getLatitude());
      stmt.setDouble(5, event.getLongitude());
      stmt.setString(6, event.getCountry());
      stmt.setString(7, event.getCity());
      stmt.setString(8, event.getEventType());
      stmt.setString(9, event.getYear());

      stmt.executeUpdate();
    }
    catch (SQLException e) {
      commit = false;
      e.printStackTrace();
      throw new DataAccessException("Error encountered while inserting into Event Table");
    }

    return commit;
  }

  /**
   * Finds an event by username and eventID
   * @param userName the associated username
   * @param eventID the event to query
   * @return the event
   * @throws DataAccessException if the query fails
   */
  public Event findByID(String userName, String eventID) throws DataAccessException {
    Event event;
    ResultSet res = null;
    String sql = "SELECT * FROM event where AssociatedUserName = ? and EventID = ?;";
    try (PreparedStatement stmt = conn.prepareStatement(sql)){
      stmt.setString(1, userName);
      stmt.setString(2, eventID);
      res = stmt.executeQuery();

      if(res.next()){
        event = new Event(
            res.getString("eventid"),
            res.getString("personID"),
            res.getString("associatedUserName"),
            res.getDouble("latitude"),
            res.getDouble("longitude"),
            res.getString("country"),
            res.getString("city"),
            res.getString("eventType"),
            res.getString("year")
        );
        return event;
      }
    } catch (SQLException e){
      e.printStackTrace();
      throw new DataAccessException("Error encountered while finding Event");
    } finally {
      if(res != null)
        try {
          res.close();
        } catch (SQLException e){
          e.printStackTrace();
        }
    }

    return null;
  }

  /**
   * Returns an array of events for the associated username
   * @param userName the associated username
   * @return the array of events
   * @throws DataAccessException if the query fails
   */
  public ArrayList<Event> findForUserName(String userName) throws DataAccessException {
    ArrayList<Event> eventList = new ArrayList<Event>();
    ResultSet res = null;
    String sql = "SELECT * FROM event where AssociatedUserName = ?";
    try (PreparedStatement stmt = conn.prepareStatement(sql)){
      stmt.setString(1, userName);
      res = stmt.executeQuery();

      while(res.next()){
        Event event = new Event(
            res.getString("eventid"),
            res.getString("personID"),
            res.getString("associatedUserName"),
            res.getDouble("latitude"),
            res.getDouble("longitude"),
            res.getString("country"),
            res.getString("city"),
            res.getString("eventType"),
            res.getString("year")
        );
        eventList.add(event);
      }

      return eventList;
    } catch (SQLException e){
      e.printStackTrace();
      throw new DataAccessException("Error encountered while finding Event");
    } finally {
      if(res != null)
        try {
          res.close();
        } catch (SQLException e){
          e.printStackTrace();
        }
    }
  }

  /**
   * Clears all of the events associated with a username
   * @param userName the associated username
   * @return a boolean stating the status of the query
   * @throws DataAccessException if the query fails
   */
  public boolean clearUserEvents(String userName) throws DataAccessException {
    boolean commit = true;
    String sql = "DELETE FROM event where associatedUserName = ?";
    try (PreparedStatement stmt = conn.prepareStatement(sql)){
      stmt.setString(1, userName);
      stmt.executeUpdate();
    } catch (SQLException e) {
      commit = false;
      throw new DataAccessException("Error encountered while deleting all Events");
    }

    return commit;
  }
}

