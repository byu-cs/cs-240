package response;

/**
 * Clear result class used to create result (response) objects
 */
public class ClearResponse {
  String message;

  public ClearResponse(String message) {
    this.message = message;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }
}
