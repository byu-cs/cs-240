package response;

/**
 * Login result class used to create result (response) objects
 */
public class LoginResponse {
  private String authToken;
  private String userName;
  private String personID;
  private String message;

  public LoginResponse() { }

  /**
   * Constructor for the Login result object
   * @param authToken "cf7a368f", // Non-empty auth token string
   * @param userName "39f9fe46" // Non-empty string containing the Person ID of the
   * @param personID "susan", // User name passed in with request
   */
  public LoginResponse(String authToken, String userName, String personID) {
    this.authToken = authToken;
    this.userName = userName;
    this.personID = personID;
  }

  public String getAuthToken() {
    return authToken;
  }

  public void setAuthToken(String authToken) {
    this.authToken = authToken;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getPersonID() {
    return personID;
  }

  public void setPersonID(String personID) {
    this.personID = personID;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }
}
