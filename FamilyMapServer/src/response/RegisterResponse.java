package response;

/**
 * Register result class used to create result (response) objects
 */
public class RegisterResponse {
  String authToken;
  String userName;
  String personID;
  String message;

  /**
   * Constructor for the register result object
   * @param authToken Non-empty auth token string
   * @param userName User name passed in with request
   * @param personID Non-empty string containing the Person ID
   */
  public RegisterResponse(String authToken, String userName, String personID) {
    this.authToken = authToken;
    this.userName = userName;
    this.personID = personID;
  }

  public RegisterResponse(String message) {
    this.message = message;
  }

  public RegisterResponse(){}

  public String getAuthToken() {
    return authToken;
  }

  public String getUserName() {
    return userName;
  }

  public String getPersonID() {
    return personID;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }
}
