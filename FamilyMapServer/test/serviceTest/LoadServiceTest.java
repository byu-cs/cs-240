package serviceTest;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import dataAccess.Database;
import exception.DataAccessException;
import exception.LoadException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import request.LoadRequest;
import response.LoadResponse;
import service.LoadService;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;

public class LoadServiceTest {
  Database db;

  LoadRequest request;
  LoadResponse response;

  @BeforeEach
  public void setUp() throws DataAccessException, FileNotFoundException {
    System.out.println("Setting up database and variables");

    db = new Database();
    db.openConnection();
    db.createTables();
    db.closeConnection(true);
  }

  @AfterEach
  public void tearDown() throws DataAccessException{
    System.out.println("Tearing down database");
    db.openConnection();
    db.clearTables();
    db.closeConnection(true);
  }

  @Test
  public void loadPass() throws DataAccessException {
    String message = "Successfully added 1 users, 3 persons, and 2 events to the database.";
    try{
      String separator = System.getProperty("file.separator");
      request = readBody("json" + separator + "example.json");

      LoadService service = new LoadService(db.openConnection());
      response = service.load(request);
      db.closeConnection(true);
    } catch (FileNotFoundException | DataAccessException | LoadException e) {
      e.printStackTrace();
      db.closeConnection(false);
    }

    assertNotNull(response);
    assertEquals(response.getMessage().toLowerCase(), message.toLowerCase());
  }

  @Test
  public void loadFail() throws DataAccessException {
    try{
      String separator = System.getProperty("file.separator");
      request = readBody("json" + separator + "duplicateError.json");

      LoadService service = new LoadService(db.openConnection());
      response = service.load(request);
      db.closeConnection(true);
    } catch (FileNotFoundException | DataAccessException | LoadException e) {
      e.printStackTrace();
      if(db.isOpen()) db.closeConnection(false);
      assertNull(response);
      assertTrue(e.getMessage().toLowerCase().contains("error"));
    }
  }

  @Test
  public void badJSON() throws DataAccessException {
    try{
      String separator = System.getProperty("file.separator");
      request = readBody("json" + separator + "badexample.json");

      LoadService service = new LoadService(db.openConnection());
      response = service.load(request);
      db.closeConnection(true);
    } catch (FileNotFoundException | DataAccessException | LoadException | JsonSyntaxException e) {
      e.printStackTrace();
      if(db.isOpen()) db.closeConnection(false);
      assertNull(response);
    }
  }

  private LoadRequest readBody(String path) throws FileNotFoundException {
    Reader reader = null;
    LoadRequest fromFile;
    Gson gson = new Gson();

    //Surnames
    reader = new FileReader(path);
    fromFile = gson.fromJson(reader, LoadRequest.class);

    return fromFile;
  }
}
