package serviceTest;

import dataAccess.Database;
import exception.DataAccessException;
import exception.RegisterException;
import model.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import request.RegisterRequest;
import response.RegisterResponse;
import service.RegisterService;


public class RegisterServiceTest {
  Database db;

  RegisterRequest request;
  RegisterResponse response;

  User user;

  @BeforeEach
  public void setUp() throws DataAccessException {
    System.out.println("Setting up database and variables");

    user = new User("lopze94",
        "password", "email",
        "josue", "lopez",
        "m", "josueID");

    db = new Database();
    db.openConnection();
    db.createTables();
    db.closeConnection(true);
  }

  @AfterEach
  public void tearDown() throws DataAccessException{
    System.out.println("Tearing down database");
    db.openConnection();
    db.clearTables();
    db.closeConnection(true);
  }

  @Test
  public void registerPass() throws DataAccessException {
    try{
      request = new RegisterRequest(user.getUserName(), user.getPassword(), user.getEmail(), user.getfirstName(), user.getlastName(), user.getGender());
      RegisterService service = new RegisterService(db.openConnection());
      response = service.register(request);

      db.closeConnection(true);
    } catch (DataAccessException | RegisterException e) {
      e.printStackTrace();
      db.closeConnection(false);
    }

    assertNotNull(response);
    assertEquals(response.getAuthToken().length(), 36);
    assertEquals(response.getUserName(), "lopze94");
  }

  @Test
  public void registerFail() throws DataAccessException {
    try{
      request = new RegisterRequest(null, user.getPassword(), user.getEmail(), user.getfirstName(), user.getlastName(), user.getGender());
      RegisterService service = new RegisterService(db.openConnection());
      response = service.register(request);

      db.closeConnection(true);
    } catch (DataAccessException | RegisterException e) {
      e.printStackTrace();
      db.closeConnection(false);
      assertNull(response);
      assertTrue(e.getMessage().toLowerCase().contains("error"));
    }
  }

  @Test
  public void doubleRegistering() throws DataAccessException {
    try{
      request = new RegisterRequest(user.getUserName(), user.getPassword(), user.getEmail(), user.getfirstName(), user.getlastName(), user.getGender());
      RegisterService service = new RegisterService(db.openConnection());
      response = service.register(request);
      response = service.register(request);

      db.closeConnection(true);
    } catch (DataAccessException | RegisterException e) {
      e.printStackTrace();
      db.closeConnection(false);
      assertTrue(e.getMessage().toLowerCase().contains("already exists error"));
    }
  }
}
