package serviceTest;

import dataAccess.Database;
import dataAccess.UserDAO;
import exception.DataAccessException;
import exception.LoginException;
import model.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import request.LoginRequest;
import response.LoginResponse;
import service.LoginService;

import java.sql.Connection;

public class LoginServiceTest {
  Database db;
  User user;

  LoginRequest request;
  LoginResponse response;


  @BeforeEach
  public void setUp() throws DataAccessException {
    System.out.println("Setting up database and variables");

    user = new User("lopze94",
        "password", "email",
        "josue", "lopez",
        "m", "personID");

    db = new Database();
    db.openConnection();
    db.createTables();
    db.closeConnection(true);
  }

  @AfterEach
  public void tearDown() throws DataAccessException{
    System.out.println("Tearing down database");
    db.openConnection();
    db.clearTables();
    db.closeConnection(true);
  }

  @Test
  public void loginPass() throws DataAccessException {
    try{
      Connection conn = db.openConnection();
      UserDAO uDAO = new UserDAO(conn);

      uDAO.insert(user);

      request = new LoginRequest("lopze94", "password");
      LoginService service = new LoginService(conn);
      response = service.login(request);

      db.closeConnection(true);
    } catch (DataAccessException | LoginException e) {
      e.printStackTrace();
      db.closeConnection(false);
    }

    assertNotNull(response);
    assertEquals(response.getAuthToken().length(), 36);
    assertEquals(response.getUserName(), "lopze94");
  }

  @Test
  public void loginFail() throws DataAccessException {
    try{
      Connection conn = db.openConnection();
      UserDAO uDAO = new UserDAO(conn);

      uDAO.insert(user);

      request = new LoginRequest("lopze94", "wrongPwd");
      LoginService service = new LoginService(conn);
      response = service.login(request);

      db.closeConnection(true);
    } catch (DataAccessException | LoginException e) {
      e.printStackTrace();
      db.closeConnection(false);
      assertTrue(e.getMessage().toLowerCase().contains("error"));
    }

    assertNull(response);
  }
}
