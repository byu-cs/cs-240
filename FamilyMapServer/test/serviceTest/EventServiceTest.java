package serviceTest;

import dataAccess.Database;
import dataAccess.EventDAO;
import exception.DataAccessException;
import exception.EventException;
import model.Event;
import model.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import request.EventRequest;
import response.EventResponse;
import service.EventService;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.UUID;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class EventServiceTest {
  Database db;

  private Event birth;
  private Event marriage;
  private Event death;

  private EventRequest request;
  private EventResponse response;

  User user = new User("lopze94");

  ArrayList<Event> data = new ArrayList<>();

  @BeforeEach
  public void setUp() throws DataAccessException {
    System.out.println("Setting up database and variables");
    String personID = UUID.randomUUID().toString();

    user.setPersonID("josueID");

    birth = new Event("birthID", user.getPersonID(),
        user.getUserName(), 1234567890, 1234567890,
        "Guatemala", "Guatemala", "birth", "1994");

    marriage = new Event("marriageID", user.getPersonID(),
        user.getUserName(), 1234567890, 1234567890,
        "Guatemala", "Guatemala", "marriage", "2015");

    death = new Event("deathID", user.getPersonID(),
        user.getUserName(), 1234567890, 1234567890,
        "Guatemala", "Guatemala", "death", "2019");

    data.add(birth);
    data.add(marriage);
    data.add(death);

    db = new Database();
    db.openConnection();
    db.createTables();
    db.closeConnection(true);
  }

  @AfterEach
  public void tearDown() throws DataAccessException{
    System.out.println("Tearing down database");
    db.openConnection();
    db.clearTables();
    db.closeConnection(true);
  }

  @Test //no event ID specified. just find all for token
  public void findForUsernamePass() throws DataAccessException {
    request = new EventRequest(user.getUserName(), null);
    response = new EventResponse(data);

    EventResponse compareTest = null;
    try{
      Connection conn = db.openConnection();
      insertEvents(data, conn);
      EventService service = new EventService(conn);
      compareTest = service.find(request);
      db.closeConnection(true);
    } catch (DataAccessException | EventException e) {
      e.printStackTrace();
      db.closeConnection(false);
    }

    assertNotNull(compareTest);
    assertEquals(response, compareTest);
    assertEquals(response.getData().size(), compareTest.getData().size());

  }

  @Test //no event ID specified. just find all for token
  public void findForUsernameFail() throws DataAccessException {
    request = new EventRequest("fakeUsername", null);
    response = new EventResponse(data);

    EventResponse compareTest = null;
    try{
      Connection conn = db.openConnection();
      insertEvents(data, conn);
      EventService service = new EventService(conn);
      compareTest = service.find(request);
      db.closeConnection(true);
    } catch (DataAccessException | EventException e) {
      e.printStackTrace();
      db.closeConnection(false);
    }

    assertNotNull(compareTest);
    assertEquals(compareTest.getData().size(), 0);
  }

  @Test //no event ID specified. just find all for token
  public void findEventIDPass() throws DataAccessException {
    request = new EventRequest(user.getUserName(), "birthID");
    ArrayList<Event> events = new ArrayList<Event>();
    events.add(birth);
    response = new EventResponse(events);

    EventResponse compareTest = null;
    try{
      Connection conn = db.openConnection();
      insertEvents(data, conn);
      EventService service = new EventService(conn);
      compareTest = service.find(request);
      db.closeConnection(true);
    } catch (DataAccessException | EventException e) {
      e.printStackTrace();
      db.closeConnection(false);
    }

    assertNotNull(compareTest);
    assertEquals(response, compareTest);
    assertEquals(response.getData().size(), compareTest.getData().size());
    assertEquals(compareTest.getData().size(), 1);
  }

  @Test //no event ID specified. just find all for token
  public void findEventIDFail() throws DataAccessException {
    request = new EventRequest(user.getUserName(), "InvalidID");
    ArrayList<Event> events = new ArrayList<Event>();
    events.add(birth);
    response = new EventResponse(events);

    EventResponse compareTest = null;
    try{
      Connection conn = db.openConnection();
      insertEvents(data, conn);
      EventService service = new EventService(conn);
      compareTest = service.find(request);
      db.closeConnection(true);
    } catch (DataAccessException | EventException e) {
      e.printStackTrace();
      db.closeConnection(false);
      assertEquals(e.getMessage(), "Event not found error");
    }

    assertNull(compareTest);
  }


  private void insertEvents(ArrayList<Event> events, Connection conn) throws DataAccessException {
    try{
      EventDAO eDAO = new EventDAO(conn);
      for(Event event : events){
        eDAO.insert(event);
      }
    } catch (DataAccessException e) {
      e.printStackTrace();
      throw new DataAccessException("failed to insert");
    }
  }
}
