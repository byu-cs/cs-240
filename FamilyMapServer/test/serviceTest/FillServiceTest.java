package serviceTest;

import dataAccess.Database;
import dataAccess.PersonDAO;
import dataAccess.UserDAO;
import exception.DataAccessException;
import exception.FillException;
import model.Person;
import model.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import request.FillRequest;
import response.FillResponse;
import service.FillService;

import java.sql.Connection;

public class FillServiceTest {
  Database db;

  FillRequest request;
  FillResponse response;


  @BeforeEach
  public void setUp() throws DataAccessException {
    System.out.println("Setting up database and variables");
    db = new Database();
    db.openConnection();
    db.createTables();
    db.closeConnection(true);
  }

  @AfterEach
  public void tearDown() throws DataAccessException{
    System.out.println("Tearing down database");
    db.openConnection();
    db.clearTables();
    db.closeConnection(true);
  }

  @Test
  public void fillPass(){
    String message = "Successfully added 31 persons and 91 events to the database.";
    try{
      Connection conn = db.openConnection();
      insertUser(conn);

      request = new FillRequest("lopze94", 4);
      FillService service = new FillService(conn);
      response = service.fill(request);

      db.closeConnection(true);
    } catch (DataAccessException | FillException e) {
      e.printStackTrace();
    }

    assertNotNull(response);
    assertEquals(response.getPersonsTotal(), 31);
    assertEquals(response.getEventsTotal(), 91);
    assertEquals(response.getMessage(), message);
  }

  @Test
  public void fillFail() throws DataAccessException {
    String message = "Successfully added 31 persons and 91 events to the database.";
    try{
      Connection conn = db.openConnection();
      insertUser(conn);

      request = new FillRequest("wrongUsername", 4);
      FillService service = new FillService(conn);
      response = service.fill(request);

      db.closeConnection(true);
    } catch (DataAccessException | FillException e) {
      e.printStackTrace();
      db.closeConnection(false);
      assertNull(response);
      assertTrue(e.getMessage().toLowerCase().contains("error"));
    }
  }

  private void insertUser(Connection conn) throws DataAccessException {
    try{
      UserDAO uDAO = new UserDAO(conn);
      PersonDAO pDAO = new PersonDAO(conn);

      Person person = new Person("lopze94", "Josue",
          "Lopez", "m");

      User user = new User(person.getAssociatedUsername(),
          "password", "email",
          person.getfirstName(), person.getlastName(),
          person.getGender(), person.getPersonID());

      uDAO.insert(user);
      pDAO.insert(person);
    } catch (DataAccessException e) {
      e.printStackTrace();
      throw new DataAccessException("Error creating fake user");
    }

  }
}
