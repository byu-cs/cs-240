package serviceTest;

import dataAccess.Database;
import dataAccess.PersonDAO;
import exception.DataAccessException;
import exception.PersonException;
import model.Person;
import model.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import request.PersonRequest;
import response.PersonResponse;
import service.PersonService;

import java.sql.Connection;
import java.util.ArrayList;


public class PersonServiceTest {
  Database db;

  private Person mom;
  private Person dad;

  private PersonRequest request;
  private PersonResponse response;

  User user = new User("lopze94");

  ArrayList<Person> data = new ArrayList<>();

  @BeforeEach
  public void setUp() throws DataAccessException {
    System.out.println("Setting up database and variables");
    user.setPersonID("josueID");

    mom = new Person("momID","lopze94", "Hilda",
        "Zepeda", "f", "dadID", "momID", "spouseID");

    dad = new Person("dadID", "lopze94", "Abinady",
        "Lopez", "m", "dadID", "momID", "spouseID");

    data.add(mom);
    data.add(dad);

    db = new Database();
    db.openConnection();
    db.createTables();
    db.closeConnection(true);
  }

  @AfterEach
  public void tearDown() throws DataAccessException{
    System.out.println("Tearing down database");
    db.openConnection();
    db.clearTables();
    db.closeConnection(true);
  }

  @Test //no person ID specified. just find all for token
  public void findForUsernamePass() throws DataAccessException {
    request = new PersonRequest(user.getUserName(), null);
    response = new PersonResponse(data);

    PersonResponse compareTest = null;
    try{
      Connection conn = db.openConnection();
      insertPersons(data, conn);
      PersonService service = new PersonService(conn);
      compareTest = service.find(request);
      db.closeConnection(true);
    } catch (DataAccessException | PersonException e) {
      e.printStackTrace();
      db.closeConnection(false);
    }

    assertNotNull(compareTest);
    assertEquals(response, compareTest);
    assertEquals(response.getData().size(), compareTest.getData().size());

  }

  @Test //no person ID specified. just find all for token
  public void findForUsernameFail() throws DataAccessException {
    request = new PersonRequest("fakeUsername", null);
    response = new PersonResponse(data);

    PersonResponse compareTest = null;
    try{
      Connection conn = db.openConnection();
      insertPersons(data, conn);
      PersonService service = new PersonService(conn);
      compareTest = service.find(request);
      db.closeConnection(true);
    } catch (DataAccessException | PersonException e) {
      e.printStackTrace();
      db.closeConnection(false);
    }

    assertNotNull(compareTest);
    assertEquals(compareTest.getData().size(), 0);
  }

  @Test //no person ID specified. just find all for token
  public void findPersonIDPass() throws DataAccessException {
    request = new PersonRequest(user.getUserName(), "momID");
    ArrayList<Person> persons = new ArrayList<Person>();
    persons.add(mom);
    response = new PersonResponse(persons);

    PersonResponse compareTest = null;
    try{
      Connection conn = db.openConnection();
      insertPersons(data, conn);
      PersonService service = new PersonService(conn);
      compareTest = service.find(request);
      db.closeConnection(true);
    } catch (DataAccessException | PersonException e) {
      e.printStackTrace();
      db.closeConnection(false);
    }

    assertNotNull(compareTest);
    assertEquals(response, compareTest);
    assertEquals(response.getData().size(), compareTest.getData().size());
    assertEquals(compareTest.getData().size(), 1);
  }

  @Test //no person ID specified. just find all for token
  public void findPersonIDFail() throws DataAccessException {
    request = new PersonRequest(user.getUserName(), "InvalidID");
    ArrayList<Person> persons = new ArrayList<Person>();
    persons.add(mom);
    response = new PersonResponse(persons);

    PersonResponse compareTest = null;
    try{
      Connection conn = db.openConnection();
      insertPersons(data, conn);
      PersonService service = new PersonService(conn);
      compareTest = service.find(request);
      db.closeConnection(true);
    } catch (DataAccessException | PersonException e) {
      e.printStackTrace();
      db.closeConnection(false);
      assertEquals(e.getMessage(), "Person not found error");
    }

    assertNull(compareTest);
  }


  private void insertPersons(ArrayList<Person> persons, Connection conn) throws DataAccessException {
    try{
      PersonDAO pDAO = new PersonDAO(conn);
      for(Person person : persons){
        pDAO.insert(person);
      }
    } catch (DataAccessException e) {
      e.printStackTrace();
      throw new DataAccessException("failed to insert");
    }
  }
}
