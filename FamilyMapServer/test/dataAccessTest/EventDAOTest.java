package dataAccessTest;

import dataAccess.Database;
import dataAccess.EventDAO;
import exception.DataAccessException;
import model.Event;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;



public class EventDAOTest {
  Database db;

  Event birth;
  Event marriage;
  Event death;

  @BeforeEach
  public void setUp() throws DataAccessException{
    System.out.println("Setting up database and variables");

    birth = new Event("birthID", "personID",
        "lopze94", 1234567890, 1234567890,
        "Guatemala", "Guatemala", "birth", "1994");

    marriage = new Event("marriageID", "personID",
        "lopze94", 1234567890, 1234567890,
        "Guatemala", "Guatemala", "marriage", "2015");

    death = new Event("deathID", "personID",
        "lopze94", 1234567890, 1234567890,
        "Guatemala", "Guatemala", "death", "2019");

    db = new Database();
    db.openConnection();
    db.createTables();
    db.closeConnection(true);
  }

  @AfterEach
  public void tearDown() throws DataAccessException {
    System.out.println("Tearing down database");
    db.openConnection();
    db.clearTables();
    db.closeConnection(true);
  }

  @Test
  public void insertPass() throws DataAccessException {
    Event compareTest = null;
    try{
      EventDAO eDAO = new EventDAO(db.openConnection());
      eDAO.insert(birth);

      compareTest = eDAO.findByID(birth.getAssociatedUsername(), birth.getEventID());

      db.closeConnection(true);
    } catch (DataAccessException e) {
      db.closeConnection(false);
      e.printStackTrace();
    }

    assertNotNull(compareTest);
    assertEquals(birth, compareTest);
  }

  @Test
  public void insertFail() throws DataAccessException {
    boolean didItWork = true;
    try{
      EventDAO eDAO = new EventDAO(db.openConnection());
      eDAO.insert(birth);
      eDAO.insert(birth);

      db.closeConnection(true);
    } catch (DataAccessException e) {
      db.closeConnection(false);
      didItWork = false;
    }

    assertFalse(didItWork);
  }

  @Test
  public void findByIDPass() throws DataAccessException {
    Event compareTest = null;
    try{
      EventDAO eDAO = new EventDAO(db.openConnection());
      eDAO.insert(birth);

      compareTest = eDAO.findByID(birth.getAssociatedUsername(), birth.getEventID());

      db.closeConnection(true);
    } catch (DataAccessException e) {
      db.closeConnection(false);
      e.printStackTrace();
    }

    assertNotNull(compareTest);
    assertEquals(birth, compareTest);
  }

  @Test
  public void findByIDFail() throws DataAccessException {
    Event compareTest = null;
    try{
      EventDAO eDAO = new EventDAO(db.openConnection());
      eDAO.insert(birth);
      eDAO.insert(marriage);

      compareTest = eDAO.findByID(birth.getAssociatedUsername(), "fakeID");

      db.closeConnection(true);
    } catch (DataAccessException e) {
      db.closeConnection(false);
      e.printStackTrace();
    }

    assertNull(compareTest);
  }

  @Test
  public void findForUsernamePass() throws DataAccessException {
    ArrayList<Event> list = null;
    try{
      EventDAO eDAO = new EventDAO(db.openConnection());

      eDAO.insert(birth);
      eDAO.insert(marriage);
      eDAO.insert(death);
      
      list = eDAO.findForUserName(birth.getAssociatedUsername());

      db.closeConnection(true);
    } catch (DataAccessException e) {
      db.closeConnection(false);
      e.printStackTrace();
    }

    assertNotNull(list);
    assertEquals(list.size(), 3);
  }


  @Test
  public void findForUsernameFail() throws DataAccessException {
    ArrayList<Event> list = null;
    try{
      EventDAO eDAO = new EventDAO(db.openConnection());

      eDAO.insert(birth);
      eDAO.insert(marriage);
      eDAO.insert(death);

      list = eDAO.findForUserName(birth.getPersonID());

      db.closeConnection(true);
    } catch (DataAccessException e) {
      db.closeConnection(false);
      e.printStackTrace();
    }

    assertNotNull(list);
    assertNotEquals(list.size(), 3);
  }

  @Test
  public void clearUserEventsPass() throws DataAccessException {
    ArrayList<Event> list = null;
    try{
      EventDAO eDAO = new EventDAO(db.openConnection());

      eDAO.insert(birth);
      eDAO.insert(marriage);
      eDAO.insert(death);

      list = eDAO.findForUserName(birth.getAssociatedUsername());

      db.closeConnection(true);
    } catch (DataAccessException e) {
      db.closeConnection(false);
      e.printStackTrace();
    }

    assertNotNull(list);
    assertEquals(list.size(), 3);

    try{
      EventDAO eDAO = new EventDAO(db.openConnection());

      eDAO.clearUserEvents(birth.getAssociatedUsername());

      list = eDAO.findForUserName(birth.getAssociatedUsername());

      db.closeConnection(true);
    } catch (DataAccessException e) {
      db.closeConnection(false);
      e.printStackTrace();
    }

    assertNotNull(list);
    assertEquals(list.size(), 0);
  }

  @Test
  public void clearUserEventsFail() throws DataAccessException {
    ArrayList<Event> list = null;
    try{
      EventDAO eDAO = new EventDAO(db.openConnection());

      eDAO.insert(birth);
      eDAO.insert(marriage);
      eDAO.insert(death);

      list = eDAO.findForUserName(birth.getAssociatedUsername());

      db.closeConnection(true);
    } catch (DataAccessException e) {
      db.closeConnection(false);
      e.printStackTrace();
    }

    assertNotNull(list);
    assertEquals(list.size(), 3);

    try{
      EventDAO eDAO = new EventDAO(db.openConnection());

      eDAO.clearUserEvents(birth.getPersonID());

      list = eDAO.findForUserName(birth.getAssociatedUsername());

      db.closeConnection(true);
    } catch (DataAccessException e) {
      db.closeConnection(false);
      e.printStackTrace();
    }

    assertNotNull(list);
    assertEquals(list.size(), 3);
  }
}
