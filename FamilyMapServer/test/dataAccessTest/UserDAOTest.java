package dataAccessTest;

import exception.DataAccessException;
import dataAccess.Database;
import dataAccess.UserDAO;
import model.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;


public class UserDAOTest {
  Database db;
  User user;

  @BeforeEach
  public void setUp() throws DataAccessException{
    System.out.println("Setting up database and variables");
    String personID = UUID.randomUUID().toString();
    user = new User(
        "lopze94",
        "password", "test@email.com",
        "Josue", "Lopez",
        "m", personID,
        "This is my history"
    );
    db = new Database();
    db.openConnection();
    db.createTables();
    db.closeConnection(true);
  }

  @AfterEach
  public void tearDown() throws DataAccessException{
    System.out.println("Tearing down database");
    db.openConnection();
    db.clearTables();
    db.closeConnection(true);
  }

  @Test
  public void insertPass() throws DataAccessException {
    User compareTest = null;
    db.openConnection();
    db.clearTables();
    db.closeConnection(true);

    try{
      Connection conn = db.openConnection();
      UserDAO uDAO = new UserDAO(conn);
      uDAO.insert(user);

      compareTest = uDAO.find(user.getUserID());
      db.closeConnection(true);
    } catch (DataAccessException e) {
      e.printStackTrace();
      db.closeConnection(false);
    }

    assertNotNull(compareTest);
    assertEquals(user, compareTest);
  }

  @Test
  public void insertFail() throws DataAccessException{
    boolean didItWork = true;
    try{
      Connection conn = db.openConnection();
      UserDAO uDAO = new UserDAO(conn);

      uDAO.insert(user);
      //Fail to insert duplicate
      uDAO.insert(user);
      db.closeConnection(didItWork);
    } catch (DataAccessException e){
      db.closeConnection(false);
      didItWork = false;
    }

    assertFalse(didItWork);

    User compareTest = user;

    try{
      Connection conn = db.openConnection();
      UserDAO uDAO = new UserDAO(conn);
      compareTest = uDAO.find(user.getUserID());
      db.closeConnection(true);
    } catch (DataAccessException e){
      db.closeConnection(false);
    }

    assertNull(compareTest);
  }

  @Test
  public void findPass() throws DataAccessException {
    User compareTest = null;
    db.openConnection();
    db.clearTables();
    db.closeConnection(true);

    try{
      Connection conn = db.openConnection();
      UserDAO uDAO = new UserDAO(conn);
      uDAO.insert(user);

      compareTest = uDAO.find(user.getUserID());
      db.closeConnection(true);
    } catch (DataAccessException e) {
      e.printStackTrace();
      db.closeConnection(false);
    }

    assertNotNull(compareTest);
    assertEquals(user, compareTest);
  }

  @Test
  public void findFail() throws DataAccessException {
    boolean didItWork = true;
    User compareTest = null;
    db.openConnection();
    db.clearTables();
    db.closeConnection(true);

    try{
      Connection conn = db.openConnection();
      UserDAO uDAO = new UserDAO(conn);
      uDAO.insert(user);

      compareTest = uDAO.find(user.getEmail());
      if(compareTest == null) throw new DataAccessException("User not found");
      db.closeConnection(true);
    } catch (DataAccessException e) {
      e.printStackTrace();
      db.closeConnection(false);
      didItWork = false;
    }

    assertFalse(didItWork);
    assertNull(compareTest);
    assertNotEquals(user, compareTest);
  }

  @Test
  public void deleteAllPass() throws DataAccessException{
    db.openConnection();
    db.clearTables();
    db.closeConnection(true);

    ArrayList<User> list = generateUsers();
    ArrayList<User> result = null;
    Iterator it = list.iterator();
    int dbUsers = 10;
    try{
      Connection conn = db.openConnection();
      UserDAO uDAO = new UserDAO(conn);
      while(it.hasNext()){
        User usr = (User) it.next();
        uDAO.insert(usr);
      }

      result = uDAO.queryAll();
      dbUsers = result.size();
      uDAO.deleteAll();
      result = uDAO.queryAll();
      db.closeConnection(true);
    } catch (DataAccessException e) {
      e.printStackTrace();
      db.closeConnection(false);
    }

    assertEquals(10, dbUsers);
    assertNull(result);
  }

  @Test
  public void deleteAllFail() throws DataAccessException{
    db.openConnection();
    db.clearTables();
    db.closeConnection(true);

    ArrayList<User> list = generateUsers();
    ArrayList<User> result = null;
    Iterator it = list.iterator();
    int dbUsers = 10;
    try{
      Connection conn = db.openConnection();
      UserDAO uDAO = new UserDAO(conn);
      while(it.hasNext()){
        User usr = (User) it.next();
        uDAO.insert(usr);
      }

      result = uDAO.queryAll();
      dbUsers = result.size();
      db.closeConnection(true);
      uDAO.deleteAll();

    } catch (DataAccessException | NullPointerException e) {
      e.printStackTrace();
      Connection conn = db.openConnection();
      UserDAO uDAO = new UserDAO(conn);
      result = uDAO.queryAll();
      db.closeConnection(false);
    }
    assertEquals(dbUsers, result.size());
  }

  @Test
  public void findUsernamePass() throws DataAccessException {
    User compareTest = null;
    try{
      UserDAO uDAO = new UserDAO(db.openConnection());

      uDAO.insert(user);

      compareTest = uDAO.findUsername(user.getUserName());

      db.closeConnection(true);
    } catch (DataAccessException e) {
      e.printStackTrace();
      db.closeConnection(false);
    }

    assertNotNull(compareTest);
    assertEquals(user,compareTest);
  }

  @Test
  public void findUsernameFail() throws DataAccessException {
    User compareTest = null;
    try{
      UserDAO uDAO = new UserDAO(db.openConnection());

      uDAO.insert(user);

      //Query by a different property instead of username
      compareTest = uDAO.findUsername(user.getUserID());

      db.closeConnection(true);
    } catch (DataAccessException e) {
      e.printStackTrace();
      db.closeConnection(false);
    }

    assertNull(compareTest);
  }

  @Test
  public void loginQueryPass() throws DataAccessException {
    User compareTest = null;
    try{
      UserDAO uDAO = new UserDAO(db.openConnection());

      uDAO.insert(user);

      compareTest = uDAO.loginQuery(user.getUserName(), user.getPassword());

      db.closeConnection(true);
    } catch (DataAccessException e) {
      db.closeConnection(false);
      e.printStackTrace();
    }

    assertNotNull(compareTest);
    assertEquals(user,compareTest);
  }

  @Test
  public void loginQueryFail() throws DataAccessException {
    User compareTest = null;
    try{
      UserDAO uDAO = new UserDAO(db.openConnection());

      uDAO.insert(user);

      compareTest = uDAO.loginQuery(user.getUserName(), "Invalid Password");

      db.closeConnection(true);
    } catch (DataAccessException e) {
      db.closeConnection(false);
      e.printStackTrace();
    }

    assertNull(compareTest);
  }

  @Test
  public void queryAllPass() throws DataAccessException {
    ArrayList<User> list = generateUsers();
    ArrayList<User> result = null;
    Iterator it = list.iterator();
    int dbUsers = 10;
    try {
      Connection conn = db.openConnection();
      UserDAO uDAO = new UserDAO(conn);
      while (it.hasNext()) {
        User usr = (User) it.next();
        uDAO.insert(usr);
      }

      result = uDAO.queryAll();
      db.closeConnection(true);
    } catch (DataAccessException e) {
      e.printStackTrace();
      db.closeConnection(false);
    }

    assertNotNull(result);
    assertEquals(list.size(),result.size());
  }

  @Test
  public void queryAllFail() throws DataAccessException {
    ArrayList<User> list = generateUsers();

    //Insert a duplicate users
    list.add(user);
    list.add(user);

    ArrayList<User> result = null;
    Iterator it = list.iterator();
    int dbUsers = 10;
    try {
      Connection conn = db.openConnection();
      UserDAO uDAO = new UserDAO(conn);



      while (it.hasNext()) {
        User usr = (User) it.next();
        uDAO.insert(usr);
      }

      result = uDAO.queryAll();
      db.closeConnection(true);
    } catch (DataAccessException e) {
      e.printStackTrace();
      db.closeConnection(false);
    }

    assertNull(result);
  }

  private ArrayList<User> generateUsers(){
    ArrayList<User> userList = new ArrayList<>();
    for(int i = 0; i < 10; i++){
      String personID = UUID.randomUUID().toString();
      User usr = new User(
          Integer.toString(i),
          "password", "test@email.com",
          "Josue", "Lopez",
          "m", personID
      );
      userList.add(usr);
    }

    return userList;
  }
}