package dataAccessTest;

import dataAccess.AuthTokenDAO;
import dataAccess.Database;
import exception.DataAccessException;
import model.AuthToken;
import model.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class AuthTokenDAOTest {
  Database db;
  AuthToken auth;
  User user;

  @BeforeEach
  public void setUp() throws DataAccessException{
    System.out.print("Setting variables and database... ");

    user = new User("lopze94");
    auth = new AuthToken("fakeToken",
        "123",
        1234567890,
        user.getUserName(),
        user.getPersonID());

    db = new Database();
    db.openConnection();
    db.createTables();
    db.closeConnection(true);
    System.out.print("Done!\n");
  }

  @AfterEach
  public void tearDown() throws DataAccessException{
    System.out.print("Tearing down database... ");
    db.openConnection();
    db.clearTables();
    db.closeConnection(true);
    System.out.print("Done\n");
  }

  @Test
  public void insertPass() throws DataAccessException {
    System.out.print("Testing insert (PASS)... ");
    AuthToken compareTest = null;
    try{
      AuthTokenDAO tDAO = new AuthTokenDAO(db.openConnection());
      tDAO.insert(user, auth);

      compareTest = tDAO.find(auth.getToken());

      db.closeConnection(true);
    } catch (DataAccessException e) {
      e.printStackTrace();
      db.closeConnection(false);
    }

    assertNotNull(compareTest);
    assertEquals(auth, compareTest);
    System.out.print("Done!\n");
  }

  @Test
  public void insertFail() throws DataAccessException {
    System.out.print("Testing insert (FAIL)... ");
    boolean didItWork = true;
    AuthToken compareTest = null;
    try{
      AuthTokenDAO tDAO = new AuthTokenDAO(db.openConnection());
      tDAO.insert(user, auth);
      tDAO.insert(user, auth);

      compareTest = tDAO.find(auth.getToken());

      db.closeConnection(didItWork);
    } catch (DataAccessException e) {
      db.closeConnection(false);
      didItWork = false;
    }

    assertFalse(didItWork);
    System.out.print("Done!\n");
  }

  @Test
  public void findPass() throws DataAccessException{
    System.out.print("Testing find (PASS)... ");
    AuthToken compareTest = null;
    try{
      AuthTokenDAO tDAO = new AuthTokenDAO(db.openConnection());
      tDAO.insert(user, auth);

      compareTest = tDAO.find(auth.getToken());

      db.closeConnection(true);
    } catch (DataAccessException e) {
      e.printStackTrace();
      db.closeConnection(false);
    }

    assertNotNull(compareTest);
    assertEquals(auth, compareTest);
    System.out.print("Done!\n");
  }

  @Test
  public void findFail() throws DataAccessException{
    System.out.print("Testing find (Fail)... ");
    AuthToken compareTest = null;
    try{
      AuthTokenDAO tDAO = new AuthTokenDAO(db.openConnection());
      tDAO.insert(user, auth);

      //Transaction is committed before the find operation is executed
      db.closeConnection(true);

      compareTest = tDAO.find(auth.getToken());

      db.closeConnection(true);
    } catch (DataAccessException | NullPointerException e) {
      e.printStackTrace();
    }

    assertNull(compareTest);
    System.out.print("Done!\n");
  }
}
