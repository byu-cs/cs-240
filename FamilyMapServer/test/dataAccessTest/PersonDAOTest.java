package dataAccessTest;

import exception.DataAccessException;
import dataAccess.Database;
import dataAccess.PersonDAO;
import model.Person;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.UUID;


public class PersonDAOTest {
  Database db;
  Person person;

  @BeforeEach
  public void setUp() throws DataAccessException{
    System.out.println("Setting up database and variables");
    String personID = UUID.randomUUID().toString();
    person = new Person("lopze94", "Josue",
        "Lopez", "m");
    db = new Database();
    db.openConnection();
    db.createTables();
    db.closeConnection(true);
  }

  @AfterEach
  public void tearDown() throws DataAccessException{
    System.out.println("Tearing down database");
    db.openConnection();
    db.clearTables();
    db.closeConnection(true);
  }

  @Test
  public void insertPass() throws DataAccessException {
    Person compareTest = null;
    db.openConnection();
    db.clearTables();
    db.closeConnection(true);

    try{
      Connection conn = db.openConnection();
      PersonDAO pDAO = new PersonDAO(conn);
      pDAO.insert(person);

      compareTest = pDAO.find(person.getPersonID());
      db.closeConnection(true);
    } catch (DataAccessException e) {
      e.printStackTrace();
      db.closeConnection(false);
    }

    assertNotNull(compareTest);
    assertEquals(person, compareTest);
  }

  @Test
  public void insertFail() throws DataAccessException{
    boolean didItWork = true;
    try{
      Connection conn = db.openConnection();
      PersonDAO pDAO = new PersonDAO(conn);

      pDAO.insert(person);
      //Fail to insert duplicate
      pDAO.insert(person);
      db.closeConnection(didItWork);
    } catch (DataAccessException e){
      db.closeConnection(false);
      didItWork = false;
    }

    assertFalse(didItWork);

    Person compareTest = person;

    try{
      Connection conn = db.openConnection();
      PersonDAO pDAO = new PersonDAO(conn);
      compareTest = pDAO.find(person.getPersonID());
      db.closeConnection(true);
    } catch (DataAccessException e){
      db.closeConnection(false);
    }

    assertNull(compareTest);
  }

  @Test
  public void findPass() throws DataAccessException {
    Person compareTest = null;
    db.openConnection();
    db.clearTables();
    db.closeConnection(true);

    try{
      Connection conn = db.openConnection();
      PersonDAO pDAO = new PersonDAO(conn);
      pDAO.insert(person);

      compareTest = pDAO.find(person.getPersonID());
      db.closeConnection(true);
    } catch (DataAccessException e) {
      e.printStackTrace();
      db.closeConnection(false);
    }

    assertNotNull(compareTest);
    assertEquals(person, compareTest);
  }

  @Test
  public void findFail() throws DataAccessException {
    boolean didItWork = true;
    Person compareTest = null;
    db.openConnection();
    db.clearTables();
    db.closeConnection(true);

    try{
      Connection conn = db.openConnection();
      PersonDAO pDAO = new PersonDAO(conn);
      pDAO.insert(person);

      compareTest = pDAO.find(person.getAssociatedUsername());
      if(compareTest == null) throw new DataAccessException("Person not found");
      db.closeConnection(true);
    } catch (DataAccessException e) {
      e.printStackTrace();
      db.closeConnection(false);
      didItWork = false;
    }

    assertFalse(didItWork);
    assertNull(compareTest);
    assertNotEquals(person, compareTest);
  }

  @Test
  public void deleteAllPass() throws DataAccessException{
    db.openConnection();
    db.clearTables();
    db.closeConnection(true);

    ArrayList<Person> list = generatePersons();
    ArrayList<Person> result = null;
    Iterator it = list.iterator();
    int dbPersons = 10;
    try{
      Connection conn = db.openConnection();
      PersonDAO pDAO = new PersonDAO(conn);
      while(it.hasNext()){
        Person prsn = (Person) it.next();
        pDAO.insert(prsn);
      }

      result = pDAO.queryAll();
      dbPersons = result.size();
      pDAO.deleteAll();
      result = pDAO.queryAll();
      db.closeConnection(true);
    } catch (DataAccessException e) {
      e.printStackTrace();
      db.closeConnection(false);
    }

    assertEquals(10, dbPersons);
    assertNull(result);
  }

  @Test
  public void deleteAllFail() throws DataAccessException{
    db.openConnection();
    db.clearTables();
    db.closeConnection(true);

    ArrayList<Person> list = generatePersons();
    ArrayList<Person> result = null;
    Iterator it = list.iterator();
    int dbPersons = 10;
    try{
      Connection conn = db.openConnection();
      PersonDAO pDAO = new PersonDAO(conn);
      while(it.hasNext()){
        Person usr = (Person) it.next();
        pDAO.insert(usr);
      }

      result = pDAO.queryAll();
      dbPersons = result.size();
      db.closeConnection(true);
      pDAO.deleteAll();

    } catch (DataAccessException | NullPointerException e) {
      e.printStackTrace();
      Connection conn = db.openConnection();
      PersonDAO uDAO = new PersonDAO(conn);
      result = uDAO.queryAll();
      db.closeConnection(false);
    }

    assertEquals(dbPersons, result.size());
  }

  public ArrayList<Person> generatePersons(){
    ArrayList<Person> personList = new ArrayList<>();
    for(int i = 0; i < 10; i++){
      String personID = UUID.randomUUID().toString();
      Person prsn = new Person(Integer.toString(i), "Josue",
          "Lopez", "m");
      personList.add(prsn);
    }

    return personList;
  }

  @Test
  public void clearUserEventsPass() throws DataAccessException {
    ArrayList<Person> list = null;
    try{
      PersonDAO pDAO = new PersonDAO(db.openConnection());

      pDAO.insert(person);
      person = new Person("lopze94", "Abinady",
          "Lopez", "m");
      pDAO.insert(person);
      person = new Person("lopze94", "Hilda",
          "Zepeda", "f");
      pDAO.insert(person);

      list = pDAO.findForUserName("lopze94");

      db.closeConnection(true);
    } catch (DataAccessException e) {
      db.closeConnection(false);
      e.printStackTrace();
    }

    assertNotNull(list);
    assertEquals(list.size(), 3);

    try{
      PersonDAO pDAO = new PersonDAO(db.openConnection());

      pDAO.clearUserAncestors("lopze94");

      list = pDAO.findForUserName("lopze94");

      db.closeConnection(true);
    } catch (DataAccessException e) {
      db.closeConnection(false);
      e.printStackTrace();
    }

    assertNotNull(list);
    assertEquals(list.size(), 0);
  }

  @Test
  public void clearUserEventsFail() throws DataAccessException {
    ArrayList<Person> list = null;
    try{
      PersonDAO pDAO = new PersonDAO(db.openConnection());

      pDAO.insert(person);
      person = new Person("lopze94", "Abinady",
          "Lopez", "m");
      pDAO.insert(person);
      person = new Person("lopze94", "Hilda",
          "Zepeda", "f");
      pDAO.insert(person);

      list = pDAO.findForUserName("lopze94");

      db.closeConnection(true);
    } catch (DataAccessException e) {
      db.closeConnection(false);
      e.printStackTrace();
    }

    assertNotNull(list);
    assertEquals(list.size(), 3);

    try{
      PersonDAO pDAO = new PersonDAO(db.openConnection());

      //Typo in the username
      pDAO.clearUserAncestors("lopez94");

      list = pDAO.findForUserName("lopze94");

      db.closeConnection(true);
    } catch (DataAccessException e) {
      db.closeConnection(false);
      e.printStackTrace();
    }

    assertNotNull(list);
    assertEquals(list.size(), 3);
  }

  @Test
  public void findByIDPass() throws DataAccessException {
    Person compareTest = null;
    try{
      PersonDAO pDAO = new PersonDAO(db.openConnection());

      pDAO.insert(person);

      compareTest = pDAO.findByID(person.getAssociatedUsername(), person.getPersonID());

      db.closeConnection(true);
    } catch (DataAccessException e) {
      db.closeConnection(false);
      e.printStackTrace();
    }

    assertNotNull(compareTest);
    assertEquals(person, compareTest);
  }

  @Test
  public void findByIDFail() throws DataAccessException {
    Person compareTest = null;
    try{
      PersonDAO pDAO = new PersonDAO(db.openConnection());

      pDAO.insert(person);

      compareTest = pDAO.findByID(person.getAssociatedUsername(),"TotatllyDifferentID");

      db.closeConnection(true);
    } catch (DataAccessException e) {
      db.closeConnection(false);
      e.printStackTrace();
    }

    assertNull(compareTest);
  }

  @Test
  public void findForUsernamePass() throws DataAccessException {
    ArrayList<Person> list = null;
    try{
      PersonDAO pDAO = new PersonDAO(db.openConnection());

      pDAO.insert(person);
      person = new Person("lopze94", "Abinady",
          "Lopez", "m");
      pDAO.insert(person);
      person = new Person("lopze94", "Hilda",
          "Zepeda", "f");
      pDAO.insert(person);

      list = pDAO.findForUserName("lopze94");

      db.closeConnection(true);
    } catch (DataAccessException e) {
      db.closeConnection(false);
      e.printStackTrace();
    }

    assertNotNull(list);
    assertEquals(list.size(), 3);
  }

  @Test
  public void findForUsernameFail() throws DataAccessException {
    ArrayList<Person> list = null;
    try{
      PersonDAO pDAO = new PersonDAO(db.openConnection());

      pDAO.insert(person);
      person = new Person("lopze94", "Abinady",
          "Lopez", "m");
      pDAO.insert(person);
      person = new Person("lopze94", "Hilda",
          "Zepeda", "f");
      pDAO.insert(person);

      //Typo in username
      list = pDAO.findForUserName("lopez94");

      db.closeConnection(true);
    } catch (DataAccessException e) {
      db.closeConnection(false);
      e.printStackTrace();
    }

    assertNotNull(list);
    assertEquals(list.size(), 0);
  }

  @Test
  public void queryAllPass() throws DataAccessException {
    ArrayList<Person> list = null;
    try{
      PersonDAO pDAO = new PersonDAO(db.openConnection());

      pDAO.insert(person);
      person = new Person("anotherUser", "Abinady",
          "Lopez", "m");
      pDAO.insert(person);
      person = new Person("differentUser", "Hilda",
          "Zepeda", "f");
      pDAO.insert(person);

      list = pDAO.queryAll();
    } catch (DataAccessException e) {
      e.printStackTrace();
      db.closeConnection(false);
    }

    assertNotNull(list);
    assertEquals(list.size(), 3);
  }

  @Test
  public void queryAllFail() throws DataAccessException {
    ArrayList<Person> list = null;
    try{
      PersonDAO pDAO = new PersonDAO(db.openConnection());

      pDAO.insert(person);

      person = new Person("anotherUser", "Abinady",
          "Lopez", "m");
      pDAO.insert(person);

      person = new Person("differentUser", "Hilda",
          "Zepeda", "f");
      pDAO.insert(person);

      //insert duplicate person
      pDAO.insert(person);


      list = pDAO.queryAll();
    } catch (DataAccessException e) {
      e.printStackTrace();
      db.closeConnection(false);
    }

    assertNull(list);
  }
}