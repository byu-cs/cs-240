package dataAccessTest;

import dataAccess.Database;
import exception.DataAccessException;
import org.junit.jupiter.api.Test;
;
import static org.junit.jupiter.api.Assertions.*;

public class DatabaseTest {
  Database db;

  @Test
  public void openConnectionPass() throws DataAccessException {
    db = new Database();
    db.openConnection();

    assertTrue(db.isOpen());

    db.closeConnection(true);
  }

  @Test
  public void closeConnectionPass() throws DataAccessException {
    db = new Database();
    db.openConnection();

    assertTrue(db.isOpen());

    db.closeConnection(true);

    assertFalse(db.isOpen());
  }

  @Test
  public void createTablesPass() throws DataAccessException {
    db = new Database();
    db.openConnection();
    boolean didItWork = db.createTables();
    db.closeConnection(true);

    assertTrue(didItWork);
  }

  @Test
  public void clearTablesPass() throws DataAccessException {
    db = new Database();
    db.openConnection();
    boolean didItWork = db.clearTables();
    db.closeConnection(true);

    assertTrue(didItWork);
  }
}
