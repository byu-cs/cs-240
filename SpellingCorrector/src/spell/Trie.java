package spell;

import java.util.Arrays;
import java.util.Objects;

public class Trie implements ITrie {
	private TrieNode root;
	private int trieWordCount;
	private int trieNodeCount;
	private char a = 'a';
	private char z = 'z';

	public Trie() {
		root = new TrieNode();
		trieWordCount = 0;
		trieNodeCount = 1;
	}

	/**
	 * Adds a word to a Trie
	 * @param newWord The word being added to the trie
	 */
	@Override
	public void add(String newWord) {
		newWord = newWord.toLowerCase();
		if(checkDuplicate(newWord)) return;

		TrieNode currentNode = root;

		int i = 0;
		while (i < newWord.length()) {
			char currChar = newWord.charAt(i);
			if (Objects.isNull(currentNode.getCharIndex(currChar))) {
				currentNode.setCharIndex(currChar, new TrieNode());
				trieNodeCount++;
			}
			currentNode = currentNode.getCharIndex(currChar);
			i++;
		}
		currentNode.frequency++;
		trieWordCount++;
	}

	/**
	 * Checks if a word is a duplicate word. Increments frequency
	 * @param word a word to be checked
	 * @return true if duplicate
	 */
	private boolean checkDuplicate(String word){
		TrieNode currentNode = (TrieNode) find(word);
		if (Objects.isNull(currentNode)) {
			return false;
		}
		currentNode.frequency++;
		return true;
	}

	/**
	 * Checks if a word is in the Trie
	 * @param lookupWord a word to look for in the Trie
	 * @return null or the word reference if found
	 */
	@Override
	public INode find(String lookupWord) {
		TrieNode currentNode = root;

		for (int i = 0; i < lookupWord.length(); i++) {
			char wordChar = lookupWord.charAt(i);
			if (Objects.isNull(currentNode.getCharIndex(wordChar))) return null;
			currentNode = currentNode.getCharIndex(wordChar);
		}

		return currentNode.frequency == 0 ? null : currentNode;
	}

	@Override public int getWordCount() { return trieWordCount; }

	@Override public int getNodeCount() { return trieNodeCount; }

	/**
	 * Creates a string of every word in the Trie and returns it.
	 * @return a string with the words in the Trie
	 */
	@Override
	public String toString() {
		StringBuilder currentWord = new StringBuilder();
		StringBuilder result = new StringBuilder();

		return toStringRecursion(currentWord, result, root).toString();
	}

	private StringBuilder toStringRecursion(StringBuilder currentWord, StringBuilder currentResult, TrieNode currentNode) {
		if (currentNode == null) { return currentResult; }

		if (currentNode.getValue() > 0) {
			currentResult.append(currentWord);
			currentResult.append('\n');
		}

		for (char currentChar = a; currentChar <= z; currentChar++) {
			currentWord.append(currentChar);
			toStringRecursion(currentWord, currentResult, currentNode.getCharIndex(currentChar));
			currentWord.setLength(currentWord.length() - 1);
		}
		return currentResult;
	}

	/**
	 * Creates a hash code for the object
	 * @return a number
	 */
	@Override
	public int hashCode() { return hashCodeRecursion(root); }

	private int hashCodeRecursion(TrieNode start) {
		if (Objects.isNull(start)) return 1;
		int total = 0;
		for (int currentChar = a; currentChar <= z; currentChar++) {
			total += currentChar * hashCodeRecursion(start.getCharIndex((char)currentChar));
			total = total * getWordCount();
		}
		return total;
	}

	/**
	 * Checks if two objects are equal
	 * @param wordTrie an object to be compared with
	 * @return boolean
	 */
	@Override
	public boolean equals(Object wordTrie) {
		if (!(wordTrie instanceof Trie)) { return false; }

		Trie word = (Trie) wordTrie;

		boolean sameWordCount = word.getWordCount() == getWordCount();
		boolean sameNodeCount = word.getNodeCount() == getNodeCount();
		boolean sameNodes = equalsRecursion(word.root, root);
		return sameWordCount && sameNodeCount && sameNodes;
	}

	private boolean equalsRecursion(TrieNode wordRoot, TrieNode start) {
		if (Objects.isNull(wordRoot) || Objects.isNull(start)) return true;

		if (wordRoot.frequency != start.frequency) return false;

		for (char currChar = a; currChar <= z; currChar++) if (!equalsRecursion(wordRoot.getCharIndex(currChar), start.getCharIndex(currChar))) return false;

		return true;
	}

	public static class TrieNode implements INode {
		//Variables
		private TrieNode[] trieNodes = new TrieNode[26];
		private char a = 'a';
		int frequency;

		//Constructor
		TrieNode() {
			frequency = 0;
			Arrays.fill(trieNodes, null);
		}

		//Getters
		TrieNode getCharIndex(char character) { return trieNodes[character - a]; }
		@Override public int getValue() { return frequency; }

		//Setters
		void setCharIndex(char character, TrieNode word) { trieNodes[character - a] = word; }
	}

}
