package spell;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import spell.Trie.TrieNode;

public class SpellCorrector implements ISpellCorrector {
  private Trie dictionary;
  private char a = 'a';
  private char z = 'z';

  public SpellCorrector() {
    dictionary = new Trie();
  }

  @Override
  public void useDictionary(String dictionaryFilename) throws IOException {
    File dictionaryFile = new File(dictionaryFilename);
    FileReader dictionaryReader = new FileReader(dictionaryFile);
    BufferedReader bufferedReader = new BufferedReader(dictionaryReader);
    Scanner dictionaryScanner = new Scanner(bufferedReader);

    while (dictionaryScanner.hasNext()) {
      String word = dictionaryScanner.next();
      dictionary.add(word);
    }
  }

  @Override
  public String suggestSimilarWord(String userWord) {
    userWord = userWord.toLowerCase();
    if (wordFrequency(userWord) > 0) return userWord;

    List<String> firstEdit = wordEditor(userWord);
    StrRef bestCandidateWord = new StrRef();
    IntRef greatestFrequency = new IntRef();
    IntRef tempCount = new IntRef();
    StrRef tempWord = new StrRef();

    evaluateWords(firstEdit, greatestFrequency, bestCandidateWord, tempCount, tempWord);
    if (greatestFrequency.value > 0) return bestCandidateWord.value;

    for (String editedWord : firstEdit) {
      List<String> secondEdit = wordEditor(editedWord);
      evaluateWords(secondEdit, greatestFrequency, bestCandidateWord, tempCount, tempWord);
    }
    if (greatestFrequency.value > 0) return bestCandidateWord.value;
    else return null;
  }

  public static class IntRef { int value;}
  public static class StrRef { String value;}

  private void evaluateWords(List<String> wordEdits, IntRef greatestValue, StrRef bestCandidate, IntRef tempCount, StrRef tempWord) {
    for (String word : wordEdits) {
      tempWord.value = word;
      if ((tempCount.value = wordFrequency(tempWord.value)) > 0) {
        if (tempCount.value > greatestValue.value) {
          greatestValue.value = tempCount.value;
          bestCandidate.value = tempWord.value;
        } else if (tempCount.value == greatestValue.value && tempWord.value.compareTo(bestCandidate.value) < 0)
          bestCandidate.value = tempWord.value;
      }
    }
  }

  private List<String> wordEditor(String word) {
    List<String> editsList = new ArrayList<String>();
    int length = word.length();

    deletion(editsList, word, length);
    alteration(editsList, word, length);
    insertion(editsList, word, length);

    return editsList;
  }

  private void deletion(List<String> editsList, String word, int length) {
    for (int i = 0; i < length; i++)
      if (i > 0) {
        editsList.add(word.substring(0, i) + word.substring(i + 1));
        editsList.add(word.substring(0, i - 1) + word.charAt(i) + (word.charAt(i - 1) + word.substring(i + 1)));
      } else editsList.add(word.substring(i + 1));
  }

  private void alteration(List<String> editsList, String word, int length) {
    for (int i = 0; i < length; i++)
      for (char alteredChar = a; alteredChar <= z; alteredChar++)
        editsList.add(word.substring(0, i) + alteredChar + word.substring(i + 1));
  }

  private void insertion(List<String> editsList, String word, int length) {
    for (int i = 0; i <= length; i++)
      for (char insertedChar = a; insertedChar <= z; insertedChar++)
        editsList.add(word.substring(0, i) + insertedChar + word.substring(i));
  }

  private int wordFrequency(String word) {
    TrieNode trieNode = (TrieNode) dictionary.find(word);
    return trieNode == null ? 0 : trieNode.getValue();
  }
}
