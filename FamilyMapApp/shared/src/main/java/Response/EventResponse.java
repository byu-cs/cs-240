package Response;

import Model.Event;
import java.util.ArrayList;
import java.util.Objects;

/**
 * Clear result class used to create result (response) objects
 */
public class EventResponse {
  /**
   * Variable size array of Event objects
   */
  private ArrayList<Event> data = null;
  /**
   * A string containing the message from the server
   */
  public EventResponse(ArrayList<Event> data) {
    this.data = data;
  }

  public ArrayList<Event> getData() {
    return data;
  }

  public void setData(ArrayList<Event> data) {
    this.data = data;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof EventResponse)) return false;
    EventResponse that = (EventResponse) o;
    return Objects.equals(getData(), that.getData());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getData());
  }
}
