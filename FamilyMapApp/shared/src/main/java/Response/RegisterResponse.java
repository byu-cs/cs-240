package Response;

import java.util.Objects;

/**
 * Register result class used to create result (response) objects
 */
public class RegisterResponse {
  String authToken;
  String userName;
  String personID;
  String message;

  /**
   * Constructor for the register result object
   * @param authToken Non-empty auth token string
   * @param userName User name passed in with request
   * @param personID Non-empty string containing the Person ID
   */
  public RegisterResponse(String authToken, String userName, String personID) {
    this.authToken = authToken;
    this.userName = userName;
    this.personID = personID;
  }

  public RegisterResponse(String message) {
    this.message = message;
  }

  public RegisterResponse(){}

  public String getAuthToken() {
    return authToken;
  }

  public String getUserName() {
    return userName;
  }

  public String getPersonID() {
    return personID;
  }

  public String getMessage() {
    return message;
  }

  public void setAuthToken(String authToken) {
    this.authToken = authToken;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public void setPersonID(String personID) {
    this.personID = personID;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    RegisterResponse that = (RegisterResponse) o;
    return authToken.equals(that.authToken) &&
        userName.equals(that.userName) &&
        personID.equals(that.personID);
  }

  @Override
  public int hashCode() {
    return Objects.hash(authToken, userName, personID);
  }

  @Override
  public String toString() {
    return "RegisterResponse{" +
        "authToken='" + authToken + '\'' +
        ", userName='" + userName + '\'' +
        ", personID='" + personID + '\'' +
        '}';
  }
}
