package Response;

/**
 * Load result class used to create result (response) objects
 */
public class LoadResponse {
  private String message;

  public LoadResponse(int usersTotal, int personsTotal, int eventsTotal) {
    this.message = "Successfully added "+ usersTotal +
        " users, "+ personsTotal +
        " persons, and "+ eventsTotal +
        " events to the database.";
  }

  public LoadResponse(String message) {
    this.message = message;
  }

  public String getMessage() { return message; }

  public void setMessage(String message) { this.message = message; }
}
