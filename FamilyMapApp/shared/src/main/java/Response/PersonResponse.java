package Response;

import Model.Person;
import java.util.ArrayList;
import java.util.Objects;

/**
 * Person result class used to create result (response) objects
 */
public class PersonResponse {
  /**
   * Variable size array of Person objects
   */
  private ArrayList<Person> data = null;

  public PersonResponse(ArrayList<Person> data) {
    this.data = data;
  }

  public ArrayList<Person> getData() {
    return data;
  }

  public void setData(ArrayList<Person> data) {
    this.data = data;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof PersonResponse)) return false;
    PersonResponse that = (PersonResponse) o;
    return Objects.equals(getData(), that.getData());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getData());
  }
}
