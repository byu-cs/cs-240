package Response;

public class FillResponse {
  private transient int personsTotal;
  private transient int eventsTotal;
  private String message;

  public FillResponse(int personsTotal, int eventsTotal) {
    this.personsTotal = personsTotal + 1;
    this.eventsTotal = eventsTotal;
    this.message = "Successfully added "+ this.personsTotal +" persons and "+this.eventsTotal +" events to the database.";
  }

  public FillResponse() {
  }

  public int getPersonsTotal() {
    return personsTotal;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public int getEventsTotal() {
    return eventsTotal;
  }
}
