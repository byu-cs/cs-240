package Request;

/**
 * Login request class used to create request objects
 */
public class LoginRequest {
  /**
   * "susan", // Non-empty string
   */
  String userName;
  /**
   * "mysecret" // Non-empty string
   */
  String password;

  String serverHost;

  String serverPort;

  public LoginRequest(String userName, String password) {
    this.userName = userName;
    this.password = password;
  }

  public LoginRequest() { }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getServerHost() {
    return serverHost;
  }

  public void setServerHost(String serverHost) {
    this.serverHost = serverHost;
  }

  public String getServerPort() {
    return serverPort;
  }

  public void setServerPort(String serverPort) {
    this.serverPort = serverPort;
  }

  @Override
  public String toString() {
    return "LoginRequest{" +
        "userName='" + userName + '\'' +
        ", password='" + password + '\'' +
        ", serverHost='" + serverHost + '\'' +
        ", serverPort='" + serverPort + '\'' +
        '}';
  }
}
