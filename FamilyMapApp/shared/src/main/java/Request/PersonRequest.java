package Request;

/**
 * Person request class used to create request objects
 */
public class PersonRequest {
  String username;
  String personID;
  transient String token;
  transient String serverHost;
  transient String serverPort;

  /**
   * Constructor for the person request object
   * @param username authentication username passed in header
   * @param personID id for the current person
   */
  public PersonRequest(String username, String personID) {
    this.username = username;
    this.personID = personID;
  }

  public PersonRequest(String username, String personID, String token, String serverHost, String serverPort) {
    this.username = username;
    this.personID = personID;
    this.token = token;
    this.serverHost = serverHost;
    this.serverPort = serverPort;
  }

  public String getUsername() {
    return username;
  }

  public String getPersonID() {
    return personID;
  }

  public void setPersonID(String personID) { this.personID = personID; }

  public String getToken() { return token; }

  public String getServerHost() { return serverHost; }

  public String getServerPort() { return serverPort; }
}
