package Request;

/**
 * Event request class used to create request objects
 */
public class EventRequest {
  String username;
  String eventID;
  transient String token;
  transient String serverHost;
  transient String serverPort;

  /**
   * Constructor for the event request object
   * @param username authentication username passed in header
   * @param eventID id for the current event
   */
  public EventRequest(String username, String eventID) {
    this.username = username;
    this.eventID = eventID;
  }

  public EventRequest(String username, String eventID, String token, String serverHost, String serverPort) {
    this.username = username;
    this.eventID = eventID;
    this.token = token;
    this.serverHost = serverHost;
    this.serverPort = serverPort;
  }

  public String getToken() { return token; }

  public String getUsername() { return username; }

  public String getEventID() { return eventID; }

  public String getServerHost() { return serverHost; }

  public String getServerPort() { return serverPort; }
}
