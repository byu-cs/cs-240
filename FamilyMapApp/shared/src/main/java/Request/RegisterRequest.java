package Request;

/**
 * Register request class used to create request objects
 */
public class RegisterRequest {
  String userName;
  String password;
  String email;
  String firstName;
  String lastName;
  String gender = "";
  String serverHost;
  String serverPort;

  /**
   * Constructor for the object
   * @param userName   "susan", // Non-empty string
   * @param password   "secret", // Non-empty string
   * @param email   "susan@gmail.com", // Non-empty string
   * @param firstName   "Susan", // Non-empty string
   * @param lastName   "Ellis", // Non-empty string
   * @param gender   "f" // "f" or "m"
   */
  public RegisterRequest(String userName, String password, String email, String firstName, String lastName, String gender) {
    this.userName = userName;
    this.password = password;
    this.email = email;
    this.firstName = firstName;
    this.lastName = lastName;
    this.gender = gender;
  }

  public RegisterRequest() { }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getGender() {
    return gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public String getServerHost() {
    return serverHost;
  }

  public void setServerHost(String serverHost) {
    this.serverHost = serverHost;
  }

  public String getServerPort() {
    return serverPort;
  }

  public void setServerPort(String serverPort) {
    this.serverPort = serverPort;
  }

  @Override
  public String toString() {
    return "RegisterRequest{" +
        "userName='" + userName + '\'' +
        ", password='" + password + '\'' +
        ", email='" + email + '\'' +
        ", firstName='" + firstName + '\'' +
        ", lastName='" + lastName + '\'' +
        ", gender='" + gender + '\'' +
        ", serverHost='" + serverHost + '\'' +
        ", serverPort='" + serverPort + '\'' +
        '}';
  }
}
