package Request;

import Model.User;
import Model.Person;
import Model.Event;

/**
 * Load request class used to create request objects
 */
public class LoadRequest {
  private User users[];
  private Person persons[];
  private Event events[];

  /**
   * Constructor for the request
   * @param users list of users
   * @param persons list of persons
   * @param events list of events
   */
  public LoadRequest(User[] users, Person[] persons, Event[] events) {
    this.users = users;
    this.persons = persons;
    this.events = events;
  }

  public User[] getUsers() {
    return users;
  }

  public Person[] getPersons() {
    return persons;
  }

  public Event[] getEvents() {
    return events;
  }
}
