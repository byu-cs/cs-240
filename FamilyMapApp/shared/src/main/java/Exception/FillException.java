package Exception;

public class FillException extends Exception{
    public FillException(String message){
        super(message);
    }
}
