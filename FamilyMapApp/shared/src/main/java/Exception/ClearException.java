package Exception;

public class ClearException extends Exception{
    public ClearException(String message){
        super(message);
    }
}
