package JSON;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Handles JSON to load and send data
 */
public class JSON {

  private Gson gson;

  public JSON() { gson = new Gson(); }

  /**
   * Encodes an object to JSON
   * @param result the class to encode
   * @return the JSON version of the object
   */
  public String encode(Object result) {
    return gson.toJson(result);
  }

  /**
   * Decodes the data of the exchange
   * @param exchange the exchange object
   * @param objClass the class to be decoded to
   * @return object with specified class
   */
  public Object decodeExchange(HttpExchange exchange, Class objClass) {
    InputStream is = exchange.getRequestBody();
    BufferedReader reader = new BufferedReader(new InputStreamReader(is));

    return gson.fromJson(reader,objClass);
  }

  /**
   * Decodes a JSON string/file
   * @param toDecode the string to decode
   * @param objClass the class to convert to
   * @return the object of specified class with the data
   */
  public Object decode(String toDecode, Class objClass) {
    return gson.fromJson(toDecode,objClass);
  }
}

