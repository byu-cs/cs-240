package Model;

public class Names {
  String[] female = null;
  String[] male = null;
  String[] surname = null;

  public String[] getFemale() {
    return female;
  }

  public void setFemale(String[] female) {
    this.female = female;
  }

  public String[] getMale() {
    return male;
  }

  public void setMale(String[] male) {
    this.male = male;
  }

  public String[] getSurname() {
    return surname;
  }

  public void setSurname(String[] surname) {
    this.surname = surname;
  }

  public static class Female {
    String[] data = null;
    public Female(String[] data) { this.data = data; }

    public String[] getData() { return data; }
    public void setData(String[] data) { this.data = data; }
  }

  public static class Male {
    String[] data = null;
    public Male(String[] data) { this.data = data; }

    public String[] getData() { return data; }
    public void setData(String[] data) { this.data = data; }
  }

  public static class Surname {
    String[] data = null;
    public Surname(String[] data) { this.data = data; }

    public String[] getData() { return data; }
    public void setData(String[] data) { this.data = data; }
  }
}
