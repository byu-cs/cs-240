package Model;

import java.util.HashMap;

public class PersonData {
  Person person;
  HashMap<String, Event> personEvents;

  public PersonData(Person person) {
    this.person = person;
    this.personEvents = new HashMap<>();
  }

  public Person getPerson() {
    return person;
  }

  public void setPerson(Person person) {
    this.person = person;
  }

  public HashMap<String, Event> getPersonEvents() {
    return personEvents;
  }

  public void setPersonEvents(HashMap<String, Event> personEvents) {
    this.personEvents = personEvents;
  }
}
