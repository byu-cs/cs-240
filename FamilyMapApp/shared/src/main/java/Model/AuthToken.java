package Model;

import java.util.Objects;
import java.util.UUID;

public class AuthToken {

    private String token;
    private String tokenID;
    private double dateCreated;
    private String associatedUserName;
    private String personID;

    /**
     * Constructs an AuthToken object used for user session and accepts no parameters
     * the constructor method will generate a token and record the time of creation.
     */
    public AuthToken()
    {
        tokenID = UUID.randomUUID().toString();
        token = UUID.randomUUID().toString();
        dateCreated = System.currentTimeMillis();
    }

    public AuthToken(String token, String tokenID, double dateCreated, String associatedUserName, String personID) {
        this.token = token;
        this.tokenID = tokenID;
        this.dateCreated = dateCreated;
        this.associatedUserName = associatedUserName;
        this.personID = personID;
    }

    public String getTokenID() { return tokenID; }

    public double getDateCreated() {
        return dateCreated;
    }

    public String getToken() {
        return token ;
    }

    public String getAssociatedUserName() { return associatedUserName; }

    public String getPersonID() { return personID; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AuthToken)) return false;
        AuthToken authToken = (AuthToken) o;
        return Double.compare(authToken.getDateCreated(), getDateCreated()) == 0 &&
            Objects.equals(getToken(), authToken.getToken()) &&
            Objects.equals(getTokenID(), authToken.getTokenID()) &&
            Objects.equals(getAssociatedUserName(), authToken.getAssociatedUserName()) &&
            Objects.equals(getPersonID(), authToken.getPersonID());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getToken(), getTokenID(), getDateCreated(), getAssociatedUserName(), getPersonID());
    }

    @Override
    public String toString() {
        return "AuthToken{" +
            "token='" + token + '\'' +
            ", tokenID='" + tokenID + '\'' +
            ", dateCreated=" + dateCreated +
            ", associatedUserName='" + associatedUserName + '\'' +
            ", personID='" + personID + '\'' +
            '}';
    }
}
