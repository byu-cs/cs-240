package DataAccessObject;

import Exception.DataAccessException;

import java.sql.*;

public class Database {
  private Connection conn;
  private boolean isOpen = false;

  static {
    try {
      final String driver = "org.sqlite.JDBC";
      Class.forName(driver);
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
  }

  public boolean isOpen() {
    return isOpen;
  }

  /**
   * Opens database connection
   * @return returns the connection
   * @throws DataAccessException if something goes wrong while connecting to the DB
   */
  public Connection openConnection() throws DataAccessException {
    if(isOpen) {
      System.out.println("Database already open.");
      return conn;
    }
    try {
      final String CONNECTION_URL = "jdbc:sqlite:familymap.sqlite";
      conn = DriverManager.getConnection(CONNECTION_URL);
      conn.setAutoCommit(false);
      isOpen = true;
    } catch (SQLException e) {
      e.printStackTrace();
      throw new DataAccessException("Error encountered while opening connection to database");
    }

//    System.out.println("DB open");
    return conn;
  }

  /**
   * Closes the conneciton
   * @param commit the option to commit or rollback changes
   * @throws DataAccessException when there is an error in the query execution
   */
  public void closeConnection(boolean commit) throws DataAccessException {
    if(!isOpen) throw new DataAccessException("Error encountered while closing the database.");
    try {
      if (commit) {
        conn.commit();
      } else {
        conn.rollback();
      }

      conn.close();
      conn = null;
      isOpen = false;

//      System.out.println("DB closed");
    } catch (SQLException e) {
      e.printStackTrace();
      throw new DataAccessException("Error encountered while closing the database connection");
    }
  }

  /**
   * Creates all of the tables
   * @throws DataAccessException when there is an error in the query execution
   */
  public boolean createTables() throws DataAccessException {
    try (Statement stmt = conn.createStatement()){
      stmt.executeUpdate(
        "CREATE TABLE IF NOT EXISTS User " +
        "(" +
        "UserID text not null unique, " +
        "UserName text not null unique, " +
        "Password text not null, " +
        "Email text not null, " +
        "firstName text not null, " +
        "lastName text not null, " +
        "Gender varchar(1), " +
        "PersonID text not null unique, " +
        "primary key (UserID)," +
        "foreign key (PersonID) references Person(PersonID) "+
        "); "
      );
      stmt.executeUpdate(
        "CREATE TABLE IF NOT EXISTS Person " +
        "(" +
        "PersonID text not null unique, " +
        "AssociatedUserName text, " +
        "firstName text not null, " +
        "lastName text not null, " +
        "Gender varchar(1), " +
        "FatherID text, " +
        "MotherID text, " +
        "SpouseID text, " +
        "primary key (PersonID), " +
        "foreign key (AssociatedUserName) references User(UserName), " +
        "foreign key (FatherID) references Person(PersonID), " +
        "foreign key (MotherID) references Person(PersonID), " +
        "foreign key (SpouseID) references Person(PersonID) " +
        "); "
      );
      stmt.executeUpdate(
        "CREATE TABLE IF NOT EXISTS Event " +
        "(" +
        "EventID text not null unique," +
        "AssociatedUserName text," +
        "PersonID text not null," +
        "Latitude float not null," +
        "Longitude float not null," +
        "Country text not null," +
        "City text not null," +
        "EventType text not null," +
        "Year int not null," +
        "primary key (EventID)," +
        "foreign key (AssociatedUserName) references Users(UserName)," +
        "foreign key (PersonID) references Persons(PersonID)" +
        "); "
      );
      stmt.executeUpdate(
        "CREATE TABLE IF NOT EXISTS AuthToken " +
        "(" +
        "TokenID text not null unique, " +
        "AssociatedUserName text not null," +
        "Token text not null unique," +
        "PersonID text not null," +
        "DateCreated REAL not null," +
        "primary key (TokenID)," +
        "foreign key (AssociatedUserName) references Users(UserName)," +
        "foreign key (PersonID) references Persons(PersonID)" +
        "); "
      );
    } catch (SQLException e) {
      e.printStackTrace();
      throw new DataAccessException("SQL Error encountered while creating tables");
    }
    return true;
  }

  /**
   * Clears all of the tables
   * @throws DataAccessException when there is an error in the sql execution
   */
  public boolean clearTables() throws DataAccessException {
    String sql = "DELETE FROM Person; " +
        "DELETE FROM User; " +
        "DELETE FROM Event; " +
        "DELETE FROM AuthToken; ";
    try (Statement stmt = conn.createStatement()){
      stmt.executeUpdate(sql);
    } catch (SQLException e) {
      throw new DataAccessException("SQL Error encountered while clearing tables");
    }
    return true;
  }
}

