package DataAccessObject;

import Exception.DataAccessException;
import Model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Handles database communication with the User table
 */
public class UserDAO {
  private Connection conn;

  public UserDAO(Connection conn){
    this.conn = conn;
  }

  /**
   * Inserts a user into the database
   * @param user the user to be inserted
   * @return a boolean stating the status of the query
   * @throws DataAccessException if the query fails
   */
  public boolean insert(User user) throws DataAccessException {
    boolean commit = true;
    String sql = "INSERT INTO user ( userid, " +
        "username, password, " +
        "email, firstName, " +
        "lastName, gender, " +
        "personid) " +
        "VALUES (?,?,?,?,?,?,?,?);";

    try (PreparedStatement stmt = conn.prepareStatement(sql)) {
      stmt.setString(1, user.getUserID());
      stmt.setString(2, user.getUserName());
      stmt.setString(3, user.getPassword());
      stmt.setString(4, user.getEmail());
      stmt.setString(5, user.getfirstName());
      stmt.setString(6, user.getlastName());
      stmt.setString(7, user.getGender());
      stmt.setString(8, user.getPersonID());

      stmt.executeUpdate();
    } catch (SQLException e){
      commit = false;
      e.printStackTrace();
      throw new DataAccessException("Error encountered while inserting into the User Table");
    }

    return commit;
  }

  /**
   * Finds a username by ID
   * @param userID the user id
   * @return the user object if found
   * @throws DataAccessException if the query fails
   */
  public User find(String userID) throws DataAccessException {
    User user;
    ResultSet res = null;
    String sql = "SELECT * FROM user where userID = ?;";
    try (PreparedStatement stmt = conn.prepareStatement(sql)){
      stmt.setString(1, userID);
      res = stmt.executeQuery();

      if(res.next()){
        user = new User(
            res.getString("userid"),
            res.getString("userName"),
            res.getString("password"),
            res.getString("email"),
            res.getString("firstName"),
            res.getString("lastName"),
            res.getString("gender"),
            res.getString("personid")
        );
        return user;
      }
    } catch (SQLException e){
      e.printStackTrace();
      throw new DataAccessException("Error encountered while finding User");
    } finally {
      if(res != null)
        try{
          res.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
    }

    return null;
  }

  /**
   * Finds a username by username
   * @param userName the username
   * @return the user object if found
   * @throws DataAccessException if the query fails
   */
  public User findUsername(String userName) throws DataAccessException {
    User user;
    ResultSet res = null;
    String sql = "SELECT * FROM user where userName = ?;";
    try (PreparedStatement stmt = conn.prepareStatement(sql)){
      stmt.setString(1, userName);
      res = stmt.executeQuery();

      if(res.next()){
        user = new User(
            res.getString("userid"),
            res.getString("userName"),
            res.getString("password"),
            res.getString("email"),
            res.getString("firstName"),
            res.getString("lastName"),
            res.getString("gender"),
            res.getString("personid")
        );
        return user;
      }
    } catch (SQLException e){
      e.printStackTrace();
      throw new DataAccessException("Error encountered while finding User");
    } finally {
      if(res != null)
        try{
          res.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
    }

    return null;
  }

  /**
   * Validates credentials
   * @param userName the username for the person logging in
   * @param password the password associated with the username (case sensitive)
   * @return the user object if found
   * @throws DataAccessException if the query fails
   */
  public User loginQuery(String userName, String password) throws DataAccessException {
    User user;
    ResultSet res = null;
    String sql = "SELECT * FROM user where userName = ? and password = ?;";
    try (PreparedStatement stmt = conn.prepareStatement(sql)){
      stmt.setString(1, userName);
      stmt.setString(2, password);
      res = stmt.executeQuery();

      if(res.next()){
        user = new User(
            res.getString("userid"),
            res.getString("userName"),
            res.getString("password"),
            res.getString("email"),
            res.getString("firstName"),
            res.getString("lastName"),
            res.getString("gender"),
            res.getString("personid")
        );
        return user;
      }
    } catch (SQLException e){
      e.printStackTrace();
      throw new DataAccessException("Error encountered while executing login query for User");
    } finally {
      if(res != null)
        try{
          res.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
    }

    return null;
  }

  /**
   * Finds all users in the database
   * @return an array of users
   * @throws DataAccessException if the query fails
   */
  public ArrayList<User> queryAll() throws DataAccessException {
    ArrayList<User> userList = new ArrayList<>();
    ResultSet res = null;
    User user = null;
    String sql = "SELECT * FROM user;";
    try (PreparedStatement stmt = conn.prepareStatement(sql)){
      res = stmt.executeQuery();

      while(res.next()){
        user = new User(
            res.getString("userid"),
            res.getString("userName"),
            res.getString("password"),
            res.getString("email"),
            res.getString("firstName"),
            res.getString("lastName"),
            res.getString("gender"),
            res.getString("personid")
        );

        userList.add(user);
      }
      if(userList.size() > 0) return userList;
    } catch (SQLException e) {
      e.printStackTrace();
      throw new DataAccessException("Error encountered while finding all Users");
    } finally {
      if(res != null)
        try{
          res.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
    }

    return null;
  }

  /**
   * Clears the user table
   * @return a boolean with the status of the query
   * @throws DataAccessException if the query fails
   */
  public boolean deleteAll() throws DataAccessException {
    boolean commit = true;
    String sql = "DELETE FROM User";
    try (PreparedStatement stmt = conn.prepareStatement(sql)){
      stmt.executeUpdate();
    } catch (SQLException e) {
      commit = false;
      throw new DataAccessException("Error encountered while deleting all Users");
    }

    return commit;
  }
}