package Server;

import com.sun.net.httpserver.HttpExchange;
import JSON.JSON;
import Response.ErrorResponse;

import java.io.*;

public class Response {
  private HttpExchange exchange = null;

  public Response(HttpExchange exchange) {
    this.exchange = exchange;
  }

  public String readReqBody() throws IOException {
    InputStream is = exchange.getRequestBody();
    StringBuilder sb = new StringBuilder();
    InputStreamReader sr = new InputStreamReader(is);
    char[] buf = new char[1024];
    int len;
    while ((len = sr.read(buf)) > 0) {
      sb.append(buf, 0, len);
    }
    return sb.toString();
  }

  public void writeResBody(Object obj) throws IOException {
    JSON json = new JSON();
    String resData = json.encode(obj);
    OutputStream os = exchange.getResponseBody();
    OutputStreamWriter sw = new OutputStreamWriter(os);
    BufferedWriter bw = new BufferedWriter(sw);
    bw.write(resData);
    bw.flush();
  }

  public void sendError(int status, Exception e) throws IOException {
    System.out.print("... Failed!\n");
    exchange.sendResponseHeaders(status, 0);
    writeResBody(new ErrorResponse(e.getMessage()));
    exchange.getResponseBody().close();
    e.printStackTrace();
  }
}
