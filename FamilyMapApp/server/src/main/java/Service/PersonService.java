package Service;

import DataAccessObject.PersonDAO;
import Exception.DataAccessException;
import Exception.PersonException;
import Model.Person;
import Request.PersonRequest;
import Response.PersonResponse;

import java.sql.Connection;
import java.util.ArrayList;

public class PersonService {
  private Connection conn = null;

  public PersonService(Connection conn) { this.conn = conn; }

  public PersonResponse find(PersonRequest req) throws PersonException {
    try{
      PersonDAO pDAO = new PersonDAO(conn);
      ArrayList<Person> personList = new ArrayList<Person>();
      String username = req.getUsername();
      if (req.getPersonID() == null || req.getPersonID().equals("")) {
        personList = pDAO.findForUserName(username);
        return new PersonResponse(personList);
      } else {
        Person person = pDAO.findByID(username, req.getPersonID());
        if(person == null) throw new PersonException("Person not found error");
        personList.add(person);
        return new PersonResponse(personList);
      }
    } catch (DataAccessException e) {
      e.printStackTrace();
      throw new PersonException(e.getMessage());
    }
  }
}
