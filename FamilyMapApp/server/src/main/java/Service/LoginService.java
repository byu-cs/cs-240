package Service;

import DataAccessObject.AuthTokenDAO;
import Exception.DataAccessException;
import DataAccessObject.UserDAO;
import Exception.LoginException;
import Model.AuthToken;
import Model.User;
import Request.LoginRequest;
import Response.LoginResponse;

import java.sql.Connection;

public class LoginService {
  private Connection conn = null;

  public LoginService(Connection conn) { this.conn = conn; }

  public LoginResponse login(LoginRequest req) throws LoginException {
    UserDAO uDAO = new UserDAO(conn);
    AuthTokenDAO tDAO = new AuthTokenDAO(conn);
    AuthToken token = new AuthToken();

    User usr = null;
    try {
      usr = uDAO.loginQuery(req.getUserName(), req.getPassword());
      if (usr == null) {
        throw new LoginException("Error matching user and password");
      }

      token = tDAO.insert(usr, token);
      if (token == null) {
        throw new LoginException("Error encountered while creating user");
      }

      return new LoginResponse(token.getToken(), usr.getUserName(), usr.getPersonID());
    } catch (DataAccessException e) {
      e.printStackTrace();
      throw new LoginException(e.getMessage());
    }
  }
}

