package Handler;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.sql.Connection;

import DataAccessObject.Database;
import Exception.DataAccessException;
import Exception.RegisterException;
import Exception.RequestException;
import JSON.JSON;
import Request.RegisterRequest;
import Server.Request;
import Response.RegisterResponse;
import Server.Response;
import Service.RegisterService;

public class RegisterHandler implements HttpHandler {
  private Database db = new Database();
  public RegisterHandler() { }

  @Override
  public void handle(HttpExchange exchange) throws IOException {
    System.out.print("POST: /register");
    JSON json = new JSON();
    Request request = new Request(exchange);
    Response response = new Response(exchange);

    try{
      request.validateMethod("POST");

      RegisterRequest reqBody = (RegisterRequest) request.getRequestBody(RegisterRequest.class);

      validateRequestBody(reqBody);

      //Send request to the service and return the response
      RegisterResponse resBody = executeRequest(reqBody);

      //If no errors are thrown, set the response status to 200
      exchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);

      //Write the data to the response body
      response.writeResBody(resBody);

      //Close the output stream
      System.out.print("Done registering!\n");
      exchange.getResponseBody().close();
    } catch (DataAccessException | RegisterException | RequestException e) {
      response.sendError(HttpURLConnection.HTTP_OK, e);
    }
  }

  private void validateRequestBody(RegisterRequest reqBody) throws RequestException{
    System.out.print("\nValidating request body");

    //Check if request body exists
    if(reqBody == null) throw new RequestException("No request body");

    //Check if required parameters exists
    if(validateRequired(reqBody.getUserName())) throw new RequestException("No userName provided error");
    if(validateRequired(reqBody.getPassword())) throw new RequestException("No password provided error");
    if(validateRequired(reqBody.getFirstName())) throw new RequestException("No firstName provided error");
    if(validateRequired(reqBody.getLastName())) throw new RequestException("No lastName provided error");
    if(validateRequired(reqBody.getEmail())) throw new RequestException("No email provided error");

    //Validate gender input
    if(!(reqBody.getGender() == null ||
        reqBody.getGender().toLowerCase().equals("f") ||
        reqBody.getGender().toLowerCase().equals("m") ||
        reqBody.getGender().equals("")))
      throw new RequestException("Invalid gender error");

    System.out.print("... Valid!\n");
  }

  private boolean validateRequired(String parameter){
    return parameter == null || parameter.equals("");
  }

  private RegisterResponse executeRequest(RegisterRequest req) throws DataAccessException, RegisterException {
    Database db = new Database();
    try{
      Connection conn = db.openConnection();
      RegisterService service = new RegisterService(conn);
      RegisterResponse response = service.register(req);
      db.closeConnection(true);
      return response;
    } catch (DataAccessException | RegisterException e) {
      db.closeConnection(false);
      throw new RegisterException(e.getMessage());
    }
  }
}
