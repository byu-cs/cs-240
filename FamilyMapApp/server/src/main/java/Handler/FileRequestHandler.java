package Handler;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.nio.file.Files;

public class FileRequestHandler implements HttpHandler {

  @Override
  public void handle(HttpExchange exchange) throws IOException {
    String urlPath = exchange.getRequestURI().toString();
    File file = readFile(urlPath);

    exchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);

    if(file.exists()){
      OutputStream outStream = exchange.getResponseBody();
      Files.copy(file.toPath(), outStream);
      outStream.close();
    }
  }

  private File readFile(String urlPath) {
    String sprtr = System.getProperty("file.separator");

    String filePath;
    switch (urlPath) {
      case "/":
        filePath = "web" + sprtr + "index.html";
        break;
      case "/css/main.css":
        filePath = "web" + sprtr + "css" + sprtr + "main.css";
        break;
      case "/favicon.ico":
        filePath = "web" + sprtr + "favicon.ico";
        break;
      default:
        filePath = "web" + sprtr + "HTML" + sprtr + "404.html";
        break;
    }

    return new File(filePath);
  }
}


