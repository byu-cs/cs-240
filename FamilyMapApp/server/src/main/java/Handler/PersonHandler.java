package Handler;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import DataAccessObject.Database;
import Exception.DataAccessException;
import Exception.PersonException;
import Exception.RequestException;
import JSON.JSON;
import Model.AuthToken;
import Request.PersonRequest;
import Server.Request;
import Response.PersonResponse;
import Server.Response;
import Service.PersonService;

import java.io.IOException;
import java.net.HttpURLConnection;

public class PersonHandler implements HttpHandler {
  Database db = new Database();

  public PersonHandler() { }

  @Override
  public void handle(HttpExchange exchange) throws IOException {
    String uri = exchange.getRequestURI().toString();
    System.out.print("GET: " + uri);

    JSON json = new JSON();
    Request request = new Request(exchange);
    Response response = new Response(exchange);

    try{
      request.validateMethod("GET");
      AuthToken authToken = request.validateAuthorization();

      String username = authToken.getAssociatedUserName();
      String personID = parsePersonID(uri);

      PersonRequest req = new PersonRequest(username, personID);

      PersonResponse resBody = executeRequest(req);

      //If no errors are thrown, set the response status to 200
      exchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);

      //Write the data to the response body
      response.writeResBody(personID != null ? resBody.getData().get(0) : resBody);

      //Close the output stream
      System.out.print("... Done!\n");
      exchange.getResponseBody().close();
    } catch (RequestException | PersonException | DataAccessException e) {
      System.out.print("... Failed!\n");
      int status = HttpURLConnection.HTTP_OK;
      response.sendError(status, e);
    }

  }

  private String parsePersonID(String uri){
    String[] params = uri.split("/");
    if(params.length > 2) return params[2];
    return null;
  }

  private PersonResponse executeRequest(PersonRequest req) throws PersonException, DataAccessException {
    try{
      PersonService service = new PersonService(db.openConnection());
      PersonResponse res = service.find(req);
      db.closeConnection(true);
      return res;
    } catch (DataAccessException | PersonException e) {
      db.closeConnection(false);
      throw new PersonException(e.getMessage());
    }
  }


}
