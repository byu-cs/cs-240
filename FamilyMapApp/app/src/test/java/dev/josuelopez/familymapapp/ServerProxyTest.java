package dev.josuelopez.familymapapp;

import android.util.Log;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.URL;
import java.util.UUID;

import Request.RegisterRequest;
import Response.RegisterResponse;
import Server.ServerProxy;
import Server.ServerProxyException;

import static junit.framework.Assert.*;

public class ServerProxyTest {

  private URL testURL;
  private ServerProxy proxy;

  @Before
  public void setUP() throws IOException {
    proxy = new ServerProxy();
    proxy.clearPost();
    try{
      testURL = new URL("http://");
    } catch (Exception e ){
      assertEquals("Throwing exception", e.getMessage());
    }
  }

  @After
  public void tearDown() {
    testURL = null;
    proxy = null;
  }


  @Test
  public void testGetRegisterURL() throws ServerProxyException {
    String username = UUID.randomUUID().toString();
    RegisterRequest request = new RegisterRequest();
    request.setServerPort("8080");
    request.setServerHost("127.0.0.1");
    request.setUserName(username);
    request.setPassword("password");
    request.setGender("m");
    request.setEmail("email");
    request.setFirstName("Joe");
    request.setLastName("Doe");
    try {
      testURL = new URL("http://" + request.getServerHost() + ":" + request.getServerPort() + "/user/register");
    } catch (Exception e){
      assertEquals("Throwing exception", e.getMessage());
    }

    RegisterResponse expectedResponse = new RegisterResponse();
    expectedResponse.setUserName(username);

    RegisterResponse out = proxy.registerPost(testURL.toString(), request);

    expectedResponse.setAuthToken(out.getAuthToken());
    expectedResponse.setPersonID(out.getPersonID());

    System.out.println(out.toString());
    System.out.println(expectedResponse.toString());

    assertEquals(expectedResponse, out);
  }

}

