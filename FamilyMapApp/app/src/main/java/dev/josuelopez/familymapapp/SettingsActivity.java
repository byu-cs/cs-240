package dev.josuelopez.familymapapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;

public class SettingsActivity extends AppCompatActivity {
  Context context = this;
  DataCache dc = DataCache.getInstance();

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_settings);

    LinearLayout logoutView = (LinearLayout) findViewById(R.id.logoutView);

    logoutView.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        DataCache.getInstance().clearDataCache();
        Intent intent = new Intent(context, MainActivity.class);
        startActivity(intent);
      }
    });

    Switch showMale = (Switch) findViewById(R.id.settings_male_filter);
    Switch showFemale = (Switch) findViewById(R.id.settings_female_filter);
    Switch showSpouseLines = (Switch) findViewById(R.id.settings_spouse_lines);
    Switch showStoryLines = (Switch) findViewById(R.id.settings_story_lines);
    Switch showFamilyLines = (Switch) findViewById(R.id.settings_family_lines);

    showMale.setChecked(dc.isShowMale());
    showFemale.setChecked(dc.isShowFemale());
    showSpouseLines.setChecked(dc.isShowSpouseLines());
    showStoryLines.setChecked(dc.isShowStoryLines());
    showFamilyLines.setChecked(dc.isShowFamilyLines());

    showMale.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        dc.setShowMale(isChecked);
      }
    });

    showFemale.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        dc.setShowFemale(isChecked);
      }
    });

    showSpouseLines.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        dc.setShowSpouseLines(isChecked);
      }
    });

    showStoryLines.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        dc.setShowStoryLines(isChecked);
      }
    });

    showFamilyLines.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        dc.setShowFamilyLines(isChecked);
      }
    });
  }

}
