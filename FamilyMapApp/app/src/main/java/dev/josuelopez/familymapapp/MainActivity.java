package dev.josuelopez.familymapapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;

import com.joanzapata.iconify.Iconify;
import com.joanzapata.iconify.fonts.FontAwesomeModule;


public class MainActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    Iconify.with(new FontAwesomeModule());

    FragmentManager fm = this.getSupportFragmentManager();
    LoginFragment loginFragment = (LoginFragment) fm.findFragmentById(R.id.mainLayout);
    MapFragment mapFragment = (MapFragment) fm.findFragmentById(R.id.mainLayout);
    DataCache dc = DataCache.getInstance();
    if (dc.isLoggedIn()) {
      if (mapFragment == null) {
        mapFragment = createMapFragment(getString(R.string.login_title));
        fm.beginTransaction()
            .replace(R.id.mainLayout, mapFragment)
            .commit();
      }
    } else {
      if (loginFragment == null) {
        loginFragment = createLoginFragment(getString(R.string.login_title));
        fm.beginTransaction()
            .add(R.id.mainLayout, loginFragment)
            .commit();
      }
    }

  }

  private LoginFragment createLoginFragment(String title) {
    LoginFragment loginFrag = new LoginFragment();

    Bundle args = new Bundle();
    args.putString(LoginFragment.ARG_TITLE, title);
    loginFrag.setArguments(args);

    return loginFrag;
  }

  private MapFragment createMapFragment(String title) {
    MapFragment mapFrag = new MapFragment();

    Bundle args = new Bundle();
    args.putString(MapFragment.ARG_TITLE, title);
    mapFrag.setArguments(args);

    return mapFrag;
  }

  public void onLoginSuccess() {
    DataCache dc = DataCache.getInstance();
    dc.setLoggedIn(true);
    FragmentManager fm = this.getSupportFragmentManager();
    MapFragment mapFragment = (MapFragment) fm.findFragmentById(R.id.mapFragment);
      if (mapFragment == null) {
        mapFragment = createMapFragment(getString(R.string.login_title));
        fm.beginTransaction()
            .replace(R.id.mainLayout, mapFragment)
            .commit();
      }
  }
}
