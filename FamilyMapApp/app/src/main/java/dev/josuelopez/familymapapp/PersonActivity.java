package dev.josuelopez.familymapapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import Model.Event;
import Model.Person;

public class PersonActivity extends AppCompatActivity {
  private static String LOG_TAG = "PersonActivity";
  private Context context = this;

  String selectedPersonID;

  DataCache dc;

  //Layout Elements
  TextView personFirstName;
  TextView personLastName;
  TextView personGender;

  ExpandableListView expandableListView;
  PersonExpandableListAdapter expandableListAdapter;
  List<String> expandableListTitle;
  HashMap<String, List<String[]>> expandableListDetail;


  @Override
  protected void onCreate(Bundle savedInstanceState) {
    selectedPersonID = getIntent().getExtras().getString("personID");

    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_person);

    dc = DataCache.getInstance();

    TextView personFirstName = findViewById(R.id.personFirstName);
    TextView personLastName = findViewById(R.id.personLastName);
    TextView personGender = findViewById(R.id.personGender);

    Person selectedPerson = dc.getPersonByID(selectedPersonID);

    personFirstName.setText(selectedPerson.getfirstName());
    personLastName.setText(selectedPerson.getlastName());
    personGender.setText(selectedPerson.getGender().equals("f") ? "Female" : "Male");

    expandableListView = (ExpandableListView) findViewById(R.id.expandableListView);
    ExpandableListDataPump pump = new ExpandableListDataPump();
    expandableListDetail = pump.getData(selectedPersonID);
    expandableListTitle = new ArrayList<String>(expandableListDetail.keySet());
    expandableListAdapter = new PersonExpandableListAdapter(this, expandableListTitle, expandableListDetail);
    expandableListView.setAdapter(expandableListAdapter);

    expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
      @Override
      public boolean onChildClick(ExpandableListView parent, View v,
                                  int groupPosition, int childPosition, long id) {

        String[] objectSelected =  expandableListDetail.get(expandableListTitle.get(groupPosition)).get( childPosition);

        if(objectSelected[3].equals("event")){
          Log.i(LOG_TAG, "Event (" + objectSelected[0] + ") " + objectSelected[4] + " clicked!");
          dc.setSelectedEvent(dc.getEventByType(objectSelected[4], objectSelected[0].toLowerCase()));

          Intent intent = new Intent(context, MapActivity.class);
          Bundle mBundle  = new Bundle();

          mBundle.putString("eventID", dc.getSelectedEvent().getEventID());
          mBundle.putInt("zoom", 9);
          intent.putExtras(mBundle);

          startActivity(intent);
        } else if (objectSelected[3].equals("person")){
          Log.i(LOG_TAG, "Person (" + objectSelected[2] + ") " + objectSelected[4] + " clicked!");
          selectedPersonID = objectSelected[4];

          Intent intent = new Intent(context, PersonActivity.class);
          Bundle mBundle  = new Bundle();

          LinkedHashMap<String, Event> events = dc.getEventMap().get(objectSelected[4]);

          Map.Entry first = (Map.Entry) events.entrySet().toArray()[events.size() -1];
          Event firstEvent = (Event) first.getValue();

          dc.setSelectedEvent(firstEvent);

          mBundle.putString("eventID", dc.getSelectedEvent().getEventID());
          mBundle.putString("personID", selectedPersonID);
          intent.putExtras(mBundle);

          startActivity(intent);
        }

        return false;
      }
    });

  }
}
