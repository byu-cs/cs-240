package dev.josuelopez.familymapapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.widget.ImageView;

import com.joanzapata.iconify.IconDrawable;
import com.joanzapata.iconify.fonts.FontAwesomeIcons;

public class SearchActivity extends AppCompatActivity {

  Context context = this;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_search);

    ImageView searchImageView = findViewById(R.id.search_icon);
    Drawable searchIcon = new IconDrawable(context, FontAwesomeIcons.fa_search).colorRes(R.color.colorMale).sizeDp(40);

    searchImageView.setImageDrawable(searchIcon);
  }
}
