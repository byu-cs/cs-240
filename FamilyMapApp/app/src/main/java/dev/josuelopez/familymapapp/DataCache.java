package dev.josuelopez.familymapapp;

import android.util.Log;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;

import Model.Event;
import Model.Person;

public class DataCache {
  private static String[] colors = {"#FF6633", "#FFB399", "#FF33FF", "#FFFF99", "#00B3E6","#E6B333", "#3366E6", "#999966", "#99FF99", "#B34D4D", "#B3B31A", "#00E680"};

  //Instance variable
  private static DataCache instance = null;
  private boolean isLoggedIn = false;

  //Data
  private HashMap<String, Person> personMap;
  private HashMap<String, LinkedHashMap<String, Event>> eventMap;
  private HashMap<String, Event> eventIDMap;
  private HashMap<String, Person> childrenMap;
  HashSet<String> eventTypes;
  HashMap<String, String> colorValues;

  //State
  private Person user;
  private Event selectedEvent;

  //Filters
  private boolean showMale = true;
  private boolean showFemale = true;

  //Settings
  private boolean showStoryLines = true;
  private boolean showSpouseLines = true;
  private boolean showFamilyLines = true;

  //Constructor
  private DataCache(){
    personMap = new HashMap<>();
    eventMap = new HashMap<>();
    eventIDMap = new HashMap<>();
    childrenMap = new HashMap<>();
    eventTypes =  new HashSet<>();
    colorValues = new HashMap<>();
  }

  //Methods
  public static DataCache getInstance(){
    if (instance == null) {
      Log.i("DataCache", "Instantiating data cache...");
      instance = new DataCache();
    }
    return instance;
  }

  public Person getUser() { return user; }

  public void setUser(Person user) { this.user = user; }

  public boolean isLoggedIn() { return isLoggedIn; }

  public void setLoggedIn(boolean loggedIn) { isLoggedIn = loggedIn; }

  public HashMap<String, Person> getPersonMap() {
    return personMap;
  }

  public Person getPersonByID(String personID) { return personMap.get(personID); }

  public void setPeople(ArrayList<Person> list) {
    for (int i = 0; i < list.size(); i++){
      Person personToAdd = list.get(i);
      personMap.put(personToAdd.getPersonID(), list.get(i));
      if(personToAdd.getFatherID() != null && !personToAdd.getFatherID().equals("")) childrenMap.put(personToAdd.getFatherID(), list.get(i));
      if(personToAdd.getMotherID() != null && !personToAdd.getMotherID().equals("")) childrenMap.put(personToAdd.getMotherID(), list.get(i));
    }
  }

  public HashMap<String, LinkedHashMap<String, Event>> getEventMap() {
    return eventMap;
  }

  public Event getEventByType(String personID, String type) {
    HashMap<String, Event> eventList = eventMap.get(personID);

    if(eventList == null) return null;

    return eventList.get(type);
  }

  public Event getEventByID(String eventID){
    return eventIDMap.get(eventID);
  }

  public void setEvents(ArrayList<Event> list) {
    Collections.sort(list, new Comparator<Event>() {
      @Override
      public int compare(Event o1, Event o2) {
        return (o2.getYear()).compareTo(o1.getYear());
      }
    });


    for(final Event event : list){
      eventTypes.add(event.getEventType().toLowerCase());
      eventIDMap.put(event.getEventID(), event);
      if(!eventMap.containsKey(event.getPersonID())){
        eventMap.put(event.getPersonID(), new LinkedHashMap<String, Event>());
      }

      LinkedHashMap<String, Event> em = eventMap.get(event.getPersonID());

      if (em != null) {
        em.put(event.getEventType().toLowerCase(), event);
      }
    }

    setColorValues();
  }

  private void setColorValues(){
    colorValues.put("m", "#456990");
    colorValues.put("f", "#EE92C2");

    int i = 0;
    for(String type : eventTypes){
      System.out.println(eventTypes.size());
      colorValues.put(type, colors[i]);
      i++;
    }
  }

  public HashSet<String> getEventTypes() {
    return eventTypes;
  }

  public void setEventTypes(LinkedHashSet<String> eventTypes) {
    this.eventTypes = eventTypes;
  }

  public HashMap<String, String> getColorValues() {
    return colorValues;
  }

  public void setColorValues(HashMap<String, String> colorValues) {
    this.colorValues = colorValues;
  }

  public Event getSelectedEvent() {
    return selectedEvent;
  }

  public void setSelectedEvent(Event selectedEvent) {
    this.selectedEvent = selectedEvent;
  }

  public Person getChildrenByParentID(String parentID) {
    return childrenMap.get(parentID);
  }

  public boolean isShowMale() {
    return showMale;
  }

  public void setShowMale(boolean showMale) {
    this.showMale = showMale;
  }

  public boolean isShowFemale() {
    return showFemale;
  }

  public void setShowFemale(boolean showFemale) {
    this.showFemale = showFemale;
  }

  public boolean isShowSpouseLines() {
    return showSpouseLines;
  }

  public void setShowSpouseLines(boolean showSpouseLines) {
    this.showSpouseLines = showSpouseLines;
  }

  public boolean isShowStoryLines() {
    return showStoryLines;
  }

  public void setShowStoryLines(boolean showStoryLines) {
    this.showStoryLines = showStoryLines;
  }

  public boolean isShowFamilyLines() {
    return showFamilyLines;
  }

  public void setShowFamilyLines(boolean showFamilyLines) {
    this.showFamilyLines = showFamilyLines;
  }

  public void clearDataCache(){
    isLoggedIn = false;
    user = null;
    eventMap.clear();
    eventMap = null;
    personMap.clear();
    personMap = null;
    childrenMap.clear();
    childrenMap = null;
    selectedEvent = null;
    instance = null;
  }

  @Override
  public String toString() {
    return "DataCache{" +
        "isLoggedIn=" + isLoggedIn +
        ", user=" + user +
        ", personMap=" + personMap +
        ", eventMap=" + eventMap +
        '}';
  }
}
