package dev.josuelopez.familymapapp;

import java.util.HashMap;
import java.util.List;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.joanzapata.iconify.IconDrawable;
import com.joanzapata.iconify.fonts.FontAwesomeIcons;

public class PersonExpandableListAdapter extends BaseExpandableListAdapter {
  DataCache dc = DataCache.getInstance();

  private Context context;
  private List<String> expandableListTitle;
  private HashMap<String, List<String[]>> expandableListDetail;

  public PersonExpandableListAdapter(Context context, List<String> expandableListTitle,
                                     HashMap<String, List<String[]>> expandableListDetail) {
    this.context = context;
    this.expandableListTitle = expandableListTitle;
    this.expandableListDetail = expandableListDetail;
  }

  @Override
  public Object getChild(int listPosition, int expandedListPosition) {
    return this.expandableListDetail.get(this.expandableListTitle.get(listPosition))
        .get(expandedListPosition);
  }

  @Override
  public long getChildId(int listPosition, int expandedListPosition) {
    return expandedListPosition;
  }

  @Override
  public View getChildView(int listPosition, final int expandedListPosition,
                           boolean isLastChild, View convertView, ViewGroup parent) {
    final String[] expandedListText = (String[]) getChild(listPosition, expandedListPosition);
    if (convertView == null) {
      LayoutInflater layoutInflater = (LayoutInflater) this.context
          .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      convertView = layoutInflater.inflate(R.layout.list_item, null);
    }

    ImageView iconImage = (ImageView) convertView.findViewById(R.id.itemIcon);

    Drawable itemIcon = getItemIcon(expandedListText[0]);
    iconImage.setImageDrawable(itemIcon);

    TextView itemDescription = (TextView) convertView
        .findViewById(R.id.itemDescription);
    itemDescription.setText(expandedListText[1]);

    TextView itemText = (TextView) convertView
        .findViewById(R.id.itemText);
    itemText.setText(expandedListText[2]);

    return convertView;
  }

  @Override
  public int getChildrenCount(int listPosition) {
    return this.expandableListDetail.get(this.expandableListTitle.get(listPosition))
        .size();
  }

  @Override
  public Object getGroup(int listPosition) {
    return this.expandableListTitle.get(listPosition);
  }

  @Override
  public int getGroupCount() {
    return this.expandableListTitle.size();
  }

  @Override
  public long getGroupId(int listPosition) {
    return listPosition;
  }

  @Override
  public View getGroupView(int listPosition, boolean isExpanded,
                           View convertView, ViewGroup parent) {
    String listTitle = (String) getGroup(listPosition);
    if (convertView == null) {
      LayoutInflater layoutInflater = (LayoutInflater) this.context.
          getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      convertView = layoutInflater.inflate(R.layout.list_group, null);
    }
    TextView listTitleTextView = (TextView) convertView
        .findViewById(R.id.listTitle);
    listTitleTextView.setTypeface(null, Typeface.BOLD);
    listTitleTextView.setText(listTitle);
    return convertView;
  }

  @Override
  public boolean hasStableIds() {
    return false;
  }

  @Override
  public boolean isChildSelectable(int listPosition, int expandedListPosition) {
    return true;
  }

  private Drawable getItemIcon(String type){
    Drawable returnedIcon = null;
    int color = Color.parseColor(dc.getColorValues().get(type.toLowerCase()));
    switch (type.toLowerCase()) {
      case "m":
        returnedIcon = new IconDrawable(context, FontAwesomeIcons.fa_male).color(color).sizeDp(40);
        break;
      case "f":
        returnedIcon = new IconDrawable(context, FontAwesomeIcons.fa_female).color(color).sizeDp(40);
        break;
      case "birth":
        returnedIcon = new IconDrawable(context, FontAwesomeIcons.fa_user).color(color).sizeDp(40);
        break;
      case "marriage":
        returnedIcon = new IconDrawable(context, FontAwesomeIcons.fa_life_ring).color(color).sizeDp(40);
        break;
      case "death":
        returnedIcon = new IconDrawable(context, FontAwesomeIcons.fa_times).color(color).sizeDp(40);
        break;
      default:
        returnedIcon = new IconDrawable(context, FontAwesomeIcons.fa_map_marker).color(color).sizeDp(40);
        break;
    }

    return returnedIcon;
  }
}