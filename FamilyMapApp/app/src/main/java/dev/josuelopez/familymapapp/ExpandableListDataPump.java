package dev.josuelopez.familymapapp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Model.Event;
import Model.Person;

public class ExpandableListDataPump {
  DataCache dc = DataCache.getInstance();

  public ExpandableListDataPump() {
  }

  public HashMap<String, List<String[]>> getData(String personID) {
    Person activePerson = dc.getPersonByID(personID);


    HashMap<String, List<String[]>> data = new HashMap<String, List<String[]>>();

    List<String[]> eventList = new ArrayList<String[]>();

    HashMap<String, Event> personEvents = dc.getEventMap().get(personID);

    if (personEvents != null) {
      for (Map.Entry<String, Event> eListEntry : personEvents.entrySet()) {
        Event event = eListEntry.getValue();

        String[] singleEvent = new String[5];

        singleEvent[0] = event.getEventType();
        singleEvent[1] = event.getEventType().toUpperCase();
        singleEvent[2] = event.getCity() + ", " + event.getCountry() + " (" + event.getYear() + ")";
        singleEvent[3] = "event";
        singleEvent[4] = event.getPersonID();

        eventList.add(singleEvent);
      }

      Collections.reverse(eventList);

      data.put("LIFE EVENTS", eventList);
    }

    List<String[]> peopleList = new ArrayList<String[]>();

    if(activePerson.getFatherID() != null && !activePerson.getFatherID().equals("")){
      Person father = dc.getPersonByID(activePerson.getFatherID());

      String[] singlePerson = new String[5];

      singlePerson[0] = father.getGender();
      singlePerson[1] = "FATHER";
      singlePerson[2] = father.getfirstName() + " " + father.getlastName();
      singlePerson[3] = "person";
      singlePerson[4] = father.getPersonID();

      if(dc.isShowMale()) peopleList.add(singlePerson);
    }

    if(activePerson.getMotherID() != null && !activePerson.getMotherID().equals("")){
      Person mother = dc.getPersonByID(activePerson.getMotherID());

      String[] singlePerson = new String[5];

      singlePerson[0] = mother.getGender();
      singlePerson[1] = "MOTHER";
      singlePerson[2] = mother.getfirstName() + " " + mother.getlastName();
      singlePerson[3] = "person";
      singlePerson[4] = mother.getPersonID();

      if(dc.isShowFemale()) peopleList.add(singlePerson);
    }

    if(activePerson.getSpouseID() != null && !activePerson.getSpouseID().equals("")){
      Person spouse = dc.getPersonByID(activePerson.getSpouseID());

      String[] singlePerson = new String[5];

      singlePerson[0] = spouse.getGender();
      singlePerson[1] = "SPOUSE";
      singlePerson[2] = spouse.getfirstName() + " " + spouse.getlastName();
      singlePerson[3] = "person";
      singlePerson[4] = spouse.getPersonID();

      if((dc.isShowMale() && spouse.getGender().equals("m")) || (dc.isShowFemale() && spouse.getGender().equals("f")))
        peopleList.add(singlePerson);
    }

    Person child = dc.getChildrenByParentID(activePerson.getPersonID());
    if(child != null){
      String[] singlePerson = new String[5];

      singlePerson[0] = child.getGender();
      singlePerson[1] = "CHILD";
      singlePerson[2] = child.getfirstName() + " " + child.getlastName();
      singlePerson[3] = "person";
      singlePerson[4] = child.getPersonID();

      if((dc.isShowMale() && child.getGender().equals("m")) || (dc.isShowFemale() && child.getGender().equals("f")))
        peopleList.add(singlePerson);
    }

    data.put("FAMILY", peopleList);


    return data;

  }
}