package dev.josuelopez.familymapapp;

import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import java.util.Objects;

import Request.LoginRequest;
import Request.RegisterRequest;
import Task.LoginTask;
import Task.RegisterTask;


public class LoginFragment extends Fragment {
  private static final String LOG_TAG = "LoginFragment";

  static final String ARG_TITLE = "User Login";

  //For tasks
  LoginRequest loginReq;
  RegisterRequest registerReq;

  //Server
  private EditText serverHostEditText;
  private EditText serverPortEditText;

  //User
  private EditText userUsernameEditText;
  private EditText userPasswordEditText;
  private EditText userFirstNameEditText;
  private EditText userLastNameEditText;
  private EditText userEmailEditText;
  private RadioGroup userGenderRadioGroup;

  //Controls
  private Button signInButton;
  private Button registerButton;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    Log.i(LOG_TAG, "in onCreate(Bundle)");

    super.onCreate(savedInstanceState);
  }

  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    Log.i(LOG_TAG, "in onCreateView(...)");
    View view = inflater.inflate(R.layout.fragment_login, container, false);

    //Requests
    loginReq = new LoginRequest();
    registerReq = new RegisterRequest();

    //Server Inputs
    serverHostEditText = view.findViewById(R.id.server_host);
    serverPortEditText = view.findViewById(R.id.server_port);

    //user Inputs
    userUsernameEditText = view.findViewById(R.id.user_username);
    userPasswordEditText = view.findViewById(R.id.user_password);
    userFirstNameEditText = view.findViewById(R.id.user_firstname);
    userLastNameEditText = view.findViewById(R.id.user_lastname);
    userEmailEditText = view.findViewById(R.id.user_email);
    userGenderRadioGroup = view.findViewById(R.id.user_gender);

    //Buttons
    signInButton = view.findViewById(R.id.sign_in);
    registerButton = view.findViewById(R.id.register);

    signInButton.setEnabled(false);
    registerButton.setEnabled(false);

    //Listeners
    createEditTextListeners();
    createRadioGroupListeners();
    createButtonListeners();

    return view;
  }

  /**
   * Creates listeners for edit text fields
   */
  private void createEditTextListeners(){
    //-----------------------  SERVER  -----------------------**
    serverHostEditText.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
        loginReq.setServerHost(s.toString());
        registerReq.setServerHost(s.toString());
      }

      @Override
      public void afterTextChanged(Editable s) {
        if(loginReq.getServerHost() == null || loginReq.getServerHost().equals("")) {
          serverHostEditText.setHint(R.string.example_host);
        } else {
          serverHostEditText.setHint("");
        }
        signInButton.setEnabled(isLoginValid());
        registerButton.setEnabled(isRegisterValid());
      }
    });

    serverPortEditText.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
        loginReq.setServerPort(s.toString());
        registerReq.setServerPort(s.toString());
      }

      @Override
      public void afterTextChanged(Editable s) {
        if(loginReq.getServerHost() == null || loginReq.getServerHost().equals("")) {
          serverPortEditText.setHint(R.string.example_port);
        } else {
          serverPortEditText.setHint("");
        }
        signInButton.setEnabled(isLoginValid());
        registerButton.setEnabled(isRegisterValid());
      }
    });

    //-----------------------  LOGIN / REGISTER  -----------------------**
    userUsernameEditText.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
        loginReq.setUserName(s.toString());
        registerReq.setUserName(s.toString());
      }

      @Override
      public void afterTextChanged(Editable s) {
        if(loginReq.getUserName() == null || loginReq.getUserName().equals("")) {
          userUsernameEditText.setHint(R.string.example_username);
        } else {
          userUsernameEditText.setHint("");
        }
        signInButton.setEnabled(isLoginValid());
        registerButton.setEnabled(isRegisterValid());
      }
    });

    userPasswordEditText.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {
      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
        loginReq.setPassword(s.toString());
        registerReq.setPassword(s.toString());
      }

      @Override
      public void afterTextChanged(Editable s) {
        if(loginReq.getPassword() == null || loginReq.getPassword().equals("")) {
          userPasswordEditText.setHint(R.string.example_password);
        } else {
          userPasswordEditText.setHint("");
        }
        signInButton.setEnabled(isLoginValid());
        registerButton.setEnabled(isRegisterValid());
      }
    });

    //-----------------------  REGISTER  -----------------------**
    userFirstNameEditText.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {
      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
        registerReq.setFirstName(s.toString());
      }

      @Override
      public void afterTextChanged(Editable s) {
        if(registerReq.getFirstName() == null || registerReq.getFirstName().equals("")) {
          userFirstNameEditText.setHint(R.string.example_firstname);
        } else {
          userFirstNameEditText.setHint("");
        }
        signInButton.setEnabled(isLoginValid());
        registerButton.setEnabled(isRegisterValid());
      }
    });

    userLastNameEditText.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {
      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
        registerReq.setLastName(s.toString());
      }

      @Override
      public void afterTextChanged(Editable s) {
        if(registerReq.getLastName() == null || registerReq.getLastName().equals("")) {
          userLastNameEditText.setHint(R.string.example_lastname);
        } else {
          userLastNameEditText.setHint("");
        }
        signInButton.setEnabled(isLoginValid());
        registerButton.setEnabled(isRegisterValid());
      }
    });

    userEmailEditText.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {
      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
        registerReq.setEmail(s.toString());
      }

      @Override
      public void afterTextChanged(Editable s) {
        if(registerReq.getEmail() == null || registerReq.getEmail().equals("")) {
          userEmailEditText.setHint(R.string.example_email);
        } else {
          userEmailEditText.setHint("");
        }
        signInButton.setEnabled(isLoginValid());
        registerButton.setEnabled(isRegisterValid());
      }
    });
  }

  /**
   * Creates listeners for radio groups
   */
  private void createRadioGroupListeners(){
    userGenderRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(RadioGroup group, int checkedId) {
        if (checkedId == R.id.male_radio) { registerReq.setGender("m"); }
        if (checkedId == R.id.female_radio) { registerReq.setGender("f"); }
      }
    });
  }

  /**
   * Creates listeners for buttons
   */
  private void createButtonListeners(){
    signInButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        loginButtonCliked();
      }
    });

    registerButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        registerButtonClicked();
      }
    });
  }

  private void loginButtonCliked(){
    LoginTask loginTask = new LoginTask(this);
    loginTask.execute(loginReq);
  }

  private void registerButtonClicked(){
    RegisterTask registerTask = new RegisterTask(this);
    registerTask.execute(registerReq);
  }

  private boolean isLoginValid(){
    return loginReq.getServerHost() != null && !loginReq.getServerHost().equals("") &&
        loginReq.getServerPort() != null && !loginReq.getServerPort().equals("") &&
        loginReq.getUserName() != null && !loginReq.getUserName().equals("") &&
        loginReq.getPassword() != null && !loginReq.getPassword().equals("");
  }

  private boolean isRegisterValid(){
    return registerReq.getServerHost() != null && !registerReq.getServerHost().equals("") &&
        registerReq.getServerPort() != null && !registerReq.getServerPort().equals("") &&
        registerReq.getUserName() != null && !registerReq.getUserName().equals("") &&
        registerReq.getPassword() != null && !registerReq.getPassword().equals("") &&
        registerReq.getFirstName() != null && !registerReq.getFirstName().equals("") &&
        registerReq.getLastName() != null && !registerReq.getLastName().equals("") &&
        registerReq.getEmail() != null && !registerReq.getEmail().equals("");
  }

}