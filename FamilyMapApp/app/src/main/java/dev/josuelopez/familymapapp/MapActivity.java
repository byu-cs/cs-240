package dev.josuelopez.familymapapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;
import android.provider.ContactsContract;

public class MapActivity extends AppCompatActivity {
  DataCache dc = DataCache.getInstance();

  private String eventID;
  private int zoom;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    eventID = getIntent().getExtras().getString("eventID");
    zoom = getIntent().getExtras().getInt("zoom");

    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_map);

    FragmentManager fm = getSupportFragmentManager();
    MapFragment mapFrag = (MapFragment) fm.findFragmentById(R.id.mapFragment);


    if(mapFrag == null) {
      mapFrag = new MapFragment();
      dc.setSelectedEvent(dc.getEventByID(eventID));
      mapFrag.setComingFromPerson(true);
      mapFrag.setZoom(zoom);
      fm.beginTransaction()
          .replace(R.id.mapLayout, mapFrag)
          .commit();
    }
  }
}
