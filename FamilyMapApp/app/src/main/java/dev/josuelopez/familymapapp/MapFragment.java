package dev.josuelopez.familymapapp;


import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.JointType;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.joanzapata.iconify.IconDrawable;
import com.joanzapata.iconify.fonts.FontAwesomeIcons;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;

import Model.Event;
import Model.Person;

/**
 * A simple {@link Fragment} subclass.
 */
public class MapFragment extends Fragment {
  private static final String LOG_TAG = "MapFragment";

  static final String ARG_TITLE = "Family Map";

  private SupportMapFragment smf;
  private GoogleMap gMap;

  private DataCache dc = DataCache.getInstance();

  //Layout Elements

  private TextView textFullName = null;
  private TextView textInfo = null;
  private ImageView genderImage = null;

  private Boolean comingFromPerson = false;
  private int zoom = 6;


  public MapFragment() {
    // Required empty public constructor
  }

  public void setComingFromPerson(Boolean bool){
    comingFromPerson = bool;
  }

  public void setZoom(int zoom) { this.zoom = zoom; }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    if (comingFromPerson){
      setHasOptionsMenu(false);
    } else {
      setHasOptionsMenu(true);
    }
    super.onCreate(savedInstanceState);
  }

  @Override
  public void onCreateOptionsMenu(Menu myMenu, MenuInflater inflater){
    inflater.inflate(R.menu.activity_main, myMenu);
    super.onCreateOptionsMenu(myMenu, inflater);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle item selection
    switch (item.getItemId()) {
      case R.id.search:
        Intent intentThree = new Intent(getActivity(), SearchActivity.class);
        startActivity(intentThree);
        return true;
      case R.id.settings:
        Intent intent = new Intent(getActivity(), SettingsActivity.class);
        startActivity(intent);
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    final View v = inflater.inflate(R.layout.fragment_map, container, false);
    
    initializeLayoutElements(v);

    smf = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
    if(smf == null ) return null;
    smf.getMapAsync(new OnMapReadyCallback() {
      @Override
      public void onMapReady(GoogleMap googleMap) {
        gMap = googleMap;


        HashMap<String, LinkedHashMap<String, Event>> eventMap = dc.getEventMap();

        for(Map.Entry<String, LinkedHashMap<String, Event>> eMapEntry : eventMap.entrySet()){
          LinkedHashMap<String, Event> eventList = eMapEntry.getValue();

          for(Map.Entry<String, Event> eListEntry : eventList.entrySet()) {
            Event event = eListEntry.getValue();

            String gender = dc.getPersonByID(event.getPersonID()).getGender();

            if(dc.isShowFemale() && gender.equals("f")){
              LatLng eventLatLong = new LatLng(event.getLatitude(), event.getLongitude());
              gMap.addMarker(new MarkerOptions().position(eventLatLong)
                  .icon(getMarkerIcon(event.getEventType()))).setTag(event);
            } else if (dc.isShowMale() && gender.equals("m")){
              LatLng eventLatLong = new LatLng(event.getLatitude(), event.getLongitude());
              gMap.addMarker(new MarkerOptions().position(eventLatLong)
                  .icon(getMarkerIcon(event.getEventType()))).setTag(event);
            }
          }
        }

        Person selectedPerson;
        Event initialEvent;


        if(dc.getSelectedEvent() == null){
          selectedPerson = dc.getUser();
          initialEvent = dc.getEventByType(selectedPerson.getPersonID(), "birth");
          dc.setSelectedEvent(initialEvent);
        } else {
          initialEvent = dc.getSelectedEvent();
        }

        LatLng placeToCenter = null;

        placeToCenter = new LatLng(initialEvent.getLatitude(), initialEvent.getLongitude());

        gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(placeToCenter, zoom));

        updateCard(v);

        gMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
          @Override
          public boolean onMarkerClick(Marker marker) {
            Event eventSelected = (Event) marker.getTag();

            dc.setSelectedEvent(eventSelected);

            updateCard(v);

            Log.d("Click","was clicked");
            return false;
          }
        });

        createSpouseLines(gMap);
        createStoryLines(gMap);
        createFamilyLines(dc.getUser(), gMap);
      }
    });

    LinearLayout eventCard = (LinearLayout) v.findViewById(R.id.event_card);
    eventCard.setOnClickListener(new View.OnClickListener() {

      @Override
      public void onClick(View v) {
        Log.i(LOG_TAG, "Event Card clicked!");

        Intent intent = new Intent(getActivity(), PersonActivity.class);
        Bundle mBundle  = new Bundle();

        mBundle.putString("personID", dc.getSelectedEvent().getPersonID());
        intent.putExtras(mBundle);

        startActivity(intent);

      }
    });

    return v;
  }

  private void initializeLayoutElements(View v){
    textFullName = (TextView) v.findViewById(R.id.card_full_name);
    textInfo = (TextView) v.findViewById(R.id.card_info);

    genderImage = v.findViewById(R.id.event_icon);
  }

  private void updateCard(View v){
    Event selectedEvent = dc.getSelectedEvent();
    if(selectedEvent == null) return;

    Person selectedPerson = dc.getPersonMap().get(selectedEvent.getPersonID());
    if(selectedPerson == null) return;

    String nameStr = selectedPerson.getfirstName() + " " + selectedPerson.getlastName();
    String eventStr = selectedEvent.getEventType().toUpperCase() + ": " + selectedEvent.getCity() + ", " + selectedEvent.getCountry();

    textFullName.setText(nameStr);
    textInfo.setText(eventStr);
    setGenderIcon(selectedPerson.getGender());
  }

  private void setGenderIcon(String gender){
    if (gender.equals("f")){
      Drawable genderIcon = new IconDrawable(getActivity(), FontAwesomeIcons.fa_female).colorRes(R.color.colorFemale).sizeDp(40);
      genderImage.setImageDrawable(genderIcon);
    } else {
      Drawable genderIcon = new IconDrawable(getActivity(), FontAwesomeIcons.fa_male).colorRes(R.color.colorMale).sizeDp(40);
      genderImage.setImageDrawable(genderIcon);
    }
  }

  private BitmapDescriptor getMarkerIcon(String eventType) {
    float colorHue;
    float[] hsv = new float[3];

    int color = Color.parseColor(dc.getColorValues().get(eventType.toLowerCase()));
    Color.colorToHSV(color, hsv);
    return BitmapDescriptorFactory.defaultMarker(hsv[0]);
  }

  private void createSpouseLines(GoogleMap gMap){
    if(dc.isShowSpouseLines() && dc.isShowFemale() && dc.isShowMale()){
      HashSet<String> visitedSpouse = new HashSet<>();

      for(Map.Entry<String, Person> entry : dc.getPersonMap().entrySet()){
        Person currentPerson = entry.getValue();

        if(!visitedSpouse.contains(currentPerson.getPersonID())) {
          if(currentPerson.getSpouseID() != null && !currentPerson.getSpouseID().equals("")){
            visitedSpouse.add(currentPerson.getSpouseID());

            ArrayList<String> currentEventTypes = new ArrayList<>(dc.getEventMap().get(currentPerson.getPersonID()).keySet());
            ArrayList<String> spouseEventTypes = new ArrayList<>(dc.getEventMap().get(currentPerson.getSpouseID()).keySet());

            Event startEvent = dc.getEventByType(currentPerson.getPersonID(), currentEventTypes.get(currentEventTypes.size()-1));
            LatLng start = null;
            if(startEvent != null){
              start = new LatLng(startEvent.getLatitude(), startEvent.getLongitude());
            }

            Event endEvent = dc.getEventByType(currentPerson.getSpouseID(), spouseEventTypes.get(spouseEventTypes.size()-1));
            Person spouse = dc.getPersonByID(currentPerson.getSpouseID());

            LatLng end = null;
            if(endEvent != null) {
              end = new LatLng(endEvent.getLatitude(), endEvent.getLongitude());
            }

            Polyline spouseLine;
            if(start != null && end != null) {
              spouseLine = gMap.addPolyline(new PolylineOptions()
                  .clickable(false)
                  .add(start, end));
              spouseLine.setColor(getResources().getColor(R.color.colorMarriage));

            }

          }

        }
      }
    }
  }

  private void createStoryLines(GoogleMap gMap) {
    if (dc.isShowStoryLines()) {
      for (Map.Entry<String, LinkedHashMap<String, Event>> entry : dc.getEventMap().entrySet()) {
        LinkedHashMap<String, Event> currentEvents = entry.getValue();
        String personID = entry.getKey();
        Person personStory = dc.getPersonByID(personID);

        if(personStory.getGender().equals("m") && !dc.isShowMale()) continue;
        if(personStory.getGender().equals("f") && !dc.isShowFemale()) continue;

        ArrayList<LatLng> coordList = new ArrayList<>();

        for(String type : currentEvents.keySet()){
          Event event = dc.getEventByType(personID, type);

          if(event != null) coordList.add(new LatLng(event.getLatitude(), event.getLongitude()));
        }

        Polyline storyLine = gMap.addPolyline(new PolylineOptions()
            .clickable(false)
            .add(coordList.toArray(new LatLng[coordList.size()])));

        storyLine.setColor(getResources().getColor(R.color.colorBirth));
        storyLine.setJointType(JointType.ROUND);
        storyLine.setTag(dc.getPersonByID(personID).getfirstName());
      }
    }
  }

  private void createFamilyLines(Person start, GoogleMap gMap){
    if(start.getGender().equals("m") && !dc.isShowMale()) return;
    if(start.getGender().equals("f") && !dc.isShowFemale()) return;
    if(dc.isShowFamilyLines()){
      createFamilyLinesRecurse(start, 6, gMap);
    }
  }

  private void createFamilyLinesRecurse(Person current, int generation, GoogleMap gMap){
    Person father = null;
    if(current.getFatherID() != null && !current.getFatherID().equals(""))
      father = dc.getPersonByID(current.getFatherID());

    Person mother = null;
    if(current.getMotherID() != null && !current.getMotherID().equals(""))
      mother = dc.getPersonByID(current.getMotherID());


    ArrayList<String> currentEventTypes = new ArrayList<>(dc.getEventMap().get(current.getPersonID()).keySet());

    Event currentBirth = dc.getEventByType(current.getPersonID(), currentEventTypes.get(currentEventTypes.size()-1));
    LatLng currLoc = new LatLng(currentBirth.getLatitude(), currentBirth.getLongitude());


    ArrayList<String> fatherEventTypes = null;
    if (father != null) {
      fatherEventTypes = new ArrayList<>(dc.getEventMap().get(father.getPersonID()).keySet());
    }

    ArrayList<String> motherEventTypes = null;
    if (mother != null) {
      motherEventTypes = new ArrayList<>(dc.getEventMap().get(mother.getPersonID()).keySet());
    }

    if(father == null ) return;
    Event fatherBirth = dc.getEventByType(father.getPersonID(), fatherEventTypes.get(fatherEventTypes.size()-1));
    LatLng fatherLoc = new LatLng(fatherBirth.getLatitude(), fatherBirth.getLongitude());

    if(mother == null ) return;
    Event motherBirth = dc.getEventByType(mother.getPersonID(), motherEventTypes.get(motherEventTypes.size()-1));
    LatLng motherLoc = new LatLng(motherBirth.getLatitude(), motherBirth.getLongitude());

    float lineWidth = (float) Math.pow(1.7,generation);

    Polyline fatherLine = gMap.addPolyline(new PolylineOptions()
        .clickable(false)
        .add(currLoc, fatherLoc));
    fatherLine.setColor(getResources().getColor(R.color.colorPrimary));
    fatherLine.setWidth(lineWidth);

    Polyline motherLine = gMap.addPolyline(new PolylineOptions()
        .clickable(false)
        .add(currLoc, motherLoc));
    motherLine.setColor(getResources().getColor(R.color.colorPrimary));
    motherLine.setWidth(lineWidth);

    createFamilyLinesRecurse(mother, generation - 1, gMap);
    createFamilyLinesRecurse(father, generation - 1, gMap);
  }
}
