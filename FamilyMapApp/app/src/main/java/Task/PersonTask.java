package Task;

import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.util.Log;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import Model.Person;
import Request.PersonRequest;
import Response.PersonResponse;
import Server.ServerProxy;
import Server.ServerProxyException;
import dev.josuelopez.familymapapp.DataCache;

public class PersonTask extends AsyncTask<PersonRequest, Void, PersonResponse> {
  final static String LOG_TAG = "PersonTask";

  Fragment f;
  private PersonRequest request;
  private PersonResponse response;
  private String personID;
  private boolean getAncestors = false;

  public PersonTask(Fragment f, boolean getAncestors) {
    this.f = f;
    this.getAncestors = getAncestors;
  }

  @Override
  protected PersonResponse doInBackground(PersonRequest... personRequests) {
    request = personRequests[0];
    personID = request.getPersonID() != null ? request.getPersonID() : "";

    try {
      ServerProxy sp = new ServerProxy();
      URL url = new URL("http://" + request.getServerHost() + ":" + request.getServerPort() + "/person/" + (!personID.equals("") ? request.getPersonID() : ""));
      response = sp.personGet(url.toString(), request);

      return response;
    } catch (MalformedURLException e) {
      e.printStackTrace();
    } catch (ServerProxyException e) {
      e.printStackTrace();
    }

    return null;
  }

  @Override
  protected void onPostExecute(PersonResponse personResponse) {
    super.onPostExecute(personResponse);
    if(personResponse == null) {
      Toast.makeText(f.getContext(), "No response received", Toast.LENGTH_SHORT).show();
      return;
    }

    DataCache dc = DataCache.getInstance();
    Person user = personResponse.getData().get(0);
    dc.setUser(user);
    dc.setPeople(personResponse.getData());
    Log.i(LOG_TAG, "Setting user: " + dc.toString());

    if(!getAncestors){
      return;
    }

    PersonRequest personReq = new PersonRequest(request.getUsername(), "", request.getToken(), request.getServerHost(), request.getServerPort());

    PersonTask personTask = new PersonTask(f, false);
    personTask.execute(personReq);
  }
}
