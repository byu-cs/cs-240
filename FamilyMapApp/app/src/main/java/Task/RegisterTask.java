package Task;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import java.util.concurrent.ExecutionException;

import Model.Person;
import Request.EventRequest;
import Request.PersonRequest;
import Request.RegisterRequest;
import Response.EventResponse;
import Response.PersonResponse;
import Response.RegisterResponse;
import Server.ServerProxy;
import Server.ServerProxyException;
import dev.josuelopez.familymapapp.DataCache;
import dev.josuelopez.familymapapp.LoginFragment;

public class RegisterTask extends AsyncTask<RegisterRequest, Void, RegisterResponse> {
  final static String LOG_TAG = "RegisterTask";

  Fragment f;

  public RegisterTask(LoginFragment f) {
    this.f = f;
  }

  private RegisterRequest request;
  private RegisterResponse response;

  @Override
  protected RegisterResponse doInBackground(RegisterRequest... registerRequests) {
    Log.i(LOG_TAG, "starting task in background");
    request = registerRequests[0];
    try{
      ServerProxy sp = new ServerProxy();

      String url = "http://" + request.getServerHost() + ":" + request.getServerPort() + "/user/register";
      response = sp.registerPost(url, request);

      return response;
    } catch (ServerProxyException e) {
      e.printStackTrace();
      response = new RegisterResponse("Registration error");
      return response;
    }
  }

  @Override
  protected void onPostExecute(RegisterResponse registerResponse) {
    super.onPostExecute(registerResponse);
    if(registerResponse.getMessage() != null && registerResponse.getMessage().toLowerCase().contains("error")){
      Toast.makeText(f.getContext(), registerResponse.getMessage(), Toast.LENGTH_SHORT).show();
      return;
    }

    PersonResponse personRes = executePersonTask(registerResponse);
    if(personRes == null || personRes.getData() == null || personRes.getData().size() < 1) return;

    EventResponse eventRes = executeEventTask(registerResponse);
    if(eventRes == null || eventRes.getData() == null || eventRes.getData().size() < 1) return;

    Person user = personRes.getData().get(0);
    Toast.makeText(f.getContext(), "New user: " + user.getfirstName() + " " + user.getlastName(), Toast.LENGTH_SHORT).show();
    DataCache dc = DataCache.getInstance();
    Log.i(LOG_TAG, dc.getEventMap().toString());
  }

  private PersonResponse executePersonTask(RegisterResponse registerResponse){
    PersonTask personTask = new PersonTask(f, true);
    PersonRequest personReq = new PersonRequest(registerResponse.getUserName(), registerResponse.getPersonID(), registerResponse.getAuthToken(), request.getServerHost(), request.getServerPort());
    PersonResponse personRes;
    try {
      personRes = personTask.execute(personReq).get();
    } catch (ExecutionException e) {
      e.printStackTrace();
      return null;
    } catch (InterruptedException e) {
      e.printStackTrace();
      return null;
    }

    return personRes;
  }

  private EventResponse executeEventTask(RegisterResponse registerResponse){
    EventTask eventTask = new EventTask(f);
    EventRequest eventReq = new EventRequest(registerResponse.getUserName(), null, registerResponse.getAuthToken(), request.getServerHost(), request.getServerPort());
    EventResponse eventRes;
    try {
      eventRes = eventTask.execute(eventReq).get();
    } catch (ExecutionException e) {
      e.printStackTrace();
      return null;
    } catch (InterruptedException e) {
      e.printStackTrace();
      return null;
    }

    return eventRes;
  }
}
