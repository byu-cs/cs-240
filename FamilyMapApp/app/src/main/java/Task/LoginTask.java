package Task;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import java.util.concurrent.ExecutionException;

import Model.Person;
import Request.EventRequest;
import Request.LoginRequest;
import Request.PersonRequest;
import Response.EventResponse;
import Response.LoginResponse;
import Response.PersonResponse;
import Server.ServerProxy;
import Server.ServerProxyException;
import dev.josuelopez.familymapapp.DataCache;
import dev.josuelopez.familymapapp.LoginFragment;

public class LoginTask extends AsyncTask<LoginRequest, Void, LoginResponse> {
  final static String LOG_TAG = "LoginTask";

  Fragment f;
  private LoginRequest request;

  public LoginTask(LoginFragment f) {
    this.f = f;
  }

  @Override
  protected LoginResponse doInBackground(LoginRequest... loginRequests) {
    request = loginRequests[0];
    LoginResponse response = null;
    try{
      ServerProxy sp = new ServerProxy();

      String url = "http://" + request.getServerHost() + ":" + request.getServerPort() + "/user/login";
      response = sp.loginPost(url, request);

      return response;
    } catch (ServerProxyException e) {
      e.printStackTrace();
      return new LoginResponse(e.getMessage());
    }
  }

  @Override
  protected void onPostExecute(LoginResponse loginResponse) {
    super.onPostExecute(loginResponse);
    if(loginResponse.getMessage() != null && loginResponse.getMessage().toLowerCase().contains("error")){
      Toast.makeText(f.getContext(), loginResponse.getMessage(), Toast.LENGTH_SHORT).show();
      return;
    }

    PersonResponse personRes = executePersonTask(loginResponse);
    if(personRes == null || personRes.getData() == null || personRes.getData().size() < 1) return;

    EventResponse eventRes = executeEventTask(loginResponse);
    if(eventRes == null || eventRes.getData() == null || eventRes.getData().size() < 1) return;

    Person user = personRes.getData().get(0);
    Toast.makeText(f.getContext(), "Signed in as: " + user.getfirstName() + " " + user.getlastName(), Toast.LENGTH_SHORT).show();

    DataCache dc = DataCache.getInstance();
    Log.i(LOG_TAG, dc.toString());
  }

  private PersonResponse executePersonTask(LoginResponse loginResponse){
    PersonTask personTask = new PersonTask(f, true);
    PersonRequest personReq = new PersonRequest(loginResponse.getUserName(), loginResponse.getPersonID(), loginResponse.getAuthToken(), request.getServerHost(), request.getServerPort());
    PersonResponse personRes;
    try {
      personRes = personTask.execute(personReq).get();
    } catch (ExecutionException e) {
      e.printStackTrace();
      return null;
    } catch (InterruptedException e) {
      e.printStackTrace();
      return null;
    }

    return personRes;
  }

  private EventResponse executeEventTask(LoginResponse loginResponse){
    EventTask eventTask = new EventTask(f);
    EventRequest eventReq = new EventRequest(loginResponse.getUserName(), null, loginResponse.getAuthToken(), request.getServerHost(), request.getServerPort());
    EventResponse eventRes;
    try {
      eventRes = eventTask.execute(eventReq).get();
    } catch (ExecutionException e) {
      e.printStackTrace();
      return null;
    } catch (InterruptedException e) {
      e.printStackTrace();
      return null;
    }

    return eventRes;
  }
}
