package Task;

import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.util.Log;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import java.net.MalformedURLException;
import java.net.URL;

import Request.EventRequest;
import Request.PersonRequest;
import Response.EventResponse;
import Response.PersonResponse;
import Server.ServerProxy;
import Server.ServerProxyException;
import dev.josuelopez.familymapapp.DataCache;
import dev.josuelopez.familymapapp.MainActivity;

public class EventTask extends AsyncTask<EventRequest, Void, EventResponse> {
  final static String LOG_TAG = "EventTask";

  Fragment f;
  private EventRequest request;
  private EventResponse response;
  private String eventID;

  public EventTask(Fragment f) {
    this.f = f;
  }

  @Override
  protected EventResponse doInBackground(EventRequest... eventRequests) {
    request = eventRequests[0];
    eventID = request.getEventID() != null ? request.getEventID() : "";

    try {
      ServerProxy sp = new ServerProxy();
      URL url = new URL("http://" + request.getServerHost() + ":" + request.getServerPort() + "/event/" + (!eventID.equals("") ? request.getEventID() : ""));
      response = sp.eventGet(url.toString(), request);

      return response;
    } catch (MalformedURLException e) {
      e.printStackTrace();
    } catch (ServerProxyException e) {
      e.printStackTrace();
    }

    return null;
  }

  @Override
  protected void onPostExecute(EventResponse eventResponse) {
    super.onPostExecute(eventResponse);
    if(eventResponse == null) {
      Toast.makeText(f.getContext(), "No response received", Toast.LENGTH_SHORT).show();
      return;
    }

    DataCache dc = DataCache.getInstance();
    dc.setEvents(eventResponse.getData());
    Log.i(LOG_TAG, dc.toString());

    MainActivity source = (MainActivity) f.getActivity();
    if(source != null) source.onLoginSuccess();
  }
}
