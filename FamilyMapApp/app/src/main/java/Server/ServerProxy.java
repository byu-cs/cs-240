package Server;

import android.util.Log;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import JSON.JSON;
import Model.Event;
import Model.Person;
import Request.EventRequest;
import Request.LoginRequest;
import Request.PersonRequest;
import Request.RegisterRequest;
import Response.ClearResponse;
import Response.EventResponse;
import Response.LoginResponse;
import Response.PersonResponse;
import Response.RegisterResponse;

public class ServerProxy {
  private static final String LOG_TAG = "HTTP Client";

  private JSON json = new JSON();

  public RegisterResponse registerPost(String urlString, RegisterRequest request) throws ServerProxyException{
    Log.i(LOG_TAG, "POST: " + urlString);

    try{
      URL url = new URL(urlString);

      HttpURLConnection connection = (HttpURLConnection) url.openConnection();

      connection.setReadTimeout(5000);
      connection.setRequestMethod("POST");
      connection.setDoOutput(true);
      connection.setRequestProperty("Accept", "application/json");

      writeRequestBody(connection, request);

      connection.connect();

      if(connection.getResponseCode() != HttpURLConnection.HTTP_OK){
        throw new ServerProxyException("Server Error");
      }

      String resBody = readResponseBody(connection);
      return (RegisterResponse) json.decode(resBody, RegisterResponse.class);
    } catch (MalformedURLException e) {
      throw new ServerProxyException("Register error");
    } catch (IOException e) {
      throw new ServerProxyException("Register error");
    }
  }

  public LoginResponse loginPost(String urlString, LoginRequest loginReg) throws ServerProxyException {
    Log.i(LOG_TAG, "POST: " + urlString);

    try{
      URL url = new URL(urlString);

      HttpURLConnection connection = (HttpURLConnection) url.openConnection();

      connection.setReadTimeout(5000);
      connection.setRequestMethod("POST");
      connection.setDoOutput(true);
      connection.addRequestProperty("Accept", "application/json");

      writeRequestBody(connection, loginReg);

      connection.connect();

      if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
        throw new ServerProxyException("Server error");
      }

      String resBody = readResponseBody(connection);
      return (LoginResponse) json.decode(resBody, LoginResponse.class);
    } catch (MalformedURLException e) {
      throw new ServerProxyException("Invalid server error: check host and port");
    } catch (IOException e) {
      throw new ServerProxyException("Server error");
    }
  }

  public PersonResponse personGet(String urlString, PersonRequest personReq) throws ServerProxyException {
    Log.i(LOG_TAG, "GET: " + urlString);

    try{
      URL url = new URL(urlString);


      HttpURLConnection connection = (HttpURLConnection) url.openConnection();

      connection.setReadTimeout(5000);
      connection.setRequestMethod("GET");
      connection.setDoOutput(false);
      connection.setRequestProperty("Accept", "application/json");
      connection.setRequestProperty("Authorization", personReq.getToken());

      connection.connect();

      if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
        Log.i(LOG_TAG, "server error");
        return null;
      }

      String resBody = readResponseBody(connection);

      Person person;
      PersonResponse response;

      if(personReq.getPersonID() != null && !personReq.getPersonID().equals("")){
        person = (Person) json.decode(resBody, Person.class);
        ArrayList<Person> data = new ArrayList<>();
        data.add(person);
        response = new PersonResponse(data);
      } else {
        response = (PersonResponse) json.decode(resBody, PersonResponse.class);
      }

      return response;
    } catch (MalformedURLException e) {
      throw new ServerProxyException("Invalid server error: check host and port");
    } catch (IOException e) {
      e.printStackTrace();
      throw new ServerProxyException("Person error");
    }
  }

  public EventResponse eventGet(String urlString, EventRequest eventReq) throws ServerProxyException {
    Log.i(LOG_TAG, "GET: " + urlString);

    try{
      URL url = new URL(urlString);

      HttpURLConnection connection = (HttpURLConnection) url.openConnection();

      connection.setReadTimeout(5000);
      connection.setRequestMethod("GET");
      connection.setDoOutput(false);
      connection.setRequestProperty("Accept", "application/json");
      connection.setRequestProperty("Authorization", eventReq.getToken());

      connection.connect();

      if(connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
        Log.i(LOG_TAG, "server error");
        return null;
      }

      String resBody = readResponseBody(connection);

      Event event;
      EventResponse response;

      if(eventReq.getEventID() != null && !eventReq.getEventID().equals("")){
        event = (Event) json.decode(resBody, Event.class);
        ArrayList<Event> data = new ArrayList<>();
        data.add(event);
        response = new EventResponse(data);
      } else {
        response = (EventResponse) json.decode(resBody, EventResponse.class);
      }

      return response;

    } catch (MalformedURLException e) {
      throw new ServerProxyException("Invalid server error: check url");
    } catch (IOException e) {
      e.printStackTrace();
      throw new ServerProxyException("Event error");
    }
  }

  public void clearPost() throws IOException {
    URL url = new URL("http://localhost:8080/clear/");

    HttpURLConnection connection = (HttpURLConnection) url.openConnection();

    connection.setReadTimeout(5000);
    connection.setRequestMethod("POST");
    connection.setRequestProperty("Accept", "application/json");
    connection.setDoOutput(false);

    connection.connect();
  }

  private void writeRequestBody(HttpURLConnection connection, Object obj) throws IOException {
    String resData = json.encode(obj);

    OutputStream os = connection.getOutputStream();
    OutputStreamWriter sw = new OutputStreamWriter(os);
    BufferedWriter bw = new BufferedWriter(sw);

    bw.write(resData);
    bw.flush();

  }

  private String readResponseBody(HttpURLConnection connection) throws IOException {
    InputStream is = connection.getInputStream();
    StringBuilder sb = new StringBuilder();
    InputStreamReader sr = new InputStreamReader(is);
    char[] buf = new char[1024];
    int len;
    while ((len = sr.read(buf)) > 0) {
      sb.append(buf, 0, len);
    }
    return sb.toString();
  }
}
