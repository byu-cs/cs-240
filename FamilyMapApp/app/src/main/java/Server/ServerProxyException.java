package Server;

public class ServerProxyException extends Exception {
  public ServerProxyException(String message) {
    super(message);
  }
}
