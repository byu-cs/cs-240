package hangman;

public class EmptyDictionaryException extends Exception {
  EmptyDictionaryException(String errorMessage) {
    super(errorMessage);
  }
}
