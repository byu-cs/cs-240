package hangman;

public class GuessAlreadyMadeException extends Exception {
  GuessAlreadyMadeException(String errorMessage) {
    super(errorMessage);
  }
}