package hangman;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class EvilHangmanGame implements IEvilHangmanGame {
  private SortedSet<Character> guessedLetters = new TreeSet<Character>();
  private SortedSet<String> words = null;
  private StringBuilder output = null;
  private String currWord = null;
  private int guessesLeft;

  @Override
  public void startGame(File dictionary, int wordLength) throws IOException, EmptyDictionaryException {
    words = new TreeSet<String>();
    if (dictionary.length() == 0 || wordLength < 1) throw new EmptyDictionaryException("File is empty");
    //Setup for loading dictionary
    FileReader fr = new FileReader(dictionary);
    BufferedReader br = new BufferedReader(fr);
    Scanner dictionaryScanner = new Scanner(br);

    String dash = "-";
    currWord = dash.repeat(wordLength);
    output = new StringBuilder(currWord);

    //Add word if it matches the wordLength
    while(dictionaryScanner.hasNext()) {
      String word = dictionaryScanner.next();
      if (word.length() == wordLength) words.add(word);
    }

    if (words.size() == 0) throw new EmptyDictionaryException("File is empty");
  }

  @Override
  public Set<String> makeGuess(char guess) throws GuessAlreadyMadeException {
    //Check if letter has been used
    Character lowerGuess = Character.toLowerCase(guess);
    if(!guessedLetters.add(lowerGuess)) {
      throw new GuessAlreadyMadeException("\'" + guess +"\' has already been used\n");
    }

    HashMap<Integer, SortedSet<String>> partition = groupWords(guess);

    int finalKey = getGroupKey(partition);

    words = partition.get(finalKey);

    checkWords(guess);

    return words;
  }

  private HashMap<Integer, SortedSet<String>> groupWords(char guess){
    HashMap<Integer, SortedSet<String>> partition = new HashMap<Integer, SortedSet<String>>();
    for (String word : words) {
      int count = 0;
      int hash = 1;
      for (int i = 0; i < word.length(); i++){
        if(guess == word.charAt(i)) {
          hash *= (word.length() - i);
          count++;
        }
      }
      hash *= count;
      if(!partition.containsKey(hash)) partition.put(hash, new TreeSet<>());
      partition.get(hash).add(word);
    }
    return partition;
  }

  private int getGroupKey(HashMap<Integer, SortedSet<String>> partition){
    int maxSize = 0;
    int finalKey = 0;
    for (Map.Entry<Integer, SortedSet<String>> entry: partition.entrySet()){
      SortedSet<String> currGroup = entry.getValue();
      int currSize = currGroup.size();
      int currKey = entry.getKey();
      if(currSize == maxSize) if (currKey < finalKey) finalKey = currKey;
      if(currSize > maxSize) {
        maxSize = currSize;
        finalKey = currKey;
      }
    }
    return finalKey;
  }

  private void checkWords(char guess){
    String check = words.first();
    output = new StringBuilder(currWord);

    int count = 0;
    for (int i = 0; i < check.length(); i++){
      if(guess == check.charAt(i)) {
        output.setCharAt(i, guess);
        count++;
      }
    }

    if (guessesLeft != 0) {
      if(count > 1 ) {
        System.out.println("Yes, there are " + count + " " + guess + "\'s");
      } else if (count == 1){
        System.out.println("Yes, there is " + count + " " + guess);
      } else {
        System.out.println("Sorry, there are no " +  guess + "\'s");
      }
    }

    currWord = output.toString();
  }

  @Override
  public SortedSet<Character> getGuessedLetters() {
    return guessedLetters;
  }

  public String getOutput(){
    return output.toString();
  }

  String getSecretWord(){
    return words.first();
  }

  public boolean match(){
    String check = words.first();
    String guessed = getOutput();
    if(check.equals(guessed)) System.out.println(guessed + " is correct!\nYou win!");
    return check.equals(guessed);
  }

  void setGuessesLeft(int guessesLeft) {
    this.guessesLeft = guessesLeft;
  }
}
