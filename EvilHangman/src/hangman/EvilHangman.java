package hangman;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.InvalidParameterException;
import java.util.Scanner;
import java.util.Set;

public class EvilHangman {
  private static int guesses;

  //Example java EvilHangman dictionary.txt 5 10
  public static void main(String[] args) throws IOException, EmptyDictionaryException, IllegalArgumentException, GuessAlreadyMadeException {
    File dictionaryFile = null;
    int wordLength = 0;
    guesses = 0;
    try{
      dictionaryFile = new File(args[0]);
      wordLength = Integer.parseInt(args[1]);
      guesses = Integer.parseInt(args[2]);
    } catch (IllegalArgumentException ex) {
      ex.printStackTrace();
    }

    if (wordLength < 1 || guesses < 1) return;

    EvilHangmanGame game = new EvilHangmanGame();
    try{
      game.startGame(dictionaryFile, wordLength);
    } catch (IOException | EmptyDictionaryException ex) {
      ex.printStackTrace();
    }
    while(guesses > 0 && !game.match()){
      game.setGuessesLeft(guesses - 1);
      print("You have " + guesses + (guesses == 1 ? " guess" : " guesses")  + " left");
      StringBuilder guessedSb = new StringBuilder();
      for(char letter : game.getGuessedLetters()){
        guessedSb.append(letter);
        guessedSb.append(" ");
      }
      print("Used letters: " + guessedSb);
      print("Word: " + game.getOutput());
      Scanner reader = new Scanner(System.in);
      String guess = null;
      char guessedChar = 0;
      try {
        System.out.print("Enter guess: ");
        guess = reader.next();
        guessedChar = getInputChar(guess);
        try{
          if(guessedChar != 0) game.makeGuess(guessedChar);
          System.out.print("\n");
        } catch (GuessAlreadyMadeException ex) {
          print(ex.getMessage());
          guesses++;
        }
      } catch (InvalidParameterException ex){
        print(ex.getMessage());
      }
    }

    if(guesses == 0 ) print("You lose!\nThe word was: " + game.getSecretWord());
  }

  private static char getInputChar(String guess){
    guess = guess.trim();
    if (!Character.isLetter(guess.charAt(0))) {
      throw new InvalidParameterException("Please, enter a valid character from a - z\n");
    } else if (guess.trim().length() != 1) {
      throw new InvalidParameterException("Please, enter only one character\n");
    }
    guesses--;
    return Character.toLowerCase(guess.charAt(0));
  }

  private static void print(String out){
    System.out.println(out);
  }
}
